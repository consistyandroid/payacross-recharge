package com.payacrossmobileapp.Json;

/**
 * Created by Somnath-Laptop on 05-Jan-2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginDetailsPojo {
    @SerializedName("RechargeID")
    @Expose
    private String rechargeID;
    @SerializedName("Regid")
    @Expose
    private String regid;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("Mobile_Number")
    @Expose
    private String mobileNumber;
    @SerializedName("UserType")
    @Expose
    private String userType;
    @SerializedName("LapuId")
    @Expose
    private String lapuId;
    @SerializedName("userTypeOtomax")
    @Expose
    private String userTypeOtomax;
    @SerializedName("userCodeOtomax")
    @Expose
    private String userCodeOtomax;
    @SerializedName("Message")
    @Expose
    private String message;

    public String getRechargeID() {
        return rechargeID;
    }

    public void setRechargeID(String rechargeID) {
        this.rechargeID = rechargeID;
    }

    public String getRegid() {
        return regid;
    }

    public void setRegid(String regid) {
        this.regid = regid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getLapuId() {
        return lapuId;
    }

    public void setLapuId(String lapuId) {
        this.lapuId = lapuId;
    }

    public String getUserTypeOtomax() {
        return userTypeOtomax;
    }

    public void setUserTypeOtomax(String userTypeOtomax) {
        this.userTypeOtomax = userTypeOtomax;
    }

    public String getUserCodeOtomax() {
        return userCodeOtomax;
    }

    public void setUserCodeOtomax(String userCodeOtomax) {
        this.userCodeOtomax = userCodeOtomax;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
