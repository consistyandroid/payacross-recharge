package com.payacrossmobileapp.utility;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.payacrossmobileapp.main.Common;
import com.payacrossmobileapp.main.R;

import org.json.JSONArray;

public class ElectricityActivity extends Activity {
	EditText edtAmount, edtConsumerNumber, edtMobile, edtBillUnit;
	ProgressDialog pDialog;
	TextView txtElectricity;
	Animation animVanish;
	Spinner spProvider, spUnitCycle;
	Button btnPayBill, btnCancel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_electricity);
	//	txtElectricity = (TextView) findViewById(R.id.txtelecbill);
		spProvider = (Spinner) findViewById(R.id.edtProvider);
		edtConsumerNumber = (EditText) findViewById(R.id.edtConsumerNumber);
		edtBillUnit = (EditText) findViewById(R.id.edtBillingUnit);
		edtAmount = (EditText) findViewById(R.id.edtAmount);
		edtMobile = (EditText) findViewById(R.id.edtMobileNumber);
		spUnitCycle = (Spinner) findViewById(R.id.edtProcessingCycle);
		btnCancel = (Button) findViewById(R.id.btnCancel);
		btnPayBill = (Button) findViewById(R.id.btnPay);
		pDialog=new ProgressDialog(ElectricityActivity.this);
		animVanish = AnimationUtils.loadAnimation(this, R.anim.anim_vanish);

		btnPayBill.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				v.startAnimation(animVanish);
				if (Validation()) {
					Toast.makeText(getApplicationContext(), "Validating....",
							Toast.LENGTH_SHORT).show();
				}
			}
		});
		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				v.startAnimation(animVanish);

			}
		});

		FontSettings();
	}

	private boolean Validation() {
		if (spProvider.getSelectedItemPosition() < 1) {
			Toast.makeText(getApplicationContext(),
					"Please select service provider? ", Toast.LENGTH_SHORT)
					.show();
			spProvider.requestFocus();
			return false;
		}
		if (edtConsumerNumber.getText().toString().trim().equals("")) {
			edtConsumerNumber.setError("Please enter consumer number ?");
			edtConsumerNumber.requestFocus();
			return false;
		}
		if (edtBillUnit.getText().toString().trim().equals("")) {
			edtBillUnit.setError("Please enter 4 digit billing number ?");
			edtBillUnit.requestFocus();
			return false;
		}
		if (edtAmount.getText().toString().length() <= 0) {
			edtAmount.setError("Please enter valid amount ?");
			edtAmount.requestFocus();
			return false;
		}
		if (edtMobile.getText().toString().length() != 10) {
			edtMobile.setError("Please enter valid mobile number ?");
			edtMobile.requestFocus();
			return false;
		}
		if (spUnitCycle.getSelectedItemPosition() < 1) {
			Toast.makeText(getApplicationContext(),
					"Please select service provider? ", Toast.LENGTH_SHORT)
					.show();
			spProvider.requestFocus();
			return false;
		}

		return true;

	}

	private void PaybillUrl() {
		pDialog.setMessage("Authentication is in processing...");
		pDialog.show();
		String url = "";
		JsonArrayRequest request = new JsonArrayRequest(url,
				new Listener<JSONArray>() {

					@Override
					public void onResponse(JSONArray arg0) {

					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub

					}
				});
		
	}
	private void FontSettings() {
		Typeface tf = Typeface.createFromAsset(getAssets(), Common.fonts);

		//txtElectricity.setTypeface(tf);

		edtConsumerNumber.setTypeface(tf);
		edtBillUnit.setTypeface(tf);
		edtAmount.setTypeface(tf);
		edtMobile.setTypeface(tf);

		btnCancel.setTypeface(tf);
		btnPayBill.setTypeface(tf);

	}

}
