package com.payacrossmobileapp.utility;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.payacrossmobileapp.main.Common;
import com.payacrossmobileapp.main.R;

public class LandlineActivity extends Activity {
	EditText edtOperator, edtLandlineNumber, edtAmount, edtMobile,
			editLandAccountNumber, edtCircle;
	TextView txtlandlinebill;
	Animation animVanish;
	Button btnPaybill, btnCancel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_landline);
	//	txtlandlinebill = (TextView) findViewById(R.id.lanlinebill);
		edtOperator = (EditText) findViewById(R.id.sp_operator);
		edtLandlineNumber = (EditText) findViewById(R.id.txtlandnumber);
		editLandAccountNumber = (EditText) findViewById(R.id.txtlandaccountnumber);
		edtAmount = (EditText) findViewById(R.id.txtamount);
		edtMobile = (EditText) findViewById(R.id.txtmobileno);
		edtCircle = (EditText) findViewById(R.id.txtcircle);
		btnCancel = (Button) findViewById(R.id.btncancel);
		btnPaybill = (Button) findViewById(R.id.btnpay);
		animVanish = AnimationUtils.loadAnimation(this, R.anim.anim_vanish);
		btnPaybill.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				v.startAnimation(animVanish);
			}
		});
		btnCancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				v.startAnimation(animVanish);
			}
		});

		FontSettings();
	}

	/*
	 * private boolean Validation() { if
	 * (edtMobile.getText().toString().length() != 10) {
	 * edtMobile.setError("Please enter valid mobile number ?");
	 * edtMobile.requestFocus(); return false; } else if
	 * (edtAmount.getText().toString().length() <= 0) {
	 * edtAmount.setError("Please enter valid amount ?");
	 * edtAmount.requestFocus(); return false; } return false;
	 * 
	 * }
	 */
	private void FontSettings() {
		Typeface tf = Typeface.createFromAsset(getAssets(), Common.fonts);

		//txtlandlinebill.setTypeface(tf);
		edtOperator.setTypeface(tf);
		editLandAccountNumber.setTypeface(tf);
		edtMobile.setTypeface(tf);
		edtAmount.setTypeface(tf);
		edtLandlineNumber.setTypeface(tf);
		
		edtCircle.setTypeface(tf);
		btnCancel.setTypeface(tf);
		btnPaybill.setTypeface(tf);

	}
}
