package com.payacrossmobileapp.utility;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;

import com.payacrossmobileapp.main.R;

@SuppressWarnings("deprecation")
public class UtilityBillTabActivity extends TabActivity{
	TabHost tabHost;
	Intent intent;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.utilititybilltab);
		tabHost=getTabHost();
		TabHost.TabSpec spec;
		
		intent =new Intent(UtilityBillTabActivity.this, ElectricityActivity.class);
		spec = tabHost
				.newTabSpec("Electricity Bill")
				.setIndicator("",getResources().getDrawable(R.drawable.electricity_tab))
				.setContent(intent);
		tabHost.addTab(spec);
		intent =new Intent(UtilityBillTabActivity.this, InsuranceActivity.class);
		spec = tabHost
				.newTabSpec("Insurance Bill")
				.setIndicator("",getResources().getDrawable(R.drawable.insurance_tab))
				.setContent(intent);
		tabHost.addTab(spec);
		intent =new Intent(UtilityBillTabActivity.this, GasActivity.class);
		spec = tabHost
				.newTabSpec("Gas Bill")
				.setIndicator("",getResources().getDrawable(R.drawable.gas_tab))
				.setContent(intent);
		tabHost.addTab(spec);
		intent =new Intent(UtilityBillTabActivity.this, LandlineActivity.class);
		spec = tabHost
				.newTabSpec("Landline Bill")
				.setIndicator("",getResources().getDrawable(R.drawable.landline_tab))
				.setContent(intent);
		tabHost.addTab(spec);
		
	}

}
