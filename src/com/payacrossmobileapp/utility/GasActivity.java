package com.payacrossmobileapp.utility;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.payacrossmobileapp.main.Common;
import com.payacrossmobileapp.main.R;

public class GasActivity extends Activity {
	EditText edtAgency, edtAmount, edtConsumerName, edtMobile,
			edtGasBookNumber;
	TextView txtgasagency;

	Button paybill, cancel;
	Animation animVanish;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gas);

		edtAgency = (EditText) findViewById(R.id.edtAgency);
		edtConsumerName = (EditText) findViewById(R.id.edtConsumerName);
		edtGasBookNumber = (EditText) findViewById(R.id.edtGasbookNumber);
		edtAmount = (EditText) findViewById(R.id.edtAmount);
		edtMobile = (EditText) findViewById(R.id.edtMobileNumber);

		cancel = (Button) findViewById(R.id.btncancel);
		paybill = (Button) findViewById(R.id.btnpay);

		animVanish = AnimationUtils.loadAnimation(this, R.anim.anim_vanish);
		paybill.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				v.startAnimation(animVanish);
			}
		});

		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				v.startAnimation(animVanish);
			}
		});
		FontSettings();
		overridePendingTransition(R.anim.open_translate, R.anim.close_scale);
	}

/*	private boolean Validation() {
		if (edtMobile.getText().toString().length() != 10) {
			edtMobile.setError("Please enter valid mobile number ?");
			edtMobile.requestFocus();
			return false;
		} else if (edtAmount.getText().toString().length() <= 0) {
			edtAmount.setError("Please enter valid amount ?");
			edtAmount.requestFocus();
			return false;
		}
		return false;

	}*/

	private void FontSettings() {
		Typeface tf = Typeface.createFromAsset(getAssets(), Common.fonts);

		// txtgasagency.setTypeface(tf);
		edtAgency.setTypeface(tf);
		edtAmount.setTypeface(tf);
		edtConsumerName.setTypeface(tf);
		edtMobile.setTypeface(tf);
		edtGasBookNumber.setTypeface(tf);

		cancel.setTypeface(tf);
		paybill.setTypeface(tf);

	}

}
