package com.payacrossmobileapp.utility;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.payacrossmobileapp.main.Common;
import com.payacrossmobileapp.main.R;

public class InsuranceActivity extends Activity {
	EditText edtCustomerDob, edtAmount,edtCompanyName,edtPolicyHolder,edtMobile,edtPolicynumber,edtBillDueDate,edtClientid;
	TextView txtinsurance;
	Animation animVanish;
	Button  btnPaybill,btnCancel;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_insurance);
		//txtinsurance =(TextView) findViewById(R.id.insurancebill);
		edtCompanyName = (EditText) findViewById(R.id.txtcompanyname);
		edtPolicynumber = (EditText) findViewById(R.id.edtpolnumber);
		edtPolicyHolder = (EditText) findViewById(R.id.edtpolholdername);
		edtCustomerDob = (EditText) findViewById(R.id.edtcustomerdob);
		edtMobile = (EditText) findViewById(R.id.edtmobileno);
		edtAmount = (EditText) findViewById(R.id.edtamount);
		edtBillDueDate = (EditText) findViewById(R.id.edtbillduedate);
		edtClientid = (EditText) findViewById(R.id.edtClientid);
		btnCancel = (Button) findViewById(R.id.btncancel);
		btnPaybill = (Button) findViewById(R.id.btnpay);
		animVanish =  AnimationUtils.loadAnimation(this, R.anim.anim_vanish);
		
		btnPaybill.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				v.startAnimation(animVanish);
				
			}
		});
		btnCancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				v.startAnimation(animVanish);
				
			}
		});
		FontSettings();

		
	}

/*	private boolean Validation() {
		if (edtMobile.getText().toString().length() != 10) {
			edtMobile.setError("Please enter valid mobile number ?");
			edtMobile.requestFocus();
			return false;
		} else if (edtAmount.getText().toString().length() <= 0) {
			edtAmount.setError("Please enter valid amount ?");
			edtAmount.requestFocus();
			return false;
		}
		return false;

	}*/
	private void FontSettings() {
		Typeface tf = Typeface.createFromAsset(getAssets(), Common.fonts);

		edtCustomerDob.setTypeface(tf);
		edtAmount.setTypeface(tf);
		edtCompanyName.setTypeface(tf);
		edtPolicyHolder.setTypeface(tf);
		edtPolicynumber.setTypeface(tf);
		edtMobile.setTypeface(tf);
		edtBillDueDate.setTypeface(tf);
		edtClientid.setTypeface(tf);
	//	txtinsurance.setTypeface(tf);
	
		btnCancel.setTypeface(tf);
		btnPaybill.setTypeface(tf);
		

	}
}
