package com.payacrossmobileapp.recharge;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.payacrossmobileapp.adapter.Report;
import com.payacrossmobileapp.adapter.ReportListAdaptor;
import com.payacrossmobileapp.main.Common;
import com.payacrossmobileapp.main.MainActivity;
import com.payacrossmobileapp.main.NetworkState;
import com.payacrossmobileapp.main.PickContact;
import com.payacrossmobileapp.main.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class DatacardActivity extends Activity {
    public static EditText edtMobile, edtAmount;
    TextView txtmobilerecharge;
    Spinner sp_Operator;
    Button recharge, clear;
    Context context=this;
    Dialog dialog;
    CheckBox check;
    Animation animVanish;
    String type, operatortype, circlecode, opcode, mobno, amount;
    ImageView imgPhnBook;
    ProgressDialog pDialog;
    Context localcont = this;
    String msgstring;
    String status = "";
    double txtAmount, currentBalance;
    int firstCnt = 0;
    String balStatus = "", lastMessage = "";
    SignalR sigR;
    NetworkState ns = new NetworkState();
    boolean flag = true;
    public ListView listview;
    ArrayAdapter<String> mobile;

    ArrayList<Report> reportList = new ArrayList<Report>();
    String lastDOC = "";
    int y = 0;

    ReportListAdaptor adapter;

    SwipeRefreshLayout swipeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datacard);

        imgPhnBook = (ImageView) findViewById(R.id.phnbook);
        edtMobile = (EditText) findViewById(R.id.edtMobileNumber);
        edtAmount = (EditText) findViewById(R.id.edtAmount);
        sp_Operator = (Spinner) findViewById(R.id.sp_operator);
        recharge = (Button) findViewById(R.id.btnrecharge);
        clear = (Button) findViewById(R.id.btnclear);
        setTitle("DataCard Recharge");
        listview = (ListView) findViewById(R.id.reachlistMessages);

        listview.setOnScrollListener(new OnScrollListener() {
            private int priorFirst = -1;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                y = listview.getFirstVisiblePosition();
            }

            @Override
            public void onScroll(final AbsListView view, final int first, final int visible, final int total) {
                y = listview.getFirstVisiblePosition();
                //Log.e("Y event :", y+"");
            }
        });

        pDialog = new ProgressDialog(DatacardActivity.this.getParent());

        localcont = this;

        Bundle extras = getIntent().getExtras();
        type = extras.getString("type");

        operatortype = extras.getString("rechtype");

        animVanish = AnimationUtils.loadAnimation(this, R.anim.anim_vanish);

        if (operatortype.equalsIgnoreCase("DC")) {
            ArrayAdapter<String> mobile = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Common.data_spinnername);
            mobile.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sp_Operator.setAdapter(mobile);
            //edtMobile.setHint("Mobile Number");
        }

        sp_Operator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View vi2ew, int position, long id) {
                if (operatortype.equals("DC")) {
                    opcode = Common.data_spinnercode[position];
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        //signalR();
      lastFiveResponse();

        swipeView = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        swipeView.setColorScheme(R.color.Green, R.color.red, R.color.blue);

        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.setRefreshing(true);
                Log.d("Swipe", "Refreshing Number");
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeView.setRefreshing(false);
                        lastFiveResponse();
                    }
                }, 3000);
            }
        });

        clear.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animVanish);
                lastFiveResponse();
                clear();

            }
        });

        recharge.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animVanish);
                if (edtMobile.getText().toString().length() <= 0) {
                    edtMobile.setError("Please enter valid number");
                    edtMobile.requestFocus();
                } else if (sp_Operator.getSelectedItemPosition() < 1) {
                    Toast.makeText(getApplicationContext(), "Please select operator", Toast.LENGTH_SHORT).show();
                    sp_Operator.requestFocus();
                } else if (edtAmount.getText().toString().length() <= 0) {
                    edtAmount.setError("Please enter amount");
                    edtAmount.requestFocus();
                } else {
                    mobno = edtMobile.getText().toString().trim();
                    amount = edtAmount.getText().toString().trim();

                    msgstring = null;

                    if (type.equals("SMS")) {
                        msgstring = String.format("%s %s %s", opcode, mobno, amount);
                        com.payacrossmobileapp.main.Log.e("msgstring", "Number: " + mobno + " Amount: " + amount);
                        MainActivity.sendMessage(msgstring, DatacardActivity.this.getParent());
                    }
                    else if(type.equals("GPRS"))

                    {
                        if (ns.isInternetAvailable(context)) {
                            new BalCheckTask().execute();
                            sigR.setDisplayMessage(true);
                        }else {

                        }
                    }
                }
            }
        });

        imgPhnBook.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent i = new Intent(DatacardActivity.this, PickContact.class);
                    i.putExtra("type", "data");
                    startActivity(i);
                } catch (Exception e) {
                    Toast.makeText(getParent(), "Number is not format" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        TypefaceSettings(); // font settings
    }

    private void clear() {
        edtMobile.setText("");
        edtAmount.setText("");
        sp_Operator.setSelection(0);
        edtMobile.requestFocus();
    }


    public void lastFiveResponse() {
        if (type.equals("GPRS")) {
            adapter = new ReportListAdaptor(DatacardActivity.this, reportList);
            listview.setAdapter(adapter);
            sigR = new SignalR(reportList, adapter, DatacardActivity.this, DatacardActivity.this);
            try {
                String url = Common.COMMON_URL + "LastFiveResponse.asmx/LastFiveResponce?LapuId=" + Common.lapuid;
                JsonArrayRequest req = new JsonArrayRequest(url,
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                if (response != null) {
                                    reportList.clear();

                                    JSONObject jo;
                                    try {
                                        Common.resCount = response.length();
                                        for (int i = 0; i < Common.resCount; i++) {
                                            jo = response.getJSONObject(i);
                                            Report report = new Report(jo.getString("DOC"), jo.getString("Pesan"));
                                            reportList.add(report);

                                            if (i == 0) {
                                                lastDOC = jo.getString("DOC");
                                            }
                                        }
                                        adapter.notifyDataSetChanged();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();
                            }
                        });
                if (Common.internet_status) {
                    VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(req);
                } else {
                }
            } catch (Exception r) {
                r.printStackTrace();
            }
        }
    }

    //Start asynchronous task for Balance check
    public class BalCheckTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "LoginUser.asmx/GetBalance";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");

            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

				/*List nameValuePairs = new ArrayList();
                    // add an HTTP variable and value pair
					nameValuePairs.add(new BasicNameValuePair("RechargeID", Common.recharge_id));
					nameValuePairs.add(new BasicNameValuePair("RegIdNo", Common.regid));
					request.setEntity(new UrlEncodedFormEntity(nameValuePairs));*/
                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);


            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONBalCheck(result);
        }
    }

    //Parse the JSON response
    void parseJSONBalCheck(String result) {

        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(this, "Wrong responce", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        balStatus = jo.getString("Status");
                        //Log.e("bal", jo.getString("Status")+" "+bal);
                    }//for
                    txtAmount = Double.parseDouble(amount);
                    //Log.e("amt", txtAmount+"");
                    currentBalance = Double.parseDouble(balStatus);
                    //Log.e("bal", currentBalance+"");

                    if (txtAmount > currentBalance) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(DatacardActivity.this.getParent());//.getParent()
                        builder.setTitle("Message");
                        builder.setIcon(R.mipmap.ic_launcher);
                        builder.setMessage("Your current balance is lower than recharge amount.");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    } else {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DatacardActivity.this.getParent());
                        // set title
                        alertDialogBuilder.setTitle("Please Confirm");
                        alertDialogBuilder.setIcon(R.mipmap.ic_launcher);

                        // set dialog message
                        //Log.e("Recharge","Number: "+mobno+" Amount: "+amount);
                        alertDialogBuilder
                                .setMessage(String.format("Number: %s\nAmount: %s\nOP: %s", mobno, amount, opcode))
                                .setCancelable(false)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        if (type.equals("GPRS")) {
                                            if (Common.internet_status) {
                                                new RechargeTask().execute();
                                            } else {
                                                pDialog.dismiss();
                                                Toast.makeText(DatacardActivity.this, "Internet connection is not available", Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        // show it
                        alertDialog.show();

                    }
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }
    //End asynchronous task for Balance check

    //Asyncronous task for recharge
    public class RechargeTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "GPRSRecharge.asmx/Recharge";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();

            Log.e("url", url);
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            try {
                url.replaceAll(" ", "%20");
                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                List nameValuePairs = new ArrayList();
                // add an HTTP variable and value pair

                nameValuePairs.add(new BasicNameValuePair("Operator", opcode));
                nameValuePairs.add(new BasicNameValuePair("Amount", amount));
                nameValuePairs.add(new BasicNameValuePair("MobileNumber", mobno));
                nameValuePairs.add(new BasicNameValuePair("DeviceID", Common.IMEI));
                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                request.setURI(new URI(url));
                HttpResponse response = client.execute(request);
                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);
                Log.i("Sending data ", String.format("Op: %s,amount: %s, mob: %s", opcode, amount, mobno));

            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSON(result);
        }
    }

    //Parse the JSON response
    void parseJSON(String result) {

        String status = "";
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(getApplicationContext(), "Recharge Failed Try after same time!", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        status = jo.getString("Status");
                    }//for
                    clear();
                /*    AlertDialog.Builder builder = new AlertDialog.Builder(DatacardActivity.this.getParent());
                    builder.setTitle("Message");
                    builder.setIcon(R.drawable.ic_launcher);
                    builder.setMessage("" + status);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                    *//*double f = Double.parseDouble(balStatus);
                    balAmt=edtAmount.getText().toString();
					double a = Double.parseDouble(balAmt);
					double amt=f-a;
					String strAmt = Double.toString(amt);

					MainActivity.txtBalance.setText(strAmt);*//*

                            edtMobile.setText("");
                            edtAmount.setText("");
                            sp_Operator.setSelection(0);
                            edtMobile.requestFocus();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();*/
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }


            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(20000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    new LoadBalCheckTask().execute();
                }
            }).start();
        }
    }

    //Start asynchronous task for show Balance on dashboard
    public class LoadBalCheckTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "LoginUser.asmx/GetBalance";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }

            return out.toString();
        }

        @Override
        protected void onPreExecute() {
			/*pDialog.setMessage("Please wait...");
			pDialog.setIndeterminate(true);
			pDialog.setCancelable(false);
			pDialog.show();*/
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            String result = null;
            url = url.replaceAll(" ", "%20");
            Log.e("url", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONLoadBalCheck(result);
        }
    }

    //Parse the JSON response
    void parseJSONLoadBalCheck(String result) {
        String status = "";
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(DatacardActivity.this, "Wrong responce", Toast.LENGTH_LONG).show();
                } else {
                    //Received response from web service
                    //"Status": "489.7500"
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        status = jo.getString("Status");
                        //Log.e("bal",status);
                    }//for

                    MainActivity.txtBalance.setText(status);
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
                e.printStackTrace();
            }
        }
    }
    //End asynchronous task for show Balance on dashboard
/*
    @Override
    protected void onPause() {
        super.onPause();
        sigR.setDisplayMessage(false);
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        flag = false;
        Thread.currentThread().interrupt();
    }

    // Setting of font
    private void TypefaceSettings() {
        Typeface tf = Typeface.createFromAsset(getAssets(), Common.fonts);
        edtMobile.setTypeface(tf);
        edtAmount.setTypeface(tf);
        recharge.setTypeface(tf);
    }
    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(DatacardActivity.this);
        builder.setTitle("Confirm");
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setMessage("Are you sure to exit?");

        builder.setNegativeButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.exit(0);
                // Do nothing
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                //dialog.dismiss();

            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

}
