package com.payacrossmobileapp.recharge;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.payacrossmobileapp.main.Common;
import com.payacrossmobileapp.main.MainActivity;
import com.payacrossmobileapp.main.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import microsoft.aspnet.signalr.client.Platform;
import microsoft.aspnet.signalr.client.http.android.AndroidPlatformComponent;

public class ElectricityBill extends Activity {
    public static EditText edtMobile, edtAmount;
    TextView txtmobilerecharge;
    String balStatus = "", balAmt;
    Spinner sp_Operator;
    Button Pay, clear,btn_view;
    CheckBox check;
    Animation animVanish;
    String type, operatortype, consNumber, cno, opcode, mobno, amount, consName, billno, dcname, dueDate, desc, billMonth,company;
    public static EditText edtCno, edtCoName, edtBillUnit, edtBillAmount;
    ImageView imgPhnBook;
    ProgressDialog pDialog;
    Context localcont = this;
    String msgstring, servicecode;
    String status = "";
    double txtAmount, currentBalance;

    String code = null, operator = null, state = null;
    SignalR sigR;

    boolean flag = false;
    public ListView listview;
    ArrayAdapter<String> mobile;

    ArrayList<com.payacrossmobileapp.adapter.Report> reportList = new ArrayList<com.payacrossmobileapp.adapter.Report>();
    String lastDOC = "", lastMessage = "";
    int y = 0;

    com.payacrossmobileapp.adapter.ReportListAdaptor adapter;
    String[] array_type = {"Select Type", "Chhattisgarh State Power"};
    SwipeRefreshLayout swipeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_electricity_bill);

        Platform.loadPlatformComponent(new AndroidPlatformComponent());

        edtCno = (EditText) findViewById(R.id.txtcno);
        edtMobile = (EditText) findViewById(R.id.edtmobileno);
        // edtBillUnit = (EditText) findViewById(R.id.txtbillunit);
        edtBillAmount = (EditText) findViewById(R.id.txtbillamount);
        sp_Operator = (Spinner) findViewById(R.id.sp_operator);
        btn_view = (Button) findViewById(R.id.btnrecharge);
        clear = (Button) findViewById(R.id.btnclear);

        pDialog = new ProgressDialog(ElectricityBill.this);//getParent();

        localcont = this;



        animVanish = AnimationUtils.loadAnimation(this, R.anim.anim_vanish);


            ArrayAdapter<String> mobile = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, array_type);
            mobile.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sp_Operator.setAdapter(mobile);


        sp_Operator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View vi2ew, int position, long id) {

                    opcode = Common.dth_spinnercode[position];

            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        Bundle extras = getIntent().getExtras();
        type = extras.getString("type");
        operatortype = extras.getString("rechtype");



        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animVanish);
                edtCno.setText("");
                edtMobile.setText("");
                edtBillAmount.setText("");
                edtCno.requestFocus();
            }
        });

        btn_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animVanish);
                if (validation()) {
                    cno = edtCno.getText().toString().trim();
                    mobno = edtMobile.getText().toString().trim();
                    amount = edtBillAmount.getText().toString().trim();

                    new ViewElectricitytask().execute();
                   /* msgstring = null;
                    if (type.equals("SMS")) {
                        msgstring = String.format("%s %s %s", opcode, cno, amount);
                        Log.e("msgstring", "Number: " + cno + " Amount: " + amount);
                        MainActivity.sendMessage(msgstring, ElectricityBill.this.getParent());
                    } else if (type.equals("GPRS")) {
                        new BalCheckTask().execute();
                        sigR.setDisplayMessage(true);
                    }*/
                }
            }
        });

        TypefaceSettings(); // font settings
    }

    private boolean validation() {
        if (sp_Operator.getSelectedItemPosition()>1) {
            Toast.makeText(ElectricityBill.this,"Select Service type",Toast.LENGTH_SHORT).show();
            sp_Operator.requestFocus();
            return false;
        }
        if (edtCno.getText().toString().equals("")) {
            edtCno.setError("Enter Consumer Number");
            edtCno.requestFocus();
            return false;
        } else if (edtMobile.getText().toString().length() < 10) {
            edtMobile.setError("Enter Mobile Number");
            edtMobile.requestFocus();
            return false;
        } else if (edtBillAmount.getText().toString().length() <= 0) {
            edtBillAmount.setError("Please enter amount");
            edtBillAmount.requestFocus();
            return false;
        }
        return true;
    }

    private void clear() {
        edtCno.setText("");
        edtMobile.setText("");
        edtBillAmount.setText("");
        edtCno.requestFocus();
    }

    //Start asynchronous task for Balance check
    public class BalCheckTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "LoginUser.asmx/GetBalance";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");

            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);


                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);


            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONBalCheck(result);
        }
    }

    //Parse the JSON response
    void parseJSONBalCheck(String result) {

        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(this, "Wrong responce", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        balStatus = jo.getString("Status");
                        Log.e("bal", balStatus);
                    }//for
                    txtAmount = Double.parseDouble(amount);
                    //Log.e("amt", txtAmount+"");
                    currentBalance = Double.parseDouble(balStatus);
                    //Log.e("bal", currentBalance+"");

                    if (txtAmount > currentBalance) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(ElectricityBill.this.getParent());//.getParent()
                        builder.setTitle("Message");
                        builder.setIcon(R.mipmap.ic_launcher);
                        builder.setMessage("Your current balance is lower than recharge amount.");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    } else {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ElectricityBill.this.getParent());
                        // set title
                        alertDialogBuilder.setTitle("Please Confirm");
                        alertDialogBuilder.setIcon(R.mipmap.ic_launcher);

                        // set dialog message
                        //Log.e("Recharge","Number: "+mobno+" Amount: "+amount);
                        alertDialogBuilder
                                .setMessage(String.format("Number: %s\nAmount: %s\nOP: %s", mobno, amount, opcode))
                                .setCancelable(false)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                       /* if (type.equalsIgnoreCase("SMS")) {
                                            msgstring = String.format("%s %s %s", opcode, mobno, amount);
                                            Log.e("msgstring","Number: "+mobno+" Amount: "+amount);
                                            MainActivity.sendMessage(msgstring, MobileActivity.this.getParent());
                                        } else if (type.equals("GPRS")) {
                                            if (Common.internet_status) {*/
                                        if (type.equals("GPRS")) {
                                            if (Common.internet_status) {
                                                new ViewElectricitytask().execute();
                                            } else {
                                                pDialog.dismiss();
                                                Toast.makeText(ElectricityBill.this, "Internet connection is not available", Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        // show it
                        alertDialog.show();

                    }
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }
    //End asynchronous task for Balance check

    //Asyncronous task for recharge
    public class ViewElectricitytask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "PayElectricityBill.asmx/getElectricityBillPayRequest";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();

            Log.e("url", url);
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            try {
                url.replaceAll(" ", "%20");
                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                List nameValuePairs = new ArrayList();
                // add an HTTP variable and value pair


                nameValuePairs.add(new BasicNameValuePair("amount", amount));
                nameValuePairs.add(new BasicNameValuePair("consumerNo", cno));
                nameValuePairs.add(new BasicNameValuePair("mobile", mobno));
                //ePairs.add(new BasicNameValuePair("DeviceID", Common.IMEI));
                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                request.setURI(new URI(url));
                HttpResponse response = client.execute(request);
                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);
                Log.i("Sending data ", String.format("Op: %s,amount: %s, mob: %s", cno, amount, mobno));

            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSON(result);
        }
    }

    //Parse the JSON response
    void parseJSON(String result) {

        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(getApplicationContext(), "Recharge Failed Try after some time!", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        consNumber = jo.getString("BpNumber");
                        consName = jo.getString("ConsumerName");
                        billno = jo.getString("BillNumber");
                        dcname = jo.getString("DCName");
                        amount = jo.getString("Amount");
                        dueDate = jo.getString("DueDate");
                        billMonth = jo.getString("BillMonth");
                        status = jo.getString("status");
                        desc = jo.getString("desc");
                    }//for
                    clear();


                }
                Intent i = new Intent(ElectricityBill.this, ElectricityBillPayActivity.class);

                Bundle bundle = new Bundle();
                bundle.putString("type", type);
                 /*   bundle.putString("company", company);
                    bundle.putString("companyCode",companycode);*/
                bundle.putString("txtCunsumerNo", consNumber);
                bundle.putString("txtBillno", billno);
                bundle.putString("txtDcName", dcname);
                bundle.putString("txtCustomerName", consName);
                bundle.putString("txtdueAmount", amount);
                bundle.putString("txtdueDate", dueDate);
                bundle.putString("txtbillmonth", billMonth);
                bundle.putString("txtmobileno", mobno);
                bundle.putString("txtdesc", desc);
                bundle.putString("txtStatus", status);
                bundle.putString("Company", sp_Operator.getSelectedItem().toString());
                            /*bundle.putString("txtApiName",apiName);*/
							/*bundle.putString("txtUname",Uname);
							bundle.putString("txtpwd",pwd);
							bundle.putString("txtuserpin",userpin);*/
                i.putExtras(bundle);

                startActivity(i);

                Log.e("consumer",consNumber);
                Log.e("billno",billno);
                Log.e("dcname",dcname);
                Log.e("consName",consName);
                Log.e("amount",amount);
                Log.e("dueDate",dueDate);
                Log.e("billMonth",billMonth);
                Log.e("mobno",mobno);
                Log.e("desc",desc);
                Log.e("status",status);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(20000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        //new LoadBalCheckTask().execute();
                    }
                }).start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //Start asynchronous task for show Balance on dashboard
        class LoadBalCheckTask extends AsyncTask<Void, Void, String> {
            String url = Common.COMMON_URL + "LoginUser.asmx/GetBalance";

            protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
                InputStream in = entity.getContent();
                StringBuffer out = new StringBuffer();
                int n = 1;
                while (n > 0) {
                    byte[] b = new byte[4096];
                    n = in.read(b);
                    if (n > 0) out.append(new String(b, 0, n));
                }

                return out.toString();
            }

            @Override
            protected void onPreExecute() {
            /*pDialog.setMessage("Please wait...");
                pDialog.setIndeterminate(true);
				pDialog.setCancelable(false);
				pDialog.show();*/
            }

            @Override
            protected String doInBackground(Void... params) {
            /*try {
                Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
                String result = null;
                url = url.replaceAll(" ", "%20");
                Log.e("url", url);
                try {

                    HttpClient client = new DefaultHttpClient();
                    HttpPost request = new HttpPost();
                    request.setURI(new URI(url));

                    request.addHeader("Regid", Common.regid);
                    request.addHeader("RechargeID", Common.recharge_id);

                    HttpResponse response = client.execute(request);

                    HttpEntity entity = response.getEntity();
                    result = getASCIIContentFromEntity(entity);

                    Log.e("Result", result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                pDialog.dismiss();
                parseJSONLoadBalCheck(result);
            }
        }
    }

    //Parse the JSON response
    void parseJSONLoadBalCheck(String result) {
        String status = "";
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(ElectricityBill.this, "Wrong responce", Toast.LENGTH_LONG).show();
                } else {
                    //Received response from web service
                    //"Status": "489.7500"
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        status = jo.getString("Status");
                        //Log.e("bal",status);
                    }//for

                    MainActivity.txtBalance.setText(status);
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
                e.printStackTrace();
            }
        }
    }
    //End asynchronous task for show Balance on dashboard

   /* @Override
    protected void onPause() {
        super.onPause();
        sigR.setDisplayMessage(false);
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        flag = false;
        Thread.currentThread().interrupt();
    }

    // Setting of font
    private void TypefaceSettings() {
        Typeface tf = Typeface.createFromAsset(getAssets(), Common.fonts);
        edtCno.setTypeface(tf);
        edtBillAmount.setTypeface(tf);
        //edtBillUnit.setTypeface(tf);
        // edtCoName.setTypeface(tf);
        btn_view.setTypeface(tf);
        clear.setTypeface(tf);
    }

    /*@Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(ElectricityBill.this);
        builder.setTitle("Confirm");
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setMessage("Are you sure to exit?");

        builder.setNegativeButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.exit(0);
                // Do nothing
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                //dialog.dismiss();

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }*/
}
