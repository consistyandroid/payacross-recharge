package com.payacrossmobileapp.recharge;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.payacrossmobileapp.adapter.Report;
import com.payacrossmobileapp.adapter.ReportListAdaptor;
import com.payacrossmobileapp.main.Common;
import com.payacrossmobileapp.main.Log;
import com.payacrossmobileapp.main.R;

import java.util.ArrayList;

import microsoft.aspnet.signalr.client.Action;
import microsoft.aspnet.signalr.client.ErrorCallback;
import microsoft.aspnet.signalr.client.LogLevel;
import microsoft.aspnet.signalr.client.Logger;
import microsoft.aspnet.signalr.client.MessageReceivedHandler;
import microsoft.aspnet.signalr.client.hubs.HubConnection;
import microsoft.aspnet.signalr.client.hubs.HubProxy;

/**
 * Created by Somnath on 2/15/2016.
 */
public class SignalR {
    ArrayList<Report> reportList = new ArrayList<Report>();
    String lastMessage = "";
    ReportListAdaptor adapter;
    boolean displayMessage = false;
    Context context;
    Activity activity;

    public SignalR(ArrayList<Report> reportList, ReportListAdaptor adapter, Context context, Activity activity) {
        this.reportList = reportList;
        this.adapter = adapter;
        this.context = context;
        this.activity = activity;


        run();
    }

    public void setDisplayMessage(boolean displayMessage) {
        this.displayMessage = displayMessage;
    }

    public void run() {
        // int cnt = 1;
        // Create a new console logger
        Logger logger = new Logger() {
            @Override
            public void log(String message, LogLevel level) {
                //System.out.println(message);
            }
        };
        // Connect to the server
        HubConnection conn = new HubConnection(Common.signalRURL, "", true, logger);

        // Create the hub proxy
        final HubProxy proxy = conn.createHubProxy("rechargeHub");

        proxy.subscribe(new Object() {
            @SuppressWarnings("unused")
            public void messageReceived(String name, String message) {
                // System.out.println(name + ": " + message);
            }
        });

        conn.connectionSlow(new Runnable() {
            @Override
            public void run() {
                System.out.println("Connection is slow wait for some time ");
                Log.i("Slow", "Connection is slow wait for some time ");
            }
        });


        // Subscribe to the error event
        conn.error(new ErrorCallback() {
            @Override
            public void onError(Throwable error) {
                error.printStackTrace();
            }
        });

        // Subscribe to the connected event
        conn.connected(new Runnable() {
            @Override
            public void run() {
            }
        });


        // Subscribe to the closed event
        conn.closed(new Runnable() {
            @Override
            public void run() {
                // System.out.println("DISCONNECTED");
            }
        });

        // Start the connection
        conn.start()
                .done(new Action<Void>() {
                    @Override
                    public void run(Void obj) throws Exception {
                        //Invoke JoinGroup to start receiving broadcast messages
                        //  proxy.invoke("sendNotifications", "A90300");A90001
                        proxy.invoke("sendNotifications", Common.lapuid);

                    }
                });

        // Subscribe to the received event
        conn.received(new MessageReceivedHandler() {
            @Override
            public void onMessageReceived(JsonElement json) {
                parseSignalR(json.toString());

            }
        });
    }

    public void parseSignalR(String jsonLine) {

// OR condition is important here because, need to check any on one condition meets, then return back
        if (lastMessage.equals(jsonLine) || // some times SignalR return same message again and again
                jsonLine.contains("DataReader associated") || // this is error of signalR

                !jsonLine.contains("RecieveNotification")// our DOT NET method name is RecieveNotification, which return data
                ) {
            return;
        }
        lastMessage = jsonLine;
        reportList.clear();

        // Log.i("before filter ", jsonLine);
        jsonLine = jsonLine.replace("serverSentEvents - Found new data: data:", "");
        // Log.i("after filter ", jsonLine);

        //region input is :
    /*	{
        "H": "RechargeHub",
		"M": "RecieveNotification",
		"A":
		[
			[
				{
				"MobileNumber": null,
				"NumberOperator": null,
				"Other": "1.  Bal. 1,50,012.81 @15:25today 5pm costemor care stop",
				.
				.
				.
				},
				.
				.

			]
		]
       }*/
//endregion

        //and we need only "Other" inside array
        // so it is A[0][0].Other, A[0][1].Other and so on
        //  Log.i("In parse ", jsonLine);

        JsonElement jelement = new JsonParser().parse(jsonLine);
        JsonObject jobject = jelement.getAsJsonObject();

      /*  JsonArray jarray = jobject.getAsJsonArray("M");// Select M
        jobject = jarray.get(0).getAsJsonObject();// get M[0]*/

        JsonArray jarray = jobject.getAsJsonArray("A");// Select M[0].A
        jarray = jarray.get(0).getAsJsonArray();// get M[0].A[0]


        String lastLocal = "";

        for (int i = jarray.size() - 1; i >= 0; i--) {
            jobject = jarray.get(i).getAsJsonObject();// get M[0].A[0][i]

            String strDate = jobject.get("FromDate").toString();//Common.getTime(jobject.get("FromDate").toString(), "M/d/yyyy H:mm:ss aa", "M/d/yyyy H:mm:ss aa");
            String result = jobject.get("Other").toString();
            // System.out.println("lastLocal " + result);
            //  Log.i("date Time ", strDate);

            if (!result.equals(lastLocal)) {
                lastLocal = result;
                final Report report = new Report(strDate, result);
                final String strResult = result;
                //  Log.i("Report ", strDate + result);


                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        reportList.add(0, report);// add at position 0
                        adapter.notifyDataSetChanged();
                    }
                });


                //Popup only 1st message and
                //Don't display message at startup and
                // we don't need accepted message
                if (i == 0 && displayMessage && !result.contains("accepted")) {
                   // Log.i("In SignalR from ", context.getClass() + "");
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);//.getParent()
                            builder.setTitle("Message");
                            builder.setIcon(R.mipmap.ic_launcher);
                            builder.setMessage(strResult.toString());
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            AlertDialog alert = builder.create();
                            alert.show();
                        }
                    });
                }

            }

        }
    }
}
