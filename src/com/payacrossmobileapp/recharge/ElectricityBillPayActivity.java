package com.payacrossmobileapp.recharge;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.payacrossmobileapp.adapter.Report;
import com.payacrossmobileapp.adapter.ReportListAdaptor;
import com.payacrossmobileapp.main.Common;
import com.payacrossmobileapp.main.Log;
import com.payacrossmobileapp.main.MainActivity;
import com.payacrossmobileapp.main.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class ElectricityBillPayActivity extends Activity {
    Context lightcontext = this;
    String type;
    Button btnLightRecharge, btnClear;
    TextView cunsumerNumber, custName, dcName, billNo, buDescription, billAmount, dueDate, status, desc, company,txtmobileno;
    ProgressDialog pDialog;
    String description, consumerno, billno, custname, dcname, pCycle, dueAmount, due_Date, Status, balStatus,
            amount, billmonth, mobileno, Company;
    double txtAmount, currentBalance;
    ListView listview;
    ArrayList<Report> reportList = new ArrayList<Report>();
    String lastDOC = "", lastMessage = "";
    int y = 0;
    SwipeRefreshLayout swipeView;
    ReportListAdaptor adapter;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_electricity_bill_pay);
        lightcontext = this;

        try {
            Bundle extras = getIntent().getExtras();
            type = extras.getString("type");
        } catch (Exception e) {
            e.printStackTrace();
        }

        btnLightRecharge = (Button) findViewById(R.id.btnrecharge);
        btnClear = (Button) findViewById(R.id.btnclear);

        cunsumerNumber = (TextView) findViewById(R.id.txtbpnumber);
        custName = (TextView) findViewById(R.id.custName);
        dcName = (TextView) findViewById(R.id.txtdcname);
        billNo = (TextView) findViewById(R.id.txtbillno);
        buDescription = (TextView) findViewById(R.id.txtdescription);
        billAmount = (TextView) findViewById(R.id.txtamount);
        dueDate = (TextView) findViewById(R.id.txtduedate);
        status = (TextView) findViewById(R.id.txtstatus);
        desc = (TextView) findViewById(R.id.txtdescription);
        company = (TextView) findViewById(R.id.txtcompany);
        txtmobileno = (TextView) findViewById(R.id.txtmobileno);
        listview = (ListView) findViewById(R.id.reachlistMessages);

        listview.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int priorFirst = -1;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                y = listview.getFirstVisiblePosition();
            }

            @Override
            public void onScroll(final AbsListView view, final int first, final int visible, final int total) {

                y = listview.getFirstVisiblePosition();
                //Log.e("Y event :", y+"");
            }
        });

        pDialog = new ProgressDialog(this);

        Bundle extras = getIntent().getExtras();

        type = extras.getString("type");
        consumerno = extras.getString("txtCunsumerNo");
        billno = extras.getString("txtBillno");
        custname = extras.getString("txtCustomerName");
        dcname = extras.getString("txtDcName");
        dueAmount = extras.getString("txtdueAmount");
        due_Date = extras.getString("txtdueDate");
        billmonth = extras.getString("txtbillmonth");
        mobileno = extras.getString("txtmobileno");
        description = extras.getString("txtdesc");
        Status = extras.getString("txtStatus");
        Company = extras.getString("Company");

        cunsumerNumber.setText(consumerno);
        custName.setText(custname);
        dcName.setText(dcname);
        billNo.setText(billno);
        billAmount.setText(dueAmount);
        buDescription.setText(description);
        dueDate.setText(due_Date);
        status.setText(Status);
        company.setText(Company);
        txtmobileno.setText(mobileno);

        lastFiveResponse();

        swipeView = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        swipeView.setColorScheme(R.color.Green, R.color.red, R.color.blue);

        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.setRefreshing(true);
                Log.d("Swipe", "Refreshing Number");
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeView.setRefreshing(false);
                        lastFiveResponse();
                    }
                }, 3000);
            }
        });

        //getData();

        if (billAmount.getText().toString().equals("0.00")) {
            btnLightRecharge.setEnabled(false);
        }

        btnLightRecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(lightcontext);
                builder.setTitle("Confirm");
                builder.setMessage("Are you sure To Pay Electricity Bill Of Consumer No:" + cunsumerNumber.getText().toString() + " with Amount: " + billAmount.getText().toString() + " ?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        if (type.equals("GPRS")) {
                            new BalCheckTask().execute();

                        }

                        //new GetBalance().execute();
                    }
                });
                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastFiveResponse();
                finish();
            }
        });
    }

    public void lastFiveResponse() {
        if (type.equals("GPRS")) {
            adapter = new ReportListAdaptor(ElectricityBillPayActivity.this, reportList);
            listview.setAdapter(adapter);
            //sigR = new SignalR(reportList, adapter, ElectricityBillPayActivity.this, ElectricityBillPayActivity.this);
            try {
                String url = Common.COMMON_URL + "LastFiveResponse.asmx/LastFiveResponce?LapuId=" + Common.lapuid;
                JsonArrayRequest req = new JsonArrayRequest(url,
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                if (response != null) {
                                    reportList.clear();

                                    JSONObject jo;
                                    try {
                                        Common.resCount = response.length();
                                        for (int i = 0; i < Common.resCount; i++) {
                                            jo = response.getJSONObject(i);
                                            Report report = new Report(jo.getString("DOC"), jo.getString("Pesan"));
                                            reportList.add(report);

                                            if (i == 0) {
                                                lastDOC = jo.getString("DOC");
                                            }
                                        }
                                        adapter.notifyDataSetChanged();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();
                            }
                        });
                if (Common.internet_status) {
                    VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(req);
                } else {
                }
            } catch (Exception r) {
                r.printStackTrace();
            }
        }
    }

    //Start asynchronous task for Balance check
    public class BalCheckTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "LoginUser.asmx/GetBalance";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");

            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);


                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);


            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONBalCheck(result);
        }
    }

    //Parse the JSON response
    void parseJSONBalCheck(String result) {

        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(this, "Wrong responce", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        balStatus = jo.getString("Status");
                        Log.e("bal", balStatus);
                    }//for
                    txtAmount = Double.parseDouble(dueAmount);
                    //Log.e("amt", txtAmount+"");
                    currentBalance = Double.parseDouble(balStatus);
                    //Log.e("bal", currentBalance+"");

                    if (txtAmount > currentBalance) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(ElectricityBillPayActivity.this);//.getParent()
                        builder.setTitle("Message");
                        builder.setIcon(R.mipmap.ic_launcher);
                        builder.setMessage("Your current balance is lower than recharge amount.");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    } else if (type.equals("GPRS")) {
                        if (Common.internet_status) {
                            new RechargeTask().execute();
                        } else {
                            pDialog.dismiss();
                            Toast.makeText(ElectricityBillPayActivity.this, "Internet connection is not available", Toast.LENGTH_LONG).show();
                        }
                    }


                }

            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }

    //End asynchronous task for Balance check
    //Asyncronous task for recharge
    public class RechargeTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "PayElectricityBill.asmx/PayElectBill";
                //?&ServiceType=" +"2"+ "&BillNumber=" + billno + "&BillAmount=" + dueAmount + "&ConNumber=" + consumerno + "&BillMonth=" + billmonth + "&ConsName=" + custname.replace(" ", "%20") + "&MobileNo=" + mobileno;

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }
            return out.toString();

        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();


        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            try {
                url.replaceAll(" ", "%20");
                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                List nameValuePairs = new ArrayList();
                // add an HTTP variable and value pair

                nameValuePairs.add(new BasicNameValuePair("ServiceType", "2"));
                nameValuePairs.add(new BasicNameValuePair("BillNumber", billno));
                nameValuePairs.add(new BasicNameValuePair("BillAmount", dueAmount));
                nameValuePairs.add(new BasicNameValuePair("ConNumber", consumerno));
                nameValuePairs.add(new BasicNameValuePair("BillMonth", billmonth));
                nameValuePairs.add(new BasicNameValuePair("ConsName", custname));
                nameValuePairs.add(new BasicNameValuePair("MobileNo", ""));


                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                request.setURI(new URI(url));
                HttpResponse response = client.execute(request);
                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

               /* HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet();
                request.setURI(new URI(url));
                HttpResponse response = client.execute(request);
                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);
                Log.e("Result", result);*/

            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSON(result);
        }
    }

    //Parse the JSON response
    void parseJSON(String result) {
        String status = "";
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(getApplicationContext(), "Recharge Failed Try after same time!", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        status = jo.getString("Status");
                    }//for

                    AlertDialog.Builder builder = new AlertDialog.Builder(ElectricityBillPayActivity.this);
                    builder.setTitle("Message");
                    builder.setIcon(R.mipmap.ic_launcher);
                    builder.setMessage("" + status);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                   /*double f = Double.parseDouble(balStatus);
                    balAmt=edtAmount.getText().toString();
					double a = Double.parseDouble(balAmt);
					double amt=f-a;
					String strAmt = Double.toString(amt);

					MainActivity.txtBalance.setText(strAmt);*/


                            //edtMobile.requestFocus();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());


            }

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(20000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    new LoadBalCheckTask().execute();
                }
            }).start();
        }
    }

    //Start asynchronous task for show Balance on dashboard
    public class LoadBalCheckTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "LoginUser.asmx/GetBalance";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }

            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            /*pDialog.setMessage("Please wait...");
                pDialog.setIndeterminate(true);
				pDialog.setCancelable(false);
				pDialog.show();*/
        }

        @Override
        protected String doInBackground(Void... params) {
            /*try {
                Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
            String result = null;
            url = url.replaceAll(" ", "%20");
            Log.e("url", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONLoadBalCheck(result);
        }
    }

    //Parse the JSON response
    void parseJSONLoadBalCheck(String result) {
        String status = "";
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(ElectricityBillPayActivity.this, "Wrong responce", Toast.LENGTH_LONG).show();
                } else {
                    //Received response from web service
                    //"Status": "489.7500"
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        status = jo.getString("Status");
                        //Log.e("bal",status);
                    }//for

                    MainActivity.txtBalance.setText(status);
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
                e.printStackTrace();
            }
        }
    }
    //End asynchronous task for show Balance on dashboard
}

