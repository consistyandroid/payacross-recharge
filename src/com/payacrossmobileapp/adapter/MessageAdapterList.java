package com.payacrossmobileapp.adapter;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.payacrossmobileapp.main.Common;
import com.payacrossmobileapp.main.R;


public class MessageAdapterList extends ArrayAdapter<String> {
    private final Context context;
    //Activity ac;
    public static int i;
    String[] mobno,status,operator,amount,transid,date,balance;
    //boolean withoutImageDayreport = false;
    //Button btnDispute;
    ProgressDialog pDialog;
    //private int mLastPosition;

    public MessageAdapterList(Context context, String[] status, String[] date, String[] operator, String[] amount, String[] transid,
                              String[] balance, String[] mobno) {
        super(context, R.layout.lastmessage_list_choice, status);
        this.context = context;
        this.status = status;
        this.date = date;
        this.operator = operator;
        this.amount = amount;
        this.transid = transid;
        this.balance = balance;
        this.mobno = mobno;
    }


    @SuppressLint("ViewHolder")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View rowView = null;
        try {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            TextView txtdate = null;


                rowView = inflater.inflate(R.layout.lastmessage_list_choice, parent, false);
                txtdate = (TextView) rowView.findViewById(R.id.txtdate);
                ImageView imgoperator = (ImageView) rowView.findViewById(R.id.imgoperator);
                TextView imgsuccess = (TextView) rowView.findViewById(R.id.imgsuccess);

                //input date from web service : 2/4/2016 1:40:07 AM
                txtdate.setText(Common.getTime(date[position], "M/d/yyyy h:mm:ss aaa", "dd-MMM-yyyy"));
                imgsuccess.setText(status[position]);
                setOpertorImage(imgoperator, operator[position]);

            TextView txttime = (TextView) rowView.findViewById(R.id.txttime);
            TextView txtoperator = (TextView) rowView.findViewById(R.id.txtoperator);
            TextView txtamount = (TextView) rowView.findViewById(R.id.txtamount);
            TextView txttransid = (TextView) rowView.findViewById(R.id.txttransid);
            TextView txtbalance = (TextView) rowView.findViewById(R.id.txtbalance);
            TextView txtmobno = (TextView) rowView.findViewById(R.id.txtmobno);
           // TextView imgsuccess = (TextView) rowView.findViewById(R.id.imgsuccess);
            LinearLayout linTop = (LinearLayout) rowView.findViewById(R.id.linTop);

            txtmobno.setBackgroundColor(Color.WHITE);
            txttransid.setBackgroundColor(Color.WHITE);
            txtamount.setBackgroundColor(Color.WHITE);
            txtbalance.setBackgroundColor(Color.WHITE);

            if (status[position].equalsIgnoreCase("SUCCESS")) {
                linTop.setBackgroundColor(Color.parseColor("#367113"));
            } else if (status[position].equalsIgnoreCase("FAIL")||status[position].equalsIgnoreCase("WAITING")||status[position].equalsIgnoreCase("WRONG NUMBER")) {
                linTop.setBackgroundColor(Color.parseColor("#f50300"));
            }

            if(status[position].equalsIgnoreCase("SUCCESS"))
            {
                imgsuccess.setTextColor(Color.parseColor("#0f9b12"));
            }
            else if(status[position].equalsIgnoreCase("FAIL"))
            {
                imgsuccess.setTextColor(Color.parseColor("#ff0000"));
            }

            txtoperator.setText(operator[position]);
            txtamount.setText(amount[position]);
            txttransid.setText(transid[position]);
            float bal = Float.parseFloat(balance[position]);
            txtbalance.setText(String.format("%.02f", bal));
            txtmobno.setText(mobno[position]);
            //input date from web service : 2/4/2016 12:00:00 AM
            txttime.setText(Common.getTime(date[position], "M/d/yyyy h:mm:ss aaa", "hh:mm aaa"));

        } catch (Exception e) {
            e.printStackTrace();
        }



        return rowView;
    }

    private void setOpertorImage(ImageView imgoperator, String operator) {
        if (operator.toLowerCase().contains("AIRCEL".toLowerCase())) {
            imgoperator.setImageResource(R.drawable.aircel_rgh);
        } else if (operator.toLowerCase().contains("AIRTELDTH".toLowerCase())) {
            imgoperator.setImageResource(R.drawable.airtel_dth);
        } else if (operator.toLowerCase().contains("AIRTEL".toLowerCase())) {
            imgoperator.setImageResource(R.drawable.airtel_rgh);
        } else if (operator.toLowerCase().contains("BSNL".toLowerCase())) {
            imgoperator.setImageResource(R.drawable.bsnl_rgh);
        } else if (operator.toLowerCase().contains("DOCOMO".toLowerCase())) {
            imgoperator.setImageResource(R.drawable.tatadocomo_rgh);
        } else if (operator.toLowerCase().contains("MTNL".toLowerCase())) {
            imgoperator.setImageResource(R.drawable.mtnl_logo);
        } else if (operator.toLowerCase().contains("RELIANCE".toLowerCase())) {
            imgoperator.setImageResource(R.drawable.reliancegsm_rgh);
        } else if (operator.toLowerCase().contains("UNINOR".toLowerCase())) {
            imgoperator.setImageResource(R.drawable.uninor);
        } else if (operator.toLowerCase().contains("VIDEOCON".toLowerCase())) {
            imgoperator.setImageResource(R.drawable.videocon_logo);
        } else if (operator.toLowerCase().contains("Idea".toLowerCase())) {
            imgoperator.setImageResource(R.drawable.idea_logo);
        } else if (operator.toLowerCase().contains("LOOP".toLowerCase())) {
            imgoperator.setImageResource(R.drawable.loop);
        } else if (operator.toLowerCase().contains("MTS".toLowerCase())) {
            imgoperator.setImageResource(R.drawable.mts_india);
        } else if (operator.toLowerCase().contains("TATAINDICOM".toLowerCase())) {
            imgoperator.setImageResource(R.drawable.tataindicom_logo);
        } else if (operator.toLowerCase().contains("VIRGIN".toLowerCase())) {
            imgoperator.setImageResource(R.drawable.virgin);
        } else if (operator.toLowerCase().contains("VODAFONE".toLowerCase())) {
            imgoperator.setImageResource(R.drawable.vodafon_rgh);
        } else if (operator.toLowerCase().contains("BIG".toLowerCase())) {
            imgoperator.setImageResource(R.drawable.bigtv_dth);
        } else if (operator.toLowerCase().contains("DISH".toLowerCase())) {
            imgoperator.setImageResource(R.drawable.dishtv_dth);
        } else if (operator.toLowerCase().contains("SUNDIRECT".toLowerCase())) {
            imgoperator.setImageResource(R.drawable.sun_dth);
        } else if (operator.toLowerCase().contains("TATASKY".toLowerCase())) {
            imgoperator.setImageResource(R.drawable.tatasky_dth);
        } else if (operator.toLowerCase().contains("VIDEOCON".toLowerCase())) {
            imgoperator.setImageResource(R.drawable.videocon_dth);
        } else {
            imgoperator.setImageResource(R.drawable.operater);
        }
    }
}
