package com.payacrossmobileapp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.payacrossmobileapp.main.R;

public class DownLineAdapterList extends ArrayAdapter<String>
{
	private final Context context;
	public static int i;
	String[] id,name,mobile,balance;
		
	public DownLineAdapterList(Context context, String[] id,String[] name,String[] mobile,String[] balance) 
	{
		super(context, R.layout.down_listitem, id);
		this.context = context;
		this.id = id;
		this.name = name;
		this.mobile = mobile;
		this.balance=balance;
	}



	@SuppressLint("ViewHolder") 
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) 
	{
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View rowView = inflater.inflate(R.layout.down_listitem, parent, false);
		
		TextView txtid = (TextView) rowView.findViewById(R.id.txtID);
	    TextView txtname = (TextView) rowView.findViewById(R.id.txtName);
		TextView txtmobile = (TextView) rowView.findViewById(R.id.txtMobile);
		TextView txtbalance = (TextView) rowView.findViewById(R.id.txtBalance);

		if (id[position] != null) 
		{
			txtid.setText(id[position]);
			txtname.setText(name[position]);
			txtmobile.setText(mobile[position]);
			txtbalance.setText(String.format("%.03f", Float.parseFloat(balance[position])));
			
			
			
		}
		
		
		/*if(position %2 ==0)
		{
			rowView.setBackgroundColor(Color.parseColor("#FDFDFE"));
		}
		else 
		{
			rowView.setBackgroundColor(Color.parseColor("#E7E7E8"));
		}*/
		return rowView;			
	}
	
}
