package com.payacrossmobileapp.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.payacrossmobileapp.main.Common;
import com.payacrossmobileapp.main.R;

public class newlast10adapter extends ArrayAdapter<String> {
    private final Context context;
    public static int i;
    String[] id, name, mobile, status, operator, amount, mobno, transid, date, balance;
    Button  btnOk;
    Dialog dialog;

    public newlast10adapter(Context context, String[] mobno, String[] status, String[] operator, String[] amount, String[] transid,
                            String[] date, String[] balance, boolean b) {
        super(context, R.layout.message_list_choice, mobno);
        this.context = context;
        this.mobno = mobno;
        this.status = status;
        this.operator = operator;
        this.date = date;
        this.amount = amount;
        this.transid = transid;
        this.balance = balance;

    }


    @SuppressLint("ViewHolder")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.message_list_choice, parent, false);

        // btnaction = (Button) rowView.findViewById(R.id.btnAction);
        TextView txtmobile = (TextView) rowView.findViewById(R.id.txtMobile);
        TextView txtStatus = (TextView) rowView.findViewById(R.id.txtstatus);
        TextView txtOperator = (TextView) rowView.findViewById(R.id.txtoperator);
        TextView txtBalance = (TextView) rowView.findViewById(R.id.txtBalance);
        // TextView txtaction = (TextView) rowView.findViewById(R.id.txtAction);
        if (mobno[position] != null) {
            txtmobile.setText(mobno[position]);
            txtStatus.setText(status[position]);
            txtOperator.setText(operator[position]);
            txtBalance.setText(String.format("%.03f", Float.parseFloat(amount[position])));
            //txtaction.setText(mobile[position]);
            if (position % 2 == 0) {
                rowView.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                //  rowView.setBackgroundColor(Color.parseColor("#FFFFFF"));
                // rowView.setBackgroundColor(Color.tiltle);
            } else {
                rowView.setBackgroundColor(getContext().getResources().getColor(R.color.spinnerbackcolor));
                //   rowView.setBackgroundColor(Color.parseColor("#FFFCB66F"));
            }

        }

        rowView.setClickable(true);
        rowView.setFocusable(true);

        rowView.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (dialog != null)
                    dialog = null;

                dialog = new Dialog(getContext());

                dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
                // dialog.requestWindowFeature(W)
                MessageAdapterList.i = position;
                dialog.setContentView(R.layout.last10transactionpopup);
                dialog.setTitle("Recharge Detail");
                dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);


                TextView txtMobno = (TextView) dialog.findViewById(R.id.txtmobno);
                TextView txtStatus = (TextView) dialog.findViewById(R.id.txtstatus);
                TextView txtTransid = (TextView) dialog.findViewById(R.id.txttransid);
                TextView txtdate = (TextView) dialog.findViewById(R.id.txtDate);
                TextView txtOpenBal = (TextView) dialog.findViewById(R.id.txtopenbal);
              /*  TextView txtSalePrice = (TextView) dialog.findViewById(R.id.txtsaleprice);
                TextView txtcomm = (TextView) dialog.findViewById(R.id.txtcomm);
                TextView txtclbal = (TextView) dialog.findViewById(R.id.txtclobal);
                btnOk = (Button) dialog.findViewById(R.id.btnok);
*/
                txtMobno.setText(mobno[position]);
                txtStatus.setText(status[position]);
                txtTransid.setText(transid[position]);
                txtdate.setText(Common.getTime(date[position], "M/d/yyyy h:mm:ss aaa", "dd-MMM-yyyy"));
                //txtOpenBal.setText(balance[position]);
                float opbal = Float.parseFloat(balance[position]);
                txtOpenBal.setText(String.format("%.02f", opbal));

                //txtcomm.setText(commission[position]);
                /*float comm = Float.parseFloat(commission[position]);
                txtcomm.setText(String.format("%.02f", comm));
                //txtdate.setText(date[position]);
                //txtSalePrice.setText(sellprice[position]);
                float saleprice = Float.parseFloat(sellprice[position]);
                txtSalePrice.setText(String.format("%.02f", saleprice));
                //  txtamount.setText(amount[position]);
                float amt = Float.parseFloat(closebalance[position]);
                txtclbal.setText(String.format("%.02f", amt));*/
                dialog.show();


                btnOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

            }

        });

        /*btnaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //btnDispute.setVisibility(View.VISIBLE);

                if (dialog != null)
                    dialog = null;

                dialog = new Dialog(getContext());

                dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
                // dialog.requestWindowFeature(W)
                MessageAdapterList.i = position;
                dialog.setContentView(R.layout.last10transactionpopup);
                dialog.setTitle("Recharge Detail");
                dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);


                TextView txtMobno = (TextView) dialog.findViewById(R.id.txtmobno);
                TextView txtStatus = (TextView) dialog.findViewById(R.id.txtstatus);
                TextView txtTransid = (TextView) dialog.findViewById(R.id.txttransid);
                TextView txtdate = (TextView) dialog.findViewById(R.id.txtDate);
                TextView txtOpenBal = (TextView) dialog.findViewById(R.id.txtopenbal);
                TextView txtSalePrice = (TextView) dialog.findViewById(R.id.txtsaleprice);
                TextView txtcomm = (TextView) dialog.findViewById(R.id.txtcomm);
                TextView txtclbal = (TextView) dialog.findViewById(R.id.txtclobal);
                btnOk = (Button) dialog.findViewById(R.id.btnok);

                txtMobno.setText(mobno[position]);
                txtStatus.setText(status[position]);
                txtTransid.setText(transid[position]);
                txtdate.setText(Common.getTime(date[position], "M/d/yyyy h:mm:ss aaa", "dd-MMM-yyyy"));
                //txtOpenBal.setText(balance[position]);
                float opbal = Float.parseFloat(balance[position]);
                txtOpenBal.setText(String.format("%.02f", opbal));

                //txtcomm.setText(commission[position]);
                float comm = Float.parseFloat(commission[position]);
                txtcomm.setText(String.format("%.02f", comm));
                //txtdate.setText(date[position]);
                //txtSalePrice.setText(sellprice[position]);
                float saleprice = Float.parseFloat(sellprice[position]);
                txtSalePrice.setText(String.format("%.02f", saleprice));
                //  txtamount.setText(amount[position]);
                float amt = Float.parseFloat(closebalance[position]);
                txtclbal.setText(String.format("%.02f", amt));
                dialog.show();


                btnOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });


            }
        });*/


        return rowView;
    }
}
