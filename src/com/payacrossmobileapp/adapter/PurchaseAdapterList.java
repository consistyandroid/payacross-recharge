package com.payacrossmobileapp.adapter;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.payacrossmobileapp.main.R;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PurchaseAdapterList extends ArrayAdapter<String>
{
	private final Context context;
	//Activity ac;
	public static int i;
	String[] strdate,remarks,credit,balance,debit,hours,prebalance;

	ProgressDialog pDialog;

	public PurchaseAdapterList(Context context,  String[] strdate, String[] remarks, String[] credit, String[] balance, String[] debit, String[] hours,String[] prebalance)
	{                                                                          
		super(context, R.layout.purchase_list_report, strdate);
		this.context = context;
		this.strdate= strdate;
		this.remarks= remarks;
		this.credit= credit;
		this.balance= balance;
		this.debit= debit;
		this.hours= hours;
		this.prebalance= prebalance;
	}



	@SuppressLint("ViewHolder") 
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) 
	{
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View rowView = inflater.inflate(R.layout.purchase_list_report, parent, false);

		final TextView txtdate = (TextView) rowView.findViewById(R.id.txtdate);
		TextView txttime = (TextView) rowView.findViewById(R.id.txttime);
		TextView txtremarks = (TextView) rowView.findViewById(R.id.txtremark);
		TextView txtdebit = (TextView) rowView.findViewById(R.id.txtdebit);
		TextView txtcredit= (TextView) rowView.findViewById(R.id.txtcredit);
		TextView txtbalance = (TextView) rowView.findViewById(R.id.txtbalance);
		TextView txtprebalance = (TextView) rowView.findViewById(R.id.txtprebal);
		//TextView txthours = (TextView) rowView.findViewById(R.id.txthrs);
		;
		/*if (status[position] != null) 
		{*/

		txtremarks.setText(remarks[position]);
		txtdate.setText(strdate[position]);

		txtdebit.setText(String.format("%.02f", Float.parseFloat(debit[position])));

		/*String strBal=balance[position];

		strBal=strBal.replaceAll(" ", "");
		float bal = Float.parseFloat(strBal);
		txtbalance.setText(String.format("%.02f", bal));*/
		txtbalance.setText(String.format("%.02f", Float.parseFloat(balance[position])));
		txtcredit.setText(String.format("%.02f", Float.parseFloat(credit[position])));
		txtprebalance.setText(String.format("%.02f", Float.parseFloat(prebalance[position])));
		//txthours.setText(hours[position]);

		String temp[]=strdate[position].split(" ");

		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

		String dateInString = temp[0].toString().trim();//"Jun 7, 2013";

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");

		try {

			Date date = formatter.parse(dateInString);

			txtdate.setText(sdf.format(date));//temp[0]
			
			txttime.setText(hours[position]);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (position % 2 == 0) {
			rowView.setBackgroundColor(getContext().getResources().getColor(R.color.faintblue));
			//  rowView.setBackgroundColor(Color.parseColor("#FFFFFF"));
			// rowView.setBackgroundColor(Color.tiltle);
		} else {
			// rowView.setBackgroundColor(getContext().getResources().getColor(R.color.LightPurple));
			rowView.setBackgroundColor(getContext().getResources().getColor(R.color.white));
		}

		return rowView;			
	}

}
