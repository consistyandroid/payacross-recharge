package com.payacrossmobileapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.payacrossmobileapp.main.Common;
import com.payacrossmobileapp.main.R;

import java.util.ArrayList;

public class ReportListAdaptor extends BaseAdapter {
    Context context;
    ArrayList<Report> report;

    public ReportListAdaptor(Context context, ArrayList<Report> report) {
        this.context = context;
        this.report = report;
    }

    @Override
    public int getCount() {
        return report.size();
    }

    @Override
    public Object getItem(int position) {
        return report.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Report rpt = report.get(position);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listitem, null);
        }
        TextView mobileNumber = (TextView) convertView.findViewById(R.id.txtMobileNumber);
        //input format : "M/d/yyyy H:mm:ss aa"


		 /*String temp[]=rpt.getMobileNumber().split(" ");


		 SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			String dateInString = temp[0].toString().trim();//"Jun 7, 2013";		
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
			try {
		 
				Date date = formatter.parse(dateInString);
				mobileNumber.setText(sdf.format(date));//temp[0]
			} catch (Exception e) {
				e.printStackTrace();
			}*/

        TextView operatorName = (TextView) convertView.findViewById(R.id.txtOpearatorName);
        operatorName.setText(rpt.getOperatorName());

        TextView time = (TextView) convertView.findViewById(R.id.txtTime);

        //"2/7/2016 6:57:23 PM"
        mobileNumber.setText(Common.getTime(rpt.getMobileNumber(), "M/d/yyyy hh:mm:ss aa", "dd-MMM-yyyy"));
        time.setText(Common.getTime(rpt.getMobileNumber(), "M/d/yyyy hh:mm:ss aa", "hh:mm:ss a"));

        return convertView;
    }
}
