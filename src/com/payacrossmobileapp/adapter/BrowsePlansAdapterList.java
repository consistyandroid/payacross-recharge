package com.payacrossmobileapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.payacrossmobileapp.main.R;

public class BrowsePlansAdapterList extends ArrayAdapter<String> {
    private final Context context;
    String strAmount;
    String[] amount, validity, description;

    public BrowsePlansAdapterList(Context context, String[] amount, String[] validity, String[] description) {
        super(context, R.layout.activity_browse_plans_adapter_list, amount);
        this.context = context;
        this.amount = amount;
        this.validity = validity;
        this.description = description;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.activity_browse_plans_adapter_list, parent, false);

        TextView txtValidity = (TextView) rowView.findViewById(R.id.txtValidity);
        TextView txtDescription = (TextView) rowView.findViewById(R.id.txtDiscription);
        final TextView textAmount = (TextView) rowView.findViewById(R.id.textAmount);

        textAmount.setText(amount[position]);
        txtValidity.setText(validity[position]);
        txtDescription.setText(description[position]);

        return rowView;
    }

}
