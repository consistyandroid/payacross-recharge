package com.payacrossmobileapp.adapter;

public class Report 
{
	private String mobileNumber;
	private String opeartorName;
	
	public Report(String mobileNumber,String operatorName)
	{
		super();
		this.mobileNumber=mobileNumber;
		this.opeartorName=operatorName;
		
	}
	//get data
	public String getMobileNumber()
	{
		return mobileNumber;
	}
	public String getOperatorName()
	{
		return opeartorName;
	}
	
	//set data
	public void setMobileNumber(String mobileNumber)
	{
		this.mobileNumber=mobileNumber;
	}
	public void setOperatorName(String operatorName)
	{
		this.opeartorName=operatorName;
	}
	
}
