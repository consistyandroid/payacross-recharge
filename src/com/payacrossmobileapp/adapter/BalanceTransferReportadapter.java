package com.payacrossmobileapp.adapter;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.payacrossmobileapp.main.R;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Admin on 3/5/2016.
 */
public class BalanceTransferReportadapter extends ArrayAdapter<String> {
    private final Context context;
    //Activity ac;
    public static int i;
    String[] strdate, remarks, balance, sender, reciver, amount;

    ProgressDialog pDialog;

    public BalanceTransferReportadapter(Context context, String[] strdate, String[] remarks, String[] balance,
                                        String[] sender, String[] reciver, String[] amount) {
        super(context, R.layout.balancetransferreport, strdate);
        this.context = context;
        this.strdate = strdate;
        this.remarks = remarks;
        this.balance = balance;
        this.sender = sender;
        this.reciver = reciver;
        this.amount = amount;


    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.balancetransferreport, parent, false);

        final TextView txtdate = (TextView) rowView.findViewById(R.id.txtdate);
        final TextView txttime = (TextView) rowView.findViewById(R.id.txttime);
        TextView txtremarks = (TextView) rowView.findViewById(R.id.txtremark);
        TextView txtbalance = (TextView) rowView.findViewById(R.id.txtcredit);
        TextView txtsender = (TextView) rowView.findViewById(R.id.txtsender);
        TextView txtreciver = (TextView) rowView.findViewById(R.id.txtreciver);
        TextView txtamount = (TextView) rowView.findViewById(R.id.txtdebit);

        /*if (status[position] != null)
        {*/
        txtdate.setTextColor(getContext().getResources().getColor(R.color.blueforall));
        txttime.setTextColor(getContext().getResources().getColor(R.color.blueforall));
        txtremarks.setText(remarks[position]);
        txtdate.setText(strdate[position]);
        txtsender.setText(sender[position]);
        txtreciver.setText(reciver[position]);
        txtamount.setText(String.format("%.02f", Float.parseFloat(amount[position])));

        if (balance[position].equals("")) {
            txtbalance.setText("0.00");
        } else {
            txtbalance.setText(String.format("%.02f", Float.parseFloat(balance[position])));
        }

        //String temp[] = strdate[position].split(" ");


        SimpleDateFormat inFormatter = new SimpleDateFormat("dd MMM yyyy hh:mm:ss");
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd MMM yyyy");
        SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mm aaa");

        try {
            Date dateVal = inFormatter.parse(strdate[position]);
            txtdate.setText(sdfDate.format(dateVal));
            txttime.setText(sdfTime.format(dateVal));

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (position % 2 == 0) {
            rowView.setBackgroundColor(getContext().getResources().getColor(R.color.faintblue));
            //  rowView.setBackgroundColor(Color.parseColor("#FFFFFF"));
            // rowView.setBackgroundColor(Color.tiltle);
        } else {
            // rowView.setBackgroundColor(getContext().getResources().getColor(R.color.LightPurple));
            rowView.setBackgroundColor(getContext().getResources().getColor(R.color.white));
        }
        return rowView;
    }
}