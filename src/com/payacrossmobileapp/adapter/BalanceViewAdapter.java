package com.payacrossmobileapp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.payacrossmobileapp.main.R;

public class BalanceViewAdapter extends ArrayAdapter<String> {
private  Context context;
public static int i;
        String[] name, balance;

public BalanceViewAdapter(Context context, String[] name, String[] balance) {
        super(context, R.layout.balanceviewlistitem, name);
        this.context = context;
        this.name = name;
        this.balance = balance;
        }

@SuppressLint("ViewHolder")
@Override
public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.balanceviewlistitem, parent, false);

        TextView txtname = (TextView) rowView.findViewById(R.id.txtName);
        TextView txtbalance = (TextView) rowView.findViewById(R.id.txtBalance);


        txtname.setText(name[position]);
        txtbalance.setText(String.format("%.02f", Float.parseFloat(balance[position])));



        if (position % 2 == 0) {
        rowView.setBackgroundColor(Color.parseColor("#FFFFFF"));

        } else {
        rowView.setBackgroundColor(Color.parseColor("#FFFCB66F"));
        }
        return rowView;
        }

        }