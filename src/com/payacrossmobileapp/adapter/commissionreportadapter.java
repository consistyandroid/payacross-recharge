package com.payacrossmobileapp.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.payacrossmobileapp.main.R;
import com.payacrossmobileapp.report.DayReportActivity;

public class commissionreportadapter extends ArrayAdapter<String> {
    private final Context context;
    public static int i;
    String[] sellprice, commission;

    Dialog dialog;


    public commissionreportadapter(Context context, String[] sellprice, String[] commission) {
        super(context, R.layout.activity_commissionreportadapter, sellprice);
        this.context = context;
        this.sellprice = sellprice;
        this.commission = commission;

    }


    @SuppressLint("ViewHolder")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.activity_commissionreportadapter, parent, false);

        TextView txtStatus = (TextView) rowView.findViewById(R.id.txtstatus);
        TextView txtcommission = (TextView) rowView.findViewById(R.id.txtcommission);

        if (sellprice[position] != null) {
            txtStatus.setText(sellprice[position]);
            txtcommission.setText(commission[position]);

        }
        Button btnview = (Button) rowView.findViewById(R.id.btnview);
        btnview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), DayReportActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("type", "GPRS");
                //bundle.putString("rechtype", "mobile");
                i.putExtras(bundle);

                getContext().startActivity(i);

            }
        });

        return rowView;
    }
}
