package com.payacrossmobileapp.adapter;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.payacrossmobileapp.main.R;

public class electricityadapterlistitem extends ArrayAdapter<String>
{
    private final Context context;
    //Activity ac;
    public static int i;
    String[] status,date,amount,transid,CustomerName,ConsumerNumber,servicetype,mobno;

    ProgressDialog pDialog;

    public electricityadapterlistitem(Context context, String[] status,String[] date,String[] servicetype,String[] amount,
                                      String[] ConsumerNumber,String[] CustomerName,String[] transid)
    {
        super(context, R.layout.activity_electricityadapterlistitem, status);
        this.context = context;
        this.status = status;
        this.date = date;
        this.servicetype = servicetype;
        this.amount = amount;
        this.ConsumerNumber = ConsumerNumber;
        // this.statmentid = statmentid;
        //this.mobno= mobno;
        this.CustomerName= CustomerName;
        this.transid= transid;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.activity_electricityadapterlistitem, parent, false);

        if (position % 2 == 0) {
            rowView.setBackgroundColor(Color.parseColor("#FDFDFE"));
        } else {
            rowView.setBackgroundColor(Color.parseColor("#E7E7E8"));
        }

        TextView txtstatus = (TextView) rowView.findViewById(R.id.txtstatus);
        final TextView txtdate = (TextView) rowView.findViewById(R.id.txtdate);
        TextView txtoperator = (TextView) rowView.findViewById(R.id.txtoperator);
        TextView txtamount = (TextView) rowView.findViewById(R.id.txtamount);
        TextView txttransid = (TextView) rowView.findViewById(R.id.txttransid);
        TextView txtbalance = (TextView) rowView.findViewById(R.id.txtbalance);
        //TextView txtdetail = (TextView) rowView.findViewById(R.id.txtdetail);
        //final TextView txtcnumber = (TextView) rowView.findViewById(R.id.txtcnumber);
        TextView txtmobno = (TextView) rowView.findViewById(R.id.txtmobno);
        //btnDispute = (Button) rowView.findViewById(R.id.btn_dispute);
        //btnDispute.setTag(position);

        //btnDispute.setVisibility(View.INVISIBLE);

        if (status[position] != null) {
            txtstatus.setText(CustomerName[position]);
            txtdate.setText(date[position]);
            txtoperator.setText(servicetype[position]);
            txtamount.setText(amount[position]);
            txttransid.setText(transid[position]);
            txtbalance.setText(ConsumerNumber[position]);
            // txtdetail.setText(CustomerName[position]);
            //txtcnumber.setText(ConsumerNumber[position]);

            txtmobno.setText(status[position]);
        }
        return rowView;
    }


}
