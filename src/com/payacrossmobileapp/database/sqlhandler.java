package com.payacrossmobileapp.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class sqlhandler {
    public static final String DATABASE_NAME = "OperatorDb";
    public static final int DATABASE_VERSION = 1;
    Context context;
    SQLiteDatabase sqlDatabase;
    sqlDbHelper dbHelper;

    public sqlhandler(Context context) {
        dbHelper = new sqlDbHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
        sqlDatabase = dbHelper.getWritableDatabase();
    }

    public void executeQuery(String query) {
        try {
            if (!sqlDatabase.isOpen()) {
                sqlDatabase = dbHelper.getWritableDatabase();
            }
            sqlDatabase.execSQL(query);
        } catch (Exception e) {
            System.out.println("DATABASE ERROR " + e);
        }
    }

    public Cursor selectQuery(String query) {
        Cursor c1 = null;
        try {
            if (!sqlDatabase.isOpen()) {
                sqlDatabase = dbHelper.getWritableDatabase();
            }
            c1 = sqlDatabase.rawQuery(query, null);
        } catch (Exception e) {
            System.out.println("DATABASE ERROR " + e);
        }
        return c1;
    }
}