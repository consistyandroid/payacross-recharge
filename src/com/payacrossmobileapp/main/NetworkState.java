package com.payacrossmobileapp.main;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Admin on 09 Jan 2017.
 */

public class NetworkState {




    public boolean isInternetAvailable(Context context) {
        boolean result = false;
        ConnectivityManager check = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (check != null) {
            NetworkInfo.State mobile = check.getNetworkInfo(0).getState();
            //wifi
            NetworkInfo.State wifi = check.getNetworkInfo(1).getState();
            if (mobile == NetworkInfo.State.CONNECTED || wifi == NetworkInfo.State.CONNECTED) {
                result = true;
            }
        }

        return result;
    }


}
