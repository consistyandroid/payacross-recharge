package com.payacrossmobileapp.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.payacrossmobileapp.adapter.BrowsePlansAdapterList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class BrowsePlanList extends Fragment {
    ProgressDialog pDialog;
    String strUrl, plan, opName, cirName, itemString, mobileNumber, operatortype, type;
    String[] strAmount, strValidity, strDescription;
    Bundle extras;
    ListView listView;
    int circlePos = 0;
    Bundle b;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View viewPlans = inflater.inflate(R.layout.activity_browse_plan_list, container, false);

        listView = (ListView) viewPlans.findViewById(R.id.listView);

        if (pDialog != null) {
            pDialog = null;
        }

        pDialog = new ProgressDialog(getActivity());

        b = getArguments();
        plan = b.getString("plans");

        extras = getActivity().getIntent().getExtras();
        opName = extras.getString("opName");
        cirName = extras.getString("cirName");
        mobileNumber = extras.getString("mobileNumber");
        circlePos = extras.getInt("cirPos");
        operatortype = extras.getString("rechtype");
        type = extras.getString("type");
        new BrowsPlansTask().execute();


        return viewPlans;
    }


    //-------------------------Do BrowsPlansTask Webservices
    private class BrowsPlansTask extends AsyncTask<Void, Void, String> {
        HttpURLConnection urlConnection;

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }
            return out.toString();
        }


        @Override
        protected String doInBackground(Void... params) {

            String result = null;

            try {
                strUrl ="http://www.esmartrc.com/RechargePlan.asmx/GetRechargePlan1";

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();

                List nameValuePairs = new ArrayList();
                // add an HTTP variable and value pair
                nameValuePairs.add(new BasicNameValuePair("MobileNo", mobileNumber));
                nameValuePairs.add(new BasicNameValuePair("OperatorIRefName", opName));
                nameValuePairs.add(new BasicNameValuePair("StateIRefName", cirName));

                android.util.Log.i("info of browse plan", opName);
                android.util.Log.i("circle of browse plan", cirName);

                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                request.setURI(new URI(strUrl));
                HttpResponse response = client.execute(request);
                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
            }
            return String.valueOf(result);

        }

        @Override
        protected void onPostExecute(String result) {

            pDialog.hide();
            parseResult(result);

        }
    }

    void parseResult(String result) {
        if (result != null) {
            try {
                // JSONObject jsonObject;
                //JSONArray jsonArray = new JSONArray(result).getJSONObject("jsonObject").getJSONArray("Topup");
                JSONArray jsonArray = new JSONArray(result);

                android.util.Log.i("Result of browse plan", result);

                try {
                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        android.util.Log.i("Result of browse plan", jsonObject.toString());

                        if (plan.equalsIgnoreCase("full") || plan.equalsIgnoreCase("talktime")) {
                            JSONArray jsonArray1 = jsonObject.getJSONArray("Topup");

                            strAmount = new String[jsonArray1.length()];
                            strValidity = new String[jsonArray1.length()];
                            strDescription = new String[jsonArray1.length()];

                            for (int j = 0; j < jsonArray1.length(); j++) {
                                try {
                                    JSONArray jsonArray2 = jsonArray1.getJSONArray(j);

                                    strAmount[j] = jsonArray2.getString(0);
                                    strValidity[j] = jsonArray2.getString(1);

                                    strDescription[j] = jsonArray2.getString(3);
                                } catch (Exception e) {

                                }
                            }

                        } else if (plan.equalsIgnoreCase("2g")) {
                            JSONArray jsonArray1 = jsonObject.getJSONArray("TwoG");
                            strAmount = new String[jsonArray1.length()];
                            strValidity = new String[jsonArray1.length()];
                            strDescription = new String[jsonArray1.length()];
                            for (int j = 0; j < jsonArray1.length(); j++) {
                                try {
                                    JSONArray jsonArray2 = jsonArray1.getJSONArray(j);
                                    strAmount[j] = jsonArray2.getString(0);
                                    strValidity[j] = jsonArray2.getString(1);
                                    strDescription[j] = jsonArray2.getString(3);
                                } catch (Exception e) {

                                }
                            }
                        } else if (plan.equalsIgnoreCase("3g")) {
                            JSONArray jsonArray1 = jsonObject.getJSONArray("ThreeG");
                            strAmount = new String[jsonArray1.length()];
                            strValidity = new String[jsonArray1.length()];
                            strDescription = new String[jsonArray1.length()];
                            for (int j = 0; j < jsonArray1.length(); j++) {
                                try {
                                    JSONArray jsonArray2 = jsonArray1.getJSONArray(j);
                                    strAmount[j] = jsonArray2.getString(0);
                                    strValidity[j] = jsonArray2.getString(1);
                                    strDescription[j] = jsonArray2.getString(3);
                                } catch (Exception e) {

                                }
                            }
                        } else if (plan.equalsIgnoreCase("Local")) {
                            JSONArray jsonArray1 = jsonObject.getJSONArray("Local");
                            strAmount = new String[jsonArray1.length()];
                            strValidity = new String[jsonArray1.length()];
                            strDescription = new String[jsonArray1.length()];
                            for (int j = 0; j < jsonArray1.length(); j++) {
                                try {
                                    JSONArray jsonArray2 = jsonArray1.getJSONArray(j);
                                    strAmount[j] = jsonArray2.getString(0);
                                    strValidity[j] = jsonArray2.getString(1);
                                    strDescription[j] = jsonArray2.getString(3);
                                } catch (Exception e) {

                                }
                            }
                        } else if (plan.equalsIgnoreCase("STD")) {
                            JSONArray jsonArray1 = jsonObject.getJSONArray("STD");
                            strAmount = new String[jsonArray1.length()];
                            strValidity = new String[jsonArray1.length()];
                            strDescription = new String[jsonArray1.length()];
                            for (int j = 0; j < jsonArray1.length(); j++) {
                                try {
                                    JSONArray jsonArray2 = jsonArray1.getJSONArray(j);
                                    strAmount[j] = jsonArray2.getString(0);
                                    strValidity[j] = jsonArray2.getString(1);
                                    strDescription[j] = jsonArray2.getString(3);
                                } catch (Exception e) {

                                }
                            }
                        } else if (plan.equalsIgnoreCase("ISD")) {
                            JSONArray jsonArray1 = jsonObject.getJSONArray("ISD");
                            strAmount = new String[jsonArray1.length()];
                            strValidity = new String[jsonArray1.length()];
                            strDescription = new String[jsonArray1.length()];
                            for (int j = 0; j < jsonArray1.length(); j++) {
                                try {
                                    JSONArray jsonArray2 = jsonArray1.getJSONArray(j);
                                    strAmount[j] = jsonArray2.getString(0);
                                    strValidity[j] = jsonArray2.getString(1);
                                    strDescription[j] = jsonArray2.getString(3);
                                } catch (Exception e) {

                                }
                            }
                        } else if (plan.equalsIgnoreCase("other")) {
                            JSONArray jsonArray1 = jsonObject.getJSONArray("Other");
                            strAmount = new String[jsonArray1.length()];
                            strValidity = new String[jsonArray1.length()];
                            strDescription = new String[jsonArray1.length()];
                            for (int j = 0; j < jsonArray1.length(); j++) {
                                try {
                                    JSONArray jsonArray2 = jsonArray1.getJSONArray(j);
                                    strAmount[j] = jsonArray2.getString(0);
                                    strValidity[j] = jsonArray2.getString(1);
                                    strDescription[j] = jsonArray2.getString(3);
                                } catch (Exception e) {

                                }
                            }
                        }

                    }

                    BrowsePlansAdapterList adapter = new BrowsePlansAdapterList(getActivity(), strAmount, strValidity, strDescription);
                    listView.setAdapter(adapter);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            itemString = strAmount[position].replaceAll("Rs. ", "");
                            Common.backFromBrowse = true;
                            Intent i = new Intent(getActivity(), MainActivity.class);
                            //i.putExtra("amount", itemString);
                            //i.putExtra("mobileNumber", mobileNumber);
                            i.putExtra("opName", opName);
                            i.putExtra("cirName", cirName);
                            i.putExtra("cirPos", circlePos);
                            // i.putExtra("type",type);
                            //  i.putExtra("rechtype",operatortype);
                            Bundle bundle = new Bundle();
                            bundle.putString("type", "GPRS");
                            //bundle.putString("rechtype", "mobile");
                            i.putExtras(bundle);

                            startActivity(i);
                            getActivity().finish();
                        }
                    });


                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (JSONException e) {
                android.util.Log.e("JSONError", e.getMessage());
            }
        }
    }
}