package com.payacrossmobileapp.main;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Common extends BroadcastReceiver {
    static String playurl = "https://play.google.com/store/apps/details?id=com.payacrossmobileapp.main";

    public static String COMMON_URL = "http://payacross.in/WebServiceses/";
    public static String signalRURL = "http://payacross.in/";
    public static String contact;

    public static String user_name, name, regid, recharge_id, emailid, mobileno,
            usertype, parentid, IMEI, lapuid, usertypeotomax, usercode, loginId,recentchanges,updateddate,versioncode;

    public static int resCount;

    public static boolean internet_status = false, report = false;
    static boolean operator_status = false;
    public static Map<String, String> autoOperatorMap = new HashMap<String, String>();
    public static String fonts = "fonts/Roboto-Bold.ttf";

    public static String pre_spinnername[] = null, pre_spinnercode[] = null,
            dth_spinnername[] = null, dth_spinnercode[] = null,
            data_spinnername[] = null, data_spinnercode[] = null,
            post_spinnername[] = null, post_spinnercode[] = null,
            preSmsCode[] = null, dthSmsCode[] = null,
            dataSmsCode[] = null, postSmsCode[] = null,
            special_spinnername[] = null, special_spinnercode[] = null,
            specialSmsCode[] = null, circle_code[] = null, circle_name[] = null;
    public static boolean loginflag;
    public static boolean loginflag1;
    public static boolean backFromBrowse = false;
    
    public static String[] day_spinnername = {"Select Day", "Today", "Two Days", "Five Days", "Ten Days"};
    public static String day_spinnercode[] = {"-1", "0", "1", "5", "9"};

    public static String[] service_name = {"Select Service", "Chhattisgarh State Power"};
   // public static String service_code[] = { "0", "PGS", "TPS", "UGS"};

    public static String[] circle_spinnername = {"Select circle", "Andhra Pradesh & Telangana", "Assam", "Bihar & Jharkhand",
            "Chennai", "Delhi NCR", "Gujarat", "Himachal Pradesh", "Haryana", "Jammu & Kashmir",
            "Karnataka", "Kerala", "Kolkata", "Maharashtra & Goa", "Madhya Pradesh & Chhattisgadh",
            "Mumbai", "North East", "Odisha", "Punjab", "Rajasthan", "Tamil Nadu", "Uttar Pradesh East",
            "Uttar Pradesh West & Uttarakhand", "West Bengal"};

    public static String circle_spinnervalue[] = {"-1", "andra-pradesh-telangana", "assam", "bihar-jharkhand",
            "chennai", "delhi-ncr", "gujarat", "himachal-pradesh", "haryana", "jammu-kashmir",
            "karnataka", "kerala", "kolkata", "maharashtra-goa", "madhya-pradesh-chhattisgarh",
            "mumbai", "north-east", "odisha", "punjab", "rajasthan", "tamil-nadu", "uttar-pradesh-east",
            "uttar-pradesh-west-uttarakhand", "west-bengal"};

    public static void checkInternetConenction(Context context) {
        internet_status = false;
        ConnectivityManager check = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (check != null) {
            NetworkInfo[] info = check.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        internet_status = true;
                    }
                }
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        checkInternetConenction(context);
    }

    public static void show(Context context, String msg) {
        try {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    context);
            alertDialogBuilder.setTitle("Message");
            alertDialogBuilder.setIcon(R.mipmap.ic_launcher);
            alertDialogBuilder
                    .setMessage(msg)
                    .setCancelable(false)
                    .setNegativeButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        } catch (Exception t) {
            Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public static String getTime(String time, String inFormat, String outFormat) {
        String out = "";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inFormat);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outFormat);
        try {
            //time = time.replace("AM", "am").replace("PM", "pm").replaceAll("\"","");
            time = time.replaceAll("\"", "");
            Date date = inputFormat.parse(time);
            out = outputFormat.format(date);

            //Log.i("out Date time", out);
        } catch (ParseException e) {
            Log.i("ParseException Error ", e.toString());
        } catch (Exception e) {
            Log.i("Error in date", e.toString());
        }
        return out;
    }


}

