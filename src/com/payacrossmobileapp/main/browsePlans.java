package com.payacrossmobileapp.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class browsePlans extends Activity {
    Spinner sp_Operator, sp_Circle;
    TextView txtBrowseplan;
String type,operatortype,opcode,circlecode,opName,cirName;
    Context localcont;
    Context context=this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_plans);
        sp_Operator = (Spinner) findViewById(R.id.sp_operator);
        sp_Circle = (Spinner) findViewById(R.id.spcircle);
        txtBrowseplan = (TextView) findViewById(R.id.txtBrowsePlan);

        Bundle extras = getIntent().getExtras();
        type = extras.getString("type");


        operatortype = extras.getString("rechtype");


            ArrayAdapter<String> mobile = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Common.pre_spinnername);
            mobile.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sp_Operator.setAdapter(mobile);


        sp_Operator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View vi2ew, int position, long id) {

                    opcode = Common.pre_spinnercode[position];

            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        ArrayAdapter<String> circle = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Common.circle_spinnername);
        circle.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_Circle.setAdapter(circle);


        sp_Circle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View vi2ew, int position, long id) {
                circlecode = Common.circle_spinnervalue[position];
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        txtBrowseplan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                opName = sp_Operator.getSelectedItem().toString().toLowerCase();
                cirName = circlecode.toLowerCase();

                //mobileNumber = edtMobile.getText().toString();


                    if (sp_Operator.getSelectedItemPosition()==0 ) {
                        Toast.makeText(context, "Please select operator ", Toast.LENGTH_SHORT).show();
                        sp_Operator.requestFocus();
                        //return false;
                    }
                    if (sp_Circle.getSelectedItemPosition() ==0) {
                        Toast.makeText(context, "Please select circle", Toast.LENGTH_SHORT).show();
                        sp_Circle.requestFocus();
                       // return false;
                    }
                    Intent brows = new Intent(browsePlans.this, BrowsePlan.class);
                    Bundle bundle = new Bundle();
                    android.util.Log.i("Before selection ", String.format("Op : %s, Cir: %s, Flag : %b", opName, cirName, Common.backFromBrowse));
                    bundle.putString("opName", opName);
                    bundle.putString("cirName", cirName);
                   //bundle.putString("mobileNumber", mobileNumber);
                    brows.putExtras(bundle);
                   startActivity(brows);
                    finish();


            }
        });

    }
    private boolean browsePlanValidation() {

        if (sp_Operator.getSelectedItemPosition()<=0) {
            Toast.makeText(localcont, "Please select operator ", Toast.LENGTH_SHORT).show();
            sp_Operator.requestFocus();
            return false;
        }
        if (sp_Circle.getSelectedItemPosition() <=0) {
            Toast.makeText(localcont, "Please select circle", Toast.LENGTH_SHORT).show();
            sp_Circle.requestFocus();
            return false;
        }
        return true;
    }
}
