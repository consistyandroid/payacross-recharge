package com.payacrossmobileapp.main;
import android.content.Context;
import android.content.Intent;
public final class CommonUtilities
{
	/**
	 * Base URL of the Demo Server (such as http://my_host:8080/gcm-demo)
	 */
	/*static final String SERVER_URL = "http://10.0.2.2:10739/WebService1.asmx/PostRegId";*/
	//static final String SERVER_URL ="http://demo1stop.onestoponlines.com/WebForm1.aspx/AIzaSyAFr7Ld4xkURdXXAQkXE3SDKY0_ig6a74A";
	
	static final String SERVER_URL =Common.COMMON_URL+"updateAndroidDeviceIDForGCMNotification.asmx/updateDeviceId";
	/**
	 * Google API project id registered to use GCM.
	 */
	static final String SENDER_ID = "1086154728425"; //540131829352
	/**
	 * Tag used on log messages.
	 */
	static final String TAG = "Sample";
	/**
	 * Intent used to display a message in the screen.
	 */
	static final String DISPLAY_MESSAGE_ACTION = "android.notification.DISPLAY_MESSAGE";
	/**
	 * Intent's extra that contains the message to be displayed.
	 */
	static final String EXTRA_MESSAGE = "message";
	/**
	 * Notifies UI to display a message.
	 * <p>
	 * This method is defined in the common helper because it's used both by the
	 * UI and the background service.
	 * 
	 * @param context
	 *            application's context.
	 * @param message
	 *            message to be displayed.
	 */
	static void displayMessage(Context context, String message) 
	{
		Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
		intent.putExtra(EXTRA_MESSAGE, message);
		context.sendBroadcast(intent);
	}
}
