package com.payacrossmobileapp.main;

import com.payacrossmobileapp.main.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

public class NotificationActivity extends Activity 
{
	//public static TextView display;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notification);
		Log.e("inside the activity ","...");
		//display=(TextView)findViewById(R.id.display);

		 String message = getIntent().getStringExtra(CommonUtilities.EXTRA_MESSAGE);
         showDialog(this, message, new DialogInterface.OnClickListener() {
             @Override
             public void onClick(DialogInterface dialog, int which) {
                 finish();
             }
         });
	}


	private void showDialog(Context context, String message,
            DialogInterface.OnClickListener onOkClick) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle("Alert Notification");

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting Icon to Dialog
        // alertDialog.setIcon(R.drawable.tick);

        // Setting OK Button
        alertDialog.setButton("OK", onOkClick);

        // Showing Alert Message
        alertDialog.show();
    }
}
