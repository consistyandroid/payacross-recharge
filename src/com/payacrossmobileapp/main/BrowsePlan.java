package com.payacrossmobileapp.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TabHost;

import com.payacrossmobileapp.recharge.MobileActivity;

import static android.graphics.Color.RED;

public class BrowsePlan extends FragmentActivity implements TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener {
    private TabPagerAdapter mAdapter;
    private ViewPager mViewPager;
    private TabHost mTabHost;
    Spinner sp_Operator, sp_Circle;
    String[] strAmount, strValidity, strDescription;
    ProgressDialog pDialog;
    String strUrl, plan, opName, cirName, itemString, mobileNumber;
    int pos;
    Button btnShow;
    String opcode, circlecode,type,operatortype;
    ListView listView;
    Bundle extras;
    static int check = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browseplan);
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        Bundle extras = getIntent().getExtras();
        type = extras.getString("type");
        operatortype = extras.getString("rechtype");
        // Tab Initialization

        initialiseTabHost();

        //FragmentManager fm = getChildFragmentManager();

        mAdapter = new TabPagerAdapter(getSupportFragmentManager());

        // Fragments and ViewPager Initialization
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOnPageChangeListener((ViewPager.OnPageChangeListener) BrowsePlan.this);

    }

    // Method to add a TabHost
    private static void AddTab(BrowsePlan activity, TabHost tabHost, TabHost.TabSpec tabSpec) {
        tabSpec.setContent(new MyTabFactory(activity));
        tabHost.addTab(tabSpec);

    }

    // Manages the Tab changes, synchronizing it with Pages
    public void onTabChanged(String tag) {
        // setTabColor(mTabHost);
        pos = this.mTabHost.getCurrentTab();
        this.mViewPager.setCurrentItem(pos);


    }

    private void setTabColor(TabHost mTabHost) {
        for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
            if (mTabHost.getTabWidget().getChildAt(i).isSelected()) {
                // SessionState.getSelectedIndex().setSelectedPage(i);
                mTabHost.getTabWidget()
                        .getChildAt(i)
                        .setBackgroundResource(
                                R.drawable.tab_selected_background);
            }
        }
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
    }

    // Manages the Page changes, synchronizing it with Tabs
    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        int pos = this.mViewPager.getCurrentItem();
        this.mTabHost.setCurrentTab(pos);
    }

    @Override
    public void onPageSelected(int pos) {
        check = pos;
        android.util.Log.i("Tab Position ", pos + "");
    }


    // Tabs Creation
    private void initialiseTabHost() {
        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup();

        // TODO Put here your Tabs
        BrowsePlan.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("Full").setIndicator("Full"));
        BrowsePlan.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("TalkTime").setIndicator("TalkTime"));
        BrowsePlan.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("2G").setIndicator("2G"));
        BrowsePlan.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("3G").setIndicator("3G"));
        BrowsePlan.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("Local").setIndicator("Local"));
        BrowsePlan.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("STD").setIndicator("STD"));
        BrowsePlan.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("ISD").setIndicator("ISD"));
        BrowsePlan.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("Other").setIndicator("Other"));

        mTabHost.setOnTabChangedListener(this);
        mTabHost.getTabWidget().getChildAt(pos).setBottom(RED);
    }

    @Override
    public void onBackPressed() {

        Intent homeintent = new Intent(BrowsePlan.this, MainActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("type", "GPRS");
       // bundle.putString("rechtype", "Mobile");
        homeintent.putExtras(bundle);
        homeintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeintent);
        finish();



    }
}
