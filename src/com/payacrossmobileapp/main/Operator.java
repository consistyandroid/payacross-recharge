package com.payacrossmobileapp.main;

import android.content.Context;
import android.database.Cursor;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by Somnath on 2/16/2016.
 */
public class Operator {
    com.payacrossmobileapp.database.sqlhandler sqlhandler;
    Context cxt;

    public Operator(com.payacrossmobileapp.database.sqlhandler sqlhandler, Context cxt) {
        this.sqlhandler = sqlhandler;
        this.cxt = cxt;
        try {
            start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // get current count of operator from sqlite
    private int getCount() {
        int count = -1;
        try {
            String query = "SELECT count(1) FROM Operator";
            Cursor c1 = sqlhandler.selectQuery(query);
            c1.moveToFirst();
            count = c1.getInt(0);
            c1.close();
        } catch (Exception t) {
            Toast.makeText(cxt, "Loading error" + t.getMessage(), Toast.LENGTH_LONG).show();
            count = -1;
        } finally {

        }
       // Toast.makeText(cxt, "Operator count : " + count, Toast.LENGTH_LONG).show();
        return count;
    }

    private void start() throws InterruptedException {
        if (getCount() <= 0) {
            getOperatorFromWebService();

        } else {
            readOperatorFromSQLite();
        }
        Log.i("Operator Loading","Completed");
    }

    //Get Operator from Web service
    private void getOperatorFromWebService() {
        try {
            String url = Common.COMMON_URL + "GPRSOperators.asmx/GetOperatorByService";

            String query1 = "DELETE FROM Operator";
            sqlhandler.executeQuery(query1);

            setFirstValue();
            JsonArrayRequest req = new JsonArrayRequest(url,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            if (response.length() == 0) {
                                //Toast.makeText(getApplicationContext(), "Error on webservice",Toast.LENGTH_LONG).show();
                            } else {
                                try {
                                    Log.i("Operator from WS  ", "" + response.length());

                                    for (int i = 0; i < response.length(); i++) {
                                        JSONObject jo = response.getJSONObject(i);
                                        String query = String.format("INSERT INTO Operator (operator_name,code,type) values('%s','%s','%s')",
                                                jo.getString("OPERATOR_NAME"),
                                                jo.getString("OPCODE"),
                                                jo.getString("service_id"));
                                        //Log.i("Query", query);
                                        String operatortype = jo.getString("service_id");
                                        writeArray(operatortype, jo.getString("OPERATOR_NAME"), jo.getString("OPCODE"));
                                        sqlhandler.executeQuery(query);
                                    }

                                } catch (Exception e) {
                                    Toast.makeText(cxt, "Error" + e.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(cxt, error.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
            if (Common.internet_status) {
                VolleySingleton.getInstance(cxt).addToRequestQueue(req);
            }
        } catch (Exception r) {
            Toast.makeText(cxt, r.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    //set "Select Opertor" for each Spinner
    private void setFirstValue() {
        Common.pre_spinnername = new String[1];
        Common.pre_spinnercode = new String[1];

        Common.dth_spinnername = new String[1];
        Common.dth_spinnercode = new String[1];

        Common.data_spinnername = new String[1];
        Common.data_spinnercode = new String[1];

        Common.post_spinnername = new String[1];
        Common.post_spinnercode = new String[1];

        Common.pre_spinnername[0] = "Select Operator";
        Common.pre_spinnercode[0] = "0";
        Common.dth_spinnername[0] = "Select Operator";
        Common.dth_spinnercode[0] = "0";
        Common.data_spinnername[0] = "Select Operator";
        Common.data_spinnercode[0] = "0";
        Common.post_spinnername[0] = "Select Operator";
        Common.post_spinnercode[0] = "0";
    }

    private void writeArray(String operatortype, String name, String code) {
        if (operatortype.equalsIgnoreCase("1")) {
            Common.pre_spinnername = Arrays.copyOf(Common.pre_spinnername, Common.pre_spinnername.length + 1);
            Common.pre_spinnercode = Arrays.copyOf(Common.pre_spinnercode, Common.pre_spinnercode.length + 1);
            Common.pre_spinnername[Common.pre_spinnername.length - 1] = name;
            Common.pre_spinnercode[Common.pre_spinnercode.length - 1] = code;

        } else if (operatortype.equalsIgnoreCase("2")) {
            Common.dth_spinnername = Arrays.copyOf(Common.dth_spinnername, Common.dth_spinnername.length + 1);
            Common.dth_spinnercode = Arrays.copyOf(Common.dth_spinnercode, Common.dth_spinnercode.length + 1);
            Common.dth_spinnername[Common.dth_spinnername.length - 1] = name;
            Common.dth_spinnercode[Common.dth_spinnercode.length - 1] = code;

        } else if (operatortype.equalsIgnoreCase("3")) {
            Common.data_spinnername = Arrays.copyOf(Common.data_spinnername, Common.data_spinnername.length + 1);
            Common.data_spinnercode = Arrays.copyOf(Common.data_spinnercode, Common.data_spinnercode.length + 1);
            Common.data_spinnername[Common.data_spinnername.length - 1] = name;
            Common.data_spinnercode[Common.data_spinnercode.length - 1] = code;

        } else if (operatortype.equalsIgnoreCase("4")) {
            Common.post_spinnername = Arrays.copyOf(Common.post_spinnername, Common.post_spinnername.length + 1);
            Common.post_spinnercode = Arrays.copyOf(Common.post_spinnercode, Common.post_spinnercode.length + 1);
            Common.post_spinnername[Common.post_spinnername.length - 1] = name;
            Common.post_spinnercode[Common.post_spinnercode.length - 1] = code;
        }
    }

    //Load Operator From SqLite dataBase
    private void readOperatorFromSQLite() {
        try {

            String query = "SELECT operator_name,code,type FROM Operator";
            Cursor c1 = sqlhandler.selectQuery(query);

            c1.moveToFirst();

            setFirstValue();

            String operatortype, name, code;

            if (c1.moveToFirst() && c1 != null && c1.getCount() != 0) {

                Common.operator_status = true;

                do {
                    operatortype = c1.getString((c1.getColumnIndex("type")));
                    name = c1.getString((c1.getColumnIndex("operator_name")));
                    code = c1.getString((c1.getColumnIndex("code")));
                    writeArray(operatortype, name, code);
                    //Log.i("operator type ", operatortype);
                } while (c1.moveToNext());
            }
            c1.close();
        } catch (Exception t) {
            t.printStackTrace();
            Toast.makeText(cxt, "Loading error" + t.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


}
