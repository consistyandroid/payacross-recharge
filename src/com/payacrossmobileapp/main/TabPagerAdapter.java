package com.payacrossmobileapp.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by Admin on 4/16/2016.
 */
public class TabPagerAdapter extends FragmentPagerAdapter {
    public TabPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;//super.getItemPosition(object)
    }

    @Override
    public Fragment getItem(int index) {
        BrowsePlanList fragmet = new BrowsePlanList();
        Bundle args = new Bundle();
        switch (index) {
            case 0:
                args.putString("plans", "full");
                fragmet.setArguments(args);
                return fragmet;
            case 1:
                args.putString("plans", "talktime");
                fragmet.setArguments(args);
                return fragmet;
            case 2:
                args.putString("plans", "2g");
                fragmet.setArguments(args);
                return fragmet;
            case 3:
                args.putString("plans", "3g");
                fragmet.setArguments(args);
                return fragmet;
            case 4:
                args.putString("plans", "other");
                fragmet.setArguments(args);
                return fragmet;
        }

        return fragmet;
    }


    @Override
    public int getCount() {
        return 5;
    }
}