package com.payacrossmobileapp.main;

/**
 * Created by Admin on 1/23/2016.
 */
/**
 * Created by Somnath on 1/16/2016.
 */
public class Log {
    static final boolean LOG = false;

    public static void CallGC()
    {
        System.gc();
    }

    public static void i(String tag, String string) {
        if (LOG) android.util.Log.i(tag, string);
        CallGC();
    }
    public static void e(String tag, String string) {
        if (LOG) android.util.Log.e(tag, string);
        CallGC();
    }
    public static void d(String tag, String string) {
        if (LOG) android.util.Log.d(tag, string);
        CallGC();
    }
    public static void v(String tag, String string) {
        if (LOG) android.util.Log.v(tag, string);
        CallGC();
    }
    public static void w(String tag, String string) {
        if (LOG) android.util.Log.w(tag, string);
        CallGC();
    }
}
