package com.payacrossmobileapp.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.payacrossmobileapp.Interface.ILoginUser;
import com.payacrossmobileapp.Json.LoginDetailsPojo;
import com.payacrossmobileapp.async.LoginUserAsync;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends Activity implements ILoginUser {
    ProgressDialog pDialog;
    EditText Username, Password, uname, mobNo, name, password, address;
    Button Login, Clear, ForgetPassword, Reset;
    Dialog dialog;
    private Boolean saveLogin, registerKey;
    private CheckBox saveLoginCheckBox;
    public static SharedPreferences loginPreference;
    public static SharedPreferences.Editor loginEditor;
    String USERNAME, PASSWORD;
    Context localcontext = this;
    String key;

    Animation animVanish;
    NetworkState ns = new NetworkState();
    String fname, fpassword, fstatus, lapuId, txtmobNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Common.checkInternetConenction(getApplicationContext());
        Username = (EditText) findViewById(R.id.edtuname);
        Password = (EditText) findViewById(R.id.edtpassword);
        Login = (Button) findViewById(R.id.btnlogin);
        Clear = (Button) findViewById(R.id.btnclear);
        ForgetPassword = (Button) findViewById(R.id.btnforgetpass);

        saveLoginCheckBox = (CheckBox) findViewById(R.id.chklogin);

        animVanish = AnimationUtils.loadAnimation(this, R.anim.anim_vanish);

        pDialog = new ProgressDialog(this);

        loginPreference = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        loginEditor = loginPreference.edit();
        saveLogin = loginPreference.getBoolean("saveLogin", false);
        registerKey = loginPreference.getBoolean("isGCMRegister", false);

        if (saveLogin == true) {
            Username.setText(loginPreference.getString("username", ""));
            Password.setText(loginPreference.getString("password", ""));
            // Pin.setText(loginPreference.getString("Pin", ""));

            saveLoginCheckBox.setChecked(true);
        }
        Login.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                v.startAnimation(animVanish);
                USERNAME = Username.getText().toString().trim();
                PASSWORD = Password.getText().toString().trim();

                Common.loginId = USERNAME;

                if (USERNAME.length() == 0) {
                    Username.setError("Please Enter User Name");
                    Username.requestFocus();
                } else if (PASSWORD.length() == 0) {
                    Password.setError("Please Enter the Password");
                    Password.requestFocus();
                } else {
                    if (saveLoginCheckBox.isChecked()) {
                        loginEditor.putBoolean("saveLogin", true);
                        loginEditor.putString("username", USERNAME);
                        loginEditor.putString("password", PASSWORD);
                        loginEditor.commit();
                    } else {
                        loginEditor.clear();
                        loginEditor.commit();
                    }

                        if (ns.isInternetAvailable(localcontext)) {
                            Log.e("in", "btn click");
                            new LoginTask().execute();
                        } else {
                            pDialog.dismiss();
                            Toast.makeText(LoginActivity.this, "Internet connection is not available", Toast.LENGTH_LONG).show();
                        }
                }
            }
        });

        Clear.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animVanish);
                Username.setText("");
                Password.setText("");
                Username.requestFocus();
            }
        });

        ForgetPassword.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(LoginActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
                dialog.setContentView(R.layout.activity_forget);
                dialog.setTitle("Reset Password");
                dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);

                Reset = (Button) dialog.findViewById(R.id.btnreset);
                Clear = (Button) dialog.findViewById(R.id.btnCancel);

                uname = (EditText) dialog.findViewById(R.id.edtregid);
                mobNo = (EditText) dialog.findViewById(R.id.edtregismoble);
                dialog.show();

                Reset.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (uname.length() == 0) {
                            uname.setError("Enter Name");
                            uname.requestFocus();
                        } else if (mobNo.length() == 0) {
                            mobNo.setError("Please enter registered mobile number");
                            mobNo.requestFocus();
                        } else {
                            if (ns.isInternetAvailable(localcontext)) {
                            lapuId = uname.getText().toString().trim();
                            txtmobNo = mobNo.getText().toString().trim();
                            new ForgotTask().execute();
                            pDialog.show();
                            } else {

                                Toast.makeText(LoginActivity.this, "Internet connection is not available", Toast.LENGTH_LONG).show();
                            }
                        }

                    }
                });

                Clear.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.hide();
                    }
                });
            }
        });
    }

    public void runLoginTask() {
        if (ns.isInternetAvailable(localcontext)) {
            LoginUserAsync loginAsync = new LoginUserAsync();
            loginAsync.delegate = this;
            loginAsync.execute();
        } else {

            Toast.makeText(LoginActivity.this, "Internet connection is not available", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void getLoginDetails(ArrayList<LoginDetailsPojo> logindetaillist) {
        for (LoginDetailsPojo pojo : logindetaillist) {

            try {
                if (logindetaillist != null) {
                    Common.usertype = pojo.getUserType();
                    Common.name = pojo.getName();
                    Common.mobileno = pojo.getMobileNumber();
                    Common.user_name = pojo.getUserName();
                    Common.recharge_id = pojo.getRechargeID();
                    Common.regid = pojo.getRegid();
                    Common.lapuid = pojo.getLapuId();
                    Common.usertypeotomax = pojo.getUserTypeOtomax();
                    Common.usercode = pojo.getUserCodeOtomax();


                    Common.loginflag = false;
                    Intent homeintent = new Intent(LoginActivity.this, MainActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("type", "GPRS");
                    homeintent.putExtras(bundle);
                    startActivity(homeintent);
                    finish();
                } else {
                    Toast.makeText(localcontext, "Wrong UserName & Password", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void getLoginErrorMessage(String result) {
        Toast.makeText(localcontext, "Wrong UserName & Password", Toast.LENGTH_LONG).show();
        Username.setText("");
        Password.setText("");
        Username.requestFocus();
    }

    //asynchronous task for login activity
    public class LoginTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "LoginUser.asmx/ValidateUser?IP=" + Common.IMEI;

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];

                n = in.read(b);

                if (n > 0) out.append(new String(b, 0, n));

            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(LoginActivity.this);
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
            //Toast.makeText(getApplicationContext(),url,Toast.LENGTH_LONG).show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            Log.e("url", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet();
                request.setHeader("userName", USERNAME);
                request.setHeader("password", PASSWORD);

                request.setURI(new URI(url));
                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("result", result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // Log.e("result", result);
            pDialog.dismiss();
            parseJSON(result);
        }
    }

    //Parse the JSON response
    void parseJSON(String result) {
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(localcontext, "Invalid Credentials", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        Common.usertype = jo.getString("UserType");
                        Common.name = jo.getString("Name");
                        Common.mobileno = jo.getString("Mobile_Number");
                        Common.user_name = jo.getString("UserName");
                        Common.recharge_id = jo.getString("RechargeID");
                        Common.regid = jo.getString("Regid");
                        Common.lapuid = jo.getString("LapuId");
                        Common.usertypeotomax = jo.getString("userTypeOtomax");
                        Common.usercode = jo.getString("userCodeOtomax");


                        loginEditor.putString("UserType", jo.getString("UserType"));
                        loginEditor.putString("Name", jo.getString("Name"));
                        loginEditor.putString("Mobile_Number", jo.getString("Mobile_Number"));
                        loginEditor.putString("UserName", jo.getString("UserName"));
                        loginEditor.putString("RechargeID", jo.getString("RechargeID"));
                        loginEditor.putString("Regid", jo.getString("Regid"));
                        loginEditor.putString("LapuId", jo.getString("LapuId"));
                        loginEditor.putString("userTypeOtomax", jo.getString("userTypeOtomax"));
                        loginEditor.putString("userCodeOtomax", jo.getString("userCodeOtomax"));
                        loginEditor.putBoolean("logoutStatus", true);
                        loginEditor.commit();


                    }
                    try {
                        if (result == null) {
                            Toast.makeText(localcontext, "Wrong UserName & Password", Toast.LENGTH_LONG).show();
                            Username.setText("");
                            Password.setText("");
                            Username.requestFocus();
                        } else {
                            Common.loginflag = false;
                            Intent homeintent = new Intent(LoginActivity.this, MainActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("type", "GPRS");
                            homeintent.putExtras(bundle);
                            startActivity(homeintent);
                            finish();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
                Toast.makeText(localcontext, "Wrong Username or Password", Toast.LENGTH_LONG).show();
            }
        }
    }

    //asynchronous task for Forgot Password activity
    public class ForgotTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "ForgetPassword.asmx/ForgotPass";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];

                n = in.read(b);

                if (n > 0) out.append(new String(b, 0, n));

            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(LoginActivity.this);
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                Log.e("MobileNo", url);
                List nameValuePairs = new ArrayList();
                // add an HTTP variable and value pair
                nameValuePairs.add(new BasicNameValuePair("LapuId", uname.getText().toString().trim()));
                nameValuePairs.add(new BasicNameValuePair("MobileNo", mobNo.getText().toString().trim()));
                Log.e("LapuId", lapuId);
                Log.e("MobileNo", txtmobNo);
                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                request.setURI(new URI(url));
                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();

			/*if(result.equals("")) //DISTRIBUTER//Admin
            {
				Toast.makeText(localcontext,"Wrong responce",Toast.LENGTH_LONG).show();
			}*/
            parseJSONForgotPass(result);
        }
    }

    //Parse the JSON response
    void parseJSONForgotPass(String result) {
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);

                if (jsonArray.length() == 0) {
                    Toast.makeText(localcontext, "Wrong User", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        fstatus = jo.getString("Status");
                    }//for

                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setTitle("Message");
                    builder.setIcon(R.mipmap.ic_launcher);
                    builder.setMessage(String.format("Status : %s", fstatus));
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            uname.setText("");
                            mobNo.setText("");
                            uname.requestFocus();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            } catch (JSONException e) {
                //Log.e("JSONError", e.getMessage());
                //e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(LoginActivity.this, StartActivity.class);
        startActivity(i);
        finish();
    }


}


