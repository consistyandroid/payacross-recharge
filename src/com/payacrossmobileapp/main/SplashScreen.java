package com.payacrossmobileapp.main;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.payacrossmobileapp.Interface.ILoginUser;
import com.payacrossmobileapp.Json.LoginDetailsPojo;
import com.payacrossmobileapp.async.LoginUserAsync;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;


public class SplashScreen extends Activity implements ILoginUser {


    // LoginActivity loginActivity;
    public static SharedPreferences loginPreference;
    public static SharedPreferences.Editor loginEditor;
    protected boolean _active = true;
    protected int _splashTime = 2000;
    ProgressDialog pDialog;
    String username, password, UserType, SavedUserName, SavedPassword;
    String operatortype;
    String code, name;
    Context localcontext = this;
    NetworkState ns = new NetworkState();
    Operator op;
    private Boolean saveLogin = true, logutStatus;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash_screen);

        loginPreference = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        Common.checkInternetConenction(getApplicationContext());
        saveLogin = loginPreference.getBoolean("saveLogin", false);
        logutStatus = loginPreference.getBoolean("logoutStatus", false);
        loginEditor = loginPreference.edit();

        com.payacrossmobileapp.database.sqlhandler sqlhandler = new com.payacrossmobileapp.database.sqlhandler(this);
        op = new Operator(sqlhandler, this);

        try {

            if (saveLogin == true) {
                SavedUserName = loginPreference.getString("username", "");
                password = loginPreference.getString("password", "");
                runLoginAsync(SavedUserName, password);

            } else {
                Thread splashTread = new Thread() {
                    @Override
                    public void run() {
                        try {
                            int waited = 0;
                            while (_active && (waited < _splashTime)) {
                                sleep(500);
                                if (_active) {
                                    waited += 500;
                                }
                            }
                        } catch (Exception e) {

                        } finally {
                            startActivity(new Intent(SplashScreen.this, StartActivity.class));
                            Common.loginflag1 = false;
                            overridePendingTransition(R.anim.fadeout, R.anim.fadeout);
                            finish();
                        }
                    }


                };

                splashTread.start();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public void getLoginDetails(ArrayList<LoginDetailsPojo> logindetaillist) {
        for (LoginDetailsPojo obj : logindetaillist) {
            username = obj.getLapuId();
            UserType = obj.getUserType();
            if (loginPreference.getString("UserType", "").equals(UserType) && loginPreference.getString("LapuId", "").equals(username)) {

                Common.loginflag = true;
                Common.usertype = loginPreference.getString("UserType", "");
                Common.name = loginPreference.getString("Name", "");
                Common.mobileno = loginPreference.getString("Mobile_Number", "");
                Common.user_name = loginPreference.getString("UserName", "");
                Common.recharge_id = loginPreference.getString("RechargeID", "");
                Common.regid = loginPreference.getString("Regid", "");
                Common.lapuid = loginPreference.getString("LapuId", "");
                Common.usertypeotomax = loginPreference.getString("userTypeOtomax", "");
                Common.usercode = loginPreference.getString("userCodeOtomax", "");

                loginEditor.putBoolean("logutStatus ", true);
                loginEditor.commit();

                if (saveLogin == true) {


                    if (Common.usertype.equalsIgnoreCase("Owner")) {
                        Intent homeinten = new Intent(SplashScreen.this, MainActivity.class);
                        Bundle bundl = new Bundle();
                        bundl.putString("type", "GPRS");
                        homeinten.putExtras(bundl);

                        homeinten.addCategory(Intent.CATEGORY_HOME);
                        homeinten.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(homeinten);
                        finish();
                    } else if (Common.usertype.equalsIgnoreCase("Distributer") || Common.usertype.equalsIgnoreCase("Distributor")) {
                        Intent homeintent = new Intent(SplashScreen.this, MainActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("type", "GPRS");
                        homeintent.putExtras(bundle);
                        homeintent.addCategory(Intent.CATEGORY_HOME);
                        homeintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(homeintent);
                        finish();
                    } else if (Common.usertype.equalsIgnoreCase("Retailer")) {
                        Intent homeinte = new Intent(SplashScreen.this, MainActivity.class);
                        Bundle bund = new Bundle();
                        bund.putString("type", "GPRS");
                        homeinte.putExtras(bund);
                        homeinte.addCategory(Intent.CATEGORY_HOME);
                        homeinte.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(homeinte);
                        finish();
                    }
                } else {
                    //Toast.makeText(getApplicationContext(), "ok2", Toast.LENGTH_LONG).show();
                    Thread splashTread = new Thread() {
                        @Override
                        public void run() {
                            try {
                                int waited = 0;
                                while (_active && (waited < _splashTime)) {
                                    sleep(500);
                                    if (_active) {
                                        waited += 500;
                                    }
                                }
                            } catch (Exception e) {

                            } finally {
                                startActivity(new Intent(SplashScreen.this, StartActivity.class));
                                Common.loginflag1 = false;
                                overridePendingTransition(R.anim.fadeout, R.anim.fadeout);
                                finish();
                            }
                        }


                    };

                    splashTread.start();
                }


            } else {
                Intent intgprs = new Intent(SplashScreen.this, LoginActivity.class);
                startActivity(intgprs);
                Toast.makeText(SplashScreen.this, "Invalid credentials", Toast.LENGTH_SHORT).show();
            }

        }


    }

    @Override
    public void getLoginErrorMessage(String result) {
        Toast.makeText(SplashScreen.this, "somthing wrong", Toast.LENGTH_SHORT).show();
    }


    private void runLoginAsync(String uName, String Pass) {
        if (ns.isInternetAvailable(localcontext)) {
            LoginUserAsync loginAsync = new LoginUserAsync();
            loginAsync.delegate = this;
            loginAsync.execute(uName, Pass);
        } else {

            Toast.makeText(SplashScreen.this, "Internet connection is not available", Toast.LENGTH_LONG).show();
        }
    }
}
