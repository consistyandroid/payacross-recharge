package com.payacrossmobileapp.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.payacrossmobileapp.report.BalanceTransferReport;
import com.payacrossmobileapp.report.DayReportActivity;
import com.payacrossmobileapp.report.DownlineListActivity;
import com.payacrossmobileapp.report.LastTenActivity;
import com.payacrossmobileapp.report.MobileReportActivity;
import com.payacrossmobileapp.report.PurchaseReportActivity;
import com.payacrossmobileapp.report.Report;
import com.payacrossmobileapp.report.ReportListAdaptor;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class BalanceTransfer extends Activity {
    EditText amount, peramount, finalamount, mobNo, address, password, edtname, edtsmsmemid;
    EditText newMobileNumber, oldPassword, newPassword, oldMobileNumber, txtBalance, txtNotification;
    EditText receiverId, pinNo, edtamount, addNotification, serverNo;
    Button btnOk, btnAdd, btnChange, btnSave, Reset, Clear;
    public static SharedPreferences.Editor loginEditor;
    public static SharedPreferences.Editor myEditor;
    private Boolean saveNum;
    static String receiver = null;
    CheckBox chkSave;
    public static SharedPreferences myPreference;
    public static SharedPreferences loginPreference;
    TextView txthelpPhnNoone, emailId;
    Button btnSend, btnCancel;
    Spinner sp_receiverId;
    ProgressDialog pDialog;
    String retailer_id[];
    private Menu mMenu;
    Dialog dialog;
    private static int RESULT_LOAD_IMAGE = 1;
    String picturePath;
    SharedPreferences shre;
    String retid, MemeberId, type, PIN;
    Double PerAmount, FinalAmount, PerValue;
    Double Amount;
    Context context = this;
    AutoCompleteTextView mem_id;
    ArrayAdapter<String> retailerid;
    SwipeRefreshLayout swipeView;
    Animation animVanish;
    String lastDOC = "", msgstring;
    Context localcont = this;
    ListView listview;
    TextView txtor;
    int y = 0;
    ArrayList<Report> reportList = new ArrayList<Report>();
    ReportListAdaptor adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balance_trans);
        amount = (EditText) findViewById(R.id.edtamount);
        finalamount = (EditText) findViewById(R.id.edtfinalamount);
        peramount = (EditText) findViewById(R.id.edtperamount);
        pinNo = (EditText) findViewById(R.id.edtpin);
        edtsmsmemid = (EditText) findViewById(R.id.edtsmsmemid);
        sp_receiverId = (Spinner) findViewById(R.id.sp_member_id);
        btnSend = (Button) findViewById(R.id.btnSend);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        listview = (ListView) findViewById(R.id.reachlistMessages);
        txtor = (TextView) findViewById(R.id.txtor);
        mem_id = (AutoCompleteTextView) findViewById(R.id.edt_member_id);

        Bundle extras = getIntent().getExtras();
        type = extras.getString("type");
        animVanish = AnimationUtils.loadAnimation(this, R.anim.anim_vanish);
        edtsmsmemid.setVisibility(View.GONE);
        listview.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int priorFirst = -1;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                y = listview.getFirstVisiblePosition();
            }

            @Override
            public void onScroll(final AbsListView view, final int first, final int visible, final int total) {

                y = listview.getFirstVisiblePosition();
                //Log.e("Y event :", y+"");
            }
        });
        if (type.equals("SMS")) {
            sp_receiverId.setVisibility(View.GONE);
            txtor.setVisibility(View.GONE);
            mem_id.setVisibility(View.GONE);
            edtsmsmemid.setVisibility(View.VISIBLE);
        }
        myPreference = getSharedPreferences("myPreference", MODE_PRIVATE);
        myEditor = myPreference.edit();
        receiver = myPreference.getString("serverno", "");

        loginPreference = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        loginEditor = loginPreference.edit();

        if (Common.internet_status) {
            new SetDownlineListTask().execute();
        } else {
            pDialog.hide();
            Toast.makeText(context, "Internet connection is not available", Toast.LENGTH_LONG).show();
        }
        sp_receiverId.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                retid = retailer_id[position];
                String tempRet = retid;
                String[] splitid = retid.split(" - ");
                retid = splitid[0];
                mem_id.setText(tempRet);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });


        mem_id.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos, long id) {
                String text = (String) parent.getItemAtPosition(pos);
                String tempRet = text;
                String[] splitid = text.split(" - ");
                retid = splitid[0];
                Log.i("Member selected", text + " -> " + retid);
                int selectionPosition = retailerid.getPosition(tempRet);
                sp_receiverId.setSelection(selectionPosition);
                // Toast.makeText(AdminActivity.this, " selected : " + retailer_id[pos], Toast.LENGTH_LONG).show();
            }
        });

        adapter = new ReportListAdaptor(localcont, reportList);
        listview.setAdapter(adapter);
        swipeView = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        swipeView.setColorScheme(R.color.Green, R.color.red, R.color.blue);

        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.setRefreshing(true);
                Log.d("Swipe", "Refreshing Number");
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeView.setRefreshing(false);
                        lastFiveResponse();
                    }
                }, 3000);
            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animVanish);
                //MemeberId = mem_id.getText().toString().trim();
                //MemeberId = sp_receiverId.getSelectedItem().toString();
                PIN = pinNo.getText().toString();
                try {
                    Amount = Double.parseDouble(amount.getText().toString().trim());
                    PerAmount = Double.parseDouble(peramount.getText().toString().trim());

                } catch (Exception e) {
                    // StackTraceElement(e);
                }
                if (amount.getText().toString().equals("")) {
                    amount.setError("Enter Amount");
                    amount.requestFocus();

                } else if (mem_id.getText().toString().equals("Select MemberId")) {

                    Toast.makeText(context, "Please select member id ", Toast.LENGTH_LONG).show();
                } else if (peramount.getText().toString().equals("")) {
                    peramount.setError("Enter Percent(%) Value");
                    peramount.requestFocus();

                } else if (pinNo.getText().toString().equals("")) {
                    pinNo.setError("Enter Pin No");
                    pinNo.requestFocus();

                } else if (type.equals("GPRS")) {
                    ConnectivityManager conMan = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    //mobile
                    NetworkInfo.State mobile = conMan.getNetworkInfo(0).getState();
                    //wifi
                    NetworkInfo.State wifi = conMan.getNetworkInfo(1).getState();

                    if (mobile == NetworkInfo.State.CONNECTED) {
                        //mobile
                        new BalanceTransTask().execute();

                    } else if (wifi == NetworkInfo.State.CONNECTED) {
                        //wifi
                        new BalanceTransTask().execute();

                    } else {

                        Toast.makeText(context, "No Network Connection ", Toast.LENGTH_LONG).show();
                    }
                } else if (type.equals("SMS")) {
                    //TR.MemberID.Amount.Pin
                    msgstring = null;
                    msgstring = edtsmsmemid.getText().toString() + "." + finalamount.getText().toString() + "." + pinNo.getText().toString();
                    MainActivity.sendMessage("TR." + msgstring, BalanceTransfer.this);
                }
            }

        });

        peramount.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    Double per = Double.parseDouble(s.toString());
                    Amount = Double.parseDouble(amount.getText().toString());

                    int finalvalue = (int) (Amount + ((Amount / 100) * per));
                    finalamount.setText(finalvalue + "");
                    Log.i("User entered", String.format("Amount:%s, per:%s, Final:%s", amount.getText().toString(), s.toString(), finalamount.getText()));

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.v("State", e.getMessage());
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Log.i("User before", s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.i("User After ", s.toString());
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animVanish);
                mem_id.setText("");
                amount.setText("");
                peramount.setText("");
                finalamount.setText("");
                sp_receiverId.setSelection(0);
                mem_id.requestFocus();
                lastFiveResponse();
            }
        });

    }

    //Menu List Layout
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mMenu = menu;

        try {
            if (type.equalsIgnoreCase("GPRS")) {
                if (Common.usertype.equalsIgnoreCase("Retailer")) {
                    getMenuInflater().inflate(R.menu.main, menu);
                    shre = PreferenceManager.getDefaultSharedPreferences(this);
                    String base64 = shre.getString("image_data", "");
                    if (base64.length() > 10) {
                        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

						/*MenuItem item ;
                        item= menu.findItem(R.id.profileimg);
						item.setIcon(new BitmapDrawable(getRoundedCornerBitmap(decodedByte)));//new BitmapDrawable(decodedByte)
						mMenu = menu;*/
                    }
                } else if (Common.usertype.equalsIgnoreCase("Owner")) {
                    getMenuInflater().inflate(R.menu.adminmenulist, menu);

                    shre = PreferenceManager.getDefaultSharedPreferences(this);
                    String base64 = shre.getString("image_data", "");

                    if (base64.length() > 10) {

                        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

					/*	MenuItem item ;
                        item= menu.findItem(R.id.profileimg);
						item.setIcon(new BitmapDrawable(getRoundedCornerBitmap(decodedByte)));
						mMenu = menu;
*/
                        /*tf = Typeface.createFromAsset(DayReportActivity.this.getAssets(), Common.fonts);
                                item = menu.findItem(R.id.bal_transfer);
								TextView itemuser = (TextView) item.getActionView();

							           tf= Typeface.createFromAsset(this.getAssets(), "Allura-Regular.otf");
							           itemuser.setTypeface(tf);
							           itemuser.setBackgroundColor(Color.TRANSPARENT);*/
                    }
                } else if (Common.usertype.equalsIgnoreCase("Distributer")) {
                    getMenuInflater().inflate(R.menu.distributormenulist, menu);

                    shre = PreferenceManager.getDefaultSharedPreferences(this);
                    String base64 = shre.getString("image_data", "");

                    if (base64.length() > 10) {

                        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

						/*MenuItem item ;
                        item= menu.findItem(R.id.profileimg);
						item.setIcon(new BitmapDrawable(getRoundedCornerBitmap(decodedByte)));
						mMenu = menu;*/
                    }
                } else {
                    Toast.makeText(localcont, "Wrong response", Toast.LENGTH_LONG).show();
                }
            } else {
                getMenuInflater().inflate(R.menu.sms_menulist, menu);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        menu.findItem(R.id.bal_check);

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();

            FileInputStream in;
            BufferedInputStream buf;

            try {
                in = new FileInputStream(picturePath);

                buf = new BufferedInputStream(in);

                Bitmap realImage = BitmapFactory.decodeStream(in);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                realImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();

                //roundedImage = new RoundImage(realImage);

                String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

                shre = PreferenceManager.getDefaultSharedPreferences(this);
                SharedPreferences.Editor edit = shre.edit();
                edit.putString("image_data", encodedImage);
                edit.commit();

				/*MenuItem item ;
                item= mMenu.findItem(R.id.profileimg);
				item.setIcon(new BitmapDrawable(getRoundedCornerBitmap(realImage)));*///new BitmapDrawable(realImage)
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    //Menu List Items
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            //Menu Profile pic
            case R.id.last_report:
                Intent last = new Intent(BalanceTransfer.this, LastTenActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("type", type);
                last.putExtras(bundle);
                startActivity(last);
                finish();
                break;

            //Menu Profile pic
            case R.id.purchasereport:
                Intent purchase = new Intent(BalanceTransfer.this, PurchaseReportActivity.class);
                Bundle purchasebundle = new Bundle();
                purchasebundle.putString("type", type);
                purchase.putExtras(purchasebundle);
                startActivity(purchase);
                finish();
                break;
         /*   case R.id.balanceview:
                Intent balview = new Intent(BalanceTransfer.this,CommissionReport.class);
                Bundle balviewbundle = new Bundle();
                balviewbundle.putString("type", type);
                balview.putExtras(balviewbundle);
                startActivity(balview);
                finish();
                break;*/
            //Menu Profile pic
    /*	case R.id.profileimg:
            Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			startActivityForResult(i, RESULT_LOAD_IMAGE);
			break;
*/
            case R.id.browseplan:
                Intent browse = new Intent(BalanceTransfer.this, browsePlans.class);
                Bundle browsebundle = new Bundle();
                browsebundle.putString("type", type);
                browse.putExtras(browsebundle);
                startActivity(browse);
                finish();
                break;

            //Menu Change Mobile Number
            case R.id.change_mobileno:
                dialog = new Dialog(BalanceTransfer.this);
                dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
                dialog.setContentView(R.layout.activity_changemobile);
                dialog.setTitle("Change Mobile");
                dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
                animVanish = AnimationUtils.loadAnimation(BalanceTransfer.this, R.anim.anim_vanish);
                newMobileNumber = (EditText) dialog.findViewById(R.id.edtnewmobileno);
                oldMobileNumber = (EditText) dialog.findViewById(R.id.edtoldmobno);

                btnChange = (Button) dialog.findViewById(R.id.btnChange);
                btnCancel = (Button) dialog.findViewById(R.id.btnCancel);

                dialog.show();

                btnChange.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setAnimation(animVanish);
                        if (oldMobileNumber.getText().toString().trim().equals("")) {
                            oldMobileNumber.setError("Please enter mobile number");
                            oldMobileNumber.requestFocus();
                        } else if (newMobileNumber.getText().toString().trim().equals("")) {
                            newMobileNumber.setError("Please enter mobile number");
                            newMobileNumber.requestFocus();
                        } else {
                            if (Common.internet_status) {
                                new ChangeMobTask().execute();
                            } else {
                                pDialog.dismiss();
                                Toast.makeText(BalanceTransfer.this, "Internet connection is not available", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setAnimation(animVanish);
                        dialog.hide();
                    }

                });
                break;

            //Menu Change Password
            case R.id.changepass:
                dialog = new Dialog(BalanceTransfer.this);
                dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
                dialog.setContentView(R.layout.activity_changepassword);
                dialog.setTitle("Change Password");
                dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
                animVanish = AnimationUtils.loadAnimation(BalanceTransfer.this, R.anim.anim_vanish);

                oldPassword = (EditText) dialog.findViewById(R.id.edtOldpasswod);
                newPassword = (EditText) dialog.findViewById(R.id.edtNewPassword);
                btnChange = (Button) dialog.findViewById(R.id.btnChange);
                btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
                dialog.show();

                btnChange.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setAnimation(animVanish);
                        if (oldPassword.getText().toString().trim().equals("")) {
                            oldPassword.setError("Please enter password");
                            oldPassword.requestFocus();
                        } else if (newPassword.getText().toString().trim().equals("")) {
                            newPassword.setError("Please enter password");
                            newPassword.requestFocus();
                        } else {
                            if (Common.internet_status) {
                                new ChangePassTask().execute();
                            } else {
                                pDialog.dismiss();
                                Toast.makeText(BalanceTransfer.this, "Internet connection is not available", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setAnimation(animVanish);
                        dialog.dismiss();
                    }
                });
                break;

            //Menu Day Report
            case R.id.dayreport:

                Intent dayReport = new Intent(BalanceTransfer.this, DayReportActivity.class);
                bundle = new Bundle();
                bundle.putString("type", type);
                dayReport.putExtras(bundle);
                startActivity(dayReport);
                break;
            case R.id.searchmobile:
                Intent search_mobile = new Intent(BalanceTransfer.this, MobileReportActivity.class);
                bundle = new Bundle();
                bundle.putString("type", type);
                search_mobile.putExtras(bundle);
                startActivity(search_mobile);
                break;

            //Menu Add Notification
            case R.id.notification:

                dialog = new Dialog(BalanceTransfer.this);
                dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
                dialog.setContentView(R.layout.activity_nofication);
                dialog.setTitle("Add Notification");
                dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
                animVanish = AnimationUtils.loadAnimation(BalanceTransfer.this, R.anim.anim_vanish);

                addNotification = (EditText) dialog.findViewById(R.id.edtNotoication);

                btnAdd = (Button) dialog.findViewById(R.id.btnAdd);
                btnCancel = (Button) dialog.findViewById(R.id.btnCancel);

                dialog.show();

                btnAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setAnimation(animVanish);
                        if (addNotification.getText().toString().trim().equals("")) {
                            addNotification.setError("Please add notification");
                            addNotification.requestFocus();
                        } else {
                            if (Common.internet_status) {
                                new AddNotificationTask().execute();
                            } else {
                                pDialog.dismiss();
                                Toast.makeText(BalanceTransfer.this, "Internet connection is not available", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setAnimation(animVanish);
                        dialog.dismiss();
                    }
                });
                break;

            //Menu Balance Check
            case R.id.bal_check:
                if (type.equalsIgnoreCase("SMS")) {
                    //msgstring=String.format("%s %s %s", smsopcode,mobno,amount);
                    MainActivity.sendMessage("BAL", BalanceTransfer.this);
                } else {
                    try {
                        if (Common.internet_status) {
                            new BalCheckTask().execute();
                        } else {
                            pDialog.dismiss();
                            Toast.makeText(BalanceTransfer.this, "Internet connection is not available", Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            //Menu Help
            case R.id.Help:

                dialog = new Dialog(BalanceTransfer.this);
                dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
                dialog.setContentView(R.layout.activity_help);
                dialog.setTitle("Help");
                dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
                animVanish = AnimationUtils.loadAnimation(BalanceTransfer.this, R.anim.anim_vanish);

                txthelpPhnNoone = (TextView) dialog.findViewById(R.id.txthelpphnno);

                emailId = (TextView) dialog.findViewById(R.id.txtemail);

                txthelpPhnNoone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent mob = new Intent(android.content.Intent.ACTION_CALL, Uri.parse("tel:+91" + txthelpPhnNoone.getText().toString().trim()));
                        startActivity(mob);
                    }
                });

                emailId.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_EMAIL, new String[]{emailId.getText().toString()}); //new String[]{"recipient@example.com"}
                        i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
                        i.putExtra(Intent.EXTRA_TEXT, "body of email");
                        try {
                            startActivity(Intent.createChooser(i, "Send mail..."));
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(BalanceTransfer.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                btnOk = (Button) dialog.findViewById(R.id.btnok);
                dialog.show();
                btnOk.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        v.setAnimation(animVanish);
                        dialog.hide();
                    }
                });
                break;


            case R.id.downline:

                if (type.equalsIgnoreCase("SMS")) {
                    //msgstring=String.format("%s %s %s", smsopcode,mobno,amount);
                    MainActivity.sendMessage("DL", BalanceTransfer.this);
                } else {
                    Intent downList = new Intent(BalanceTransfer.this, DownlineListActivity.class);
                    bundle = new Bundle();
                    bundle.putString("type", type);
                    downList.putExtras(bundle);
                    startActivity(downList);
                    finish();
                }

                break;

           /* case R.id.balanceview:
                Intent balview = new Intent(BalanceTransfer.this,BalanceView.class);
                Bundle balviewbundle = new Bundle();
                balviewbundle.putString("type",type);
                balview.putExtras(balviewbundle);
                startActivity(balview);
                finish();
                break;*/

            case R.id.baltransreport:
                Intent baltrans = new Intent(BalanceTransfer.this, BalanceTransferReport.class);
                Bundle baltranreportbundle = new Bundle();
                baltranreportbundle.putString("type", type);
                baltrans.putExtras(baltranreportbundle);
                startActivity(baltrans);
                finish();
                break;
            //Menu User Activation
            case R.id.user_activation:

                dialog = new Dialog(BalanceTransfer.this);
                dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
                dialog.setContentView(R.layout.activity_signup);
                dialog.setTitle("User Activation");
                dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);

                Reset = (Button) dialog.findViewById(R.id.btnreset);
                Clear = (Button) dialog.findViewById(R.id.btnCancel);

                mobNo = (EditText) dialog.findViewById(R.id.edtmob);
                address = (EditText) dialog.findViewById(R.id.edtaddress);
                edtname = (EditText) dialog.findViewById(R.id.edtname);
                password = (EditText) dialog.findViewById(R.id.edtpass);

                dialog.show();

                Reset.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    /*if(name.getText().toString().trim().equals(""))
                            {
								name.setError("Please enter name");
								name.requestFocus();
							}
							else if(address.getText().toString().trim().equals(""))
							{
								address.setError("Please enter address");
								address.requestFocus();
							}
							else if(mobNo.getText().toString().trim().equals(""))
							{
								mobNo.setError("Please enter mobile no");
								mobNo.requestFocus();
							}
							else if(password.getText().toString().trim().equals(""))
							{
								password.setError("Please enter pin");
								password.requestFocus();
							}
							else*/
                        if (Common.internet_status) {
                            new SignUpTask().execute();
                        } else {
                            pDialog.dismiss();
                            Toast.makeText(BalanceTransfer.this, "Internet connection is not available", Toast.LENGTH_LONG).show();
                        }
                    }
                });

                Clear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.hide();
                    }
                });

                break;
            //Menu Setting
            case R.id.sms_setting:

                dialog = new Dialog(BalanceTransfer.this);
                dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
                dialog.setContentView(R.layout.activity_setting);
                dialog.setTitle("Setting");
                dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
                animVanish = AnimationUtils.loadAnimation(BalanceTransfer.this, R.anim.anim_vanish);

                serverNo = (EditText) dialog.findViewById(R.id.txt_reciverno);
                chkSave = (CheckBox) dialog.findViewById(R.id.chksave);

                btnSave = (Button) dialog.findViewById(R.id.btn_save);
                btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);

                dialog.show();

                String server = myPreference.getString("serverno", "");
                saveNum = myPreference.getBoolean("saveNum", false);
                if (saveNum == true) {
                    serverNo.setText(server);
                    chkSave.setChecked(true);
                }

                btnSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setAnimation(animVanish);
                        MainActivity.receiver = serverNo.getText().toString().trim();
                        if (receiver.length() != 0) {
                            if (chkSave.isChecked()) {
                                myEditor.putString("serverno", receiver);
                                myEditor.putBoolean("saveNum", true);
                                myEditor.commit();
                                Toast.makeText(getApplicationContext(), "Saved successfully", Toast.LENGTH_LONG).show();
                                dialog.dismiss();
                            } else {
                                myEditor.clear();
                                myEditor.commit();
                                dialog.dismiss();
                            }
                        } else {
                            //Toast.makeText(getApplicationContext(), "Set server no", Toast.LENGTH_LONG).show();
                            serverNo.setError("Set Server Number");
                            serverNo.requestFocus();
                        }
                    }
                });

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setAnimation(animVanish);
                        dialog.dismiss();
                    }
                });
                break;

            //Menu Logout
            case R.id.logout:
                Intent logout = new Intent(BalanceTransfer.this, LoginActivity.class);
                startActivity(logout);
                finish();
                break;
            default:
                return super.onOptionsItemSelected(item);

        }
        return true;
    }

    //Start asynchronous task for Set DownLine List in Balance Transfer
    public class SetDownlineListTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "BalanceTransferDownline.asmx/UserDownline";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(BalanceTransfer.this);
            pDialog.setMessage("Proccessing....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            //Log.e("urlbal", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                List nameValuePairs = new ArrayList();
                nameValuePairs.add(new BasicNameValuePair("LapuId", Common.lapuid));
                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.i("Members", result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;

        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.hide();
            parseJSONSetDownline(result);
        }
    }

    //Parse the JSON response
    void parseJSONSetDownline(String result) {
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);

                if (jsonArray.length() == 0) {
                    Toast.makeText(context, "Set Down-line Wrong response", Toast.LENGTH_LONG).show();
                } else {
                    Log.i("Members", result);
                    ArrayList<String> list = new ArrayList<>();
                    list.add("Select MemberId");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        if (jo.getString("MemberIdName").length() > 0) {
                            list.add(jo.getString("MemberIdName"));
                        }
                    }
                    retailer_id = list.toArray(new String[0]);
                    retailerid = new ArrayAdapter<String>(BalanceTransfer.this, android.R.layout.simple_spinner_item,
                            retailer_id);
                    retailerid.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    sp_receiverId.setAdapter(retailerid);

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                            (this, android.R.layout.select_dialog_item, retailer_id);

                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


                    mem_id = (AutoCompleteTextView) findViewById(R.id.edt_member_id);
                    mem_id.setThreshold(1);
                    mem_id.setAdapter(adapter);
                    //Log.i("Result of bal trans", mem_id.toString());

                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }
    //End asynchronous task for Set DownLine List in Balance Transfer

    //Start asynchronous task for Balance Transfer
    public class BalanceTransTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "BalaneTransfer.asmx/UserBalanceTransfer";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");

            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            //Log.e("urlbal", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                List nameValuePairs = new ArrayList();
                // add an HTTP variable and value pair
                nameValuePairs.add(new BasicNameValuePair("MemberId", retid));
                nameValuePairs.add(new BasicNameValuePair("Amount", finalamount.getText().toString().trim()));
                nameValuePairs.add(new BasicNameValuePair("Pin", pinNo.getText().toString().trim()));
                // Log.i("Balance Tran Send ", String.format("Member ID : %s, Amount : %s, Pin : %s", retid, finalamount.getText().toString().trim(), pinNo.getText().toString().trim()));
                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = client.execute(request);
                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.i("UserBalanceTransfer Result", result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.hide();
            parseJSONBalanceTrans(result);
        }
    }

    //Parse the JSON response
    void parseJSONBalanceTrans(String result) {
        String status = "";
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(context, "Bal Trans Wrong responce", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        status = jo.getString("Status");
                        Log.e("baltrans", status);
                    }//for

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);//.getParent()
                    builder.setTitle("Message");
                    builder.setIcon(R.mipmap.ic_launcher);
                    builder.setMessage("" + status);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //new LoadBalCheckTask().execute();
                            mem_id.setText("");
                            amount.setText("");
                            peramount.setText("");
                            finalamount.setText("");
                            pinNo.setText("");
                            sp_receiverId.setSelection(0);
                            mem_id.requestFocus();
                            lastFiveResponse();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(20000);
                            } catch (InterruptedException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            // new LoadBalCheckTask().execute();
                        }
                    }).start();
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }

    //End asynchronous task for Balance Transfer
    public void lastFiveResponse() {
        if (type.equals("GPRS")) {


            try {
                String url = Common.COMMON_URL + "LastFiveResponse.asmx/LastFiveResponce?LapuId=" + Common.lapuid;
                JsonArrayRequest req = new JsonArrayRequest(url,
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                if (response != null) {
                                    reportList.clear();

                                    JSONObject jo;
                                    try {
                                        Common.resCount = response.length();
                                        for (int i = 0; i < Common.resCount; i++) {
                                            jo = response.getJSONObject(i);
                                            Report report = new Report(jo.getString("DOC"), jo.getString("Pesan"));
                                            reportList.add(report);
                                            if (i == 0) {
                                                lastDOC = jo.getString("DOC");
                                            }
                                        }
                                        adapter.notifyDataSetChanged();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();
                            }
                        });
                if (Common.internet_status) {
                    VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(req);
                } else {
                }
            } catch (Exception r) {
                r.printStackTrace();
            }
        }
    }

    //Start asynchronous task for Change Mobile
    public class ChangeMobTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "ChangeMobileNo.asmx/ChangeMobileno";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];

                n = in.read(b);

                if (n > 0) out.append(new String(b, 0, n));

            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            //Log.e("urlbal", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                List nameValuePairs = new ArrayList();
                // add an HTTP variable and value pair
                nameValuePairs.add(new BasicNameValuePair("Lapuid", Common.lapuid));
                nameValuePairs.add(new BasicNameValuePair("OldMobileNo", oldMobileNumber.getText().toString().trim()));
                nameValuePairs.add(new BasicNameValuePair("NewMobileNo", newMobileNumber.getText().toString().trim()));

                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);


            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONChangeMob(result);
        }
    }

    //Parse the JSON response
    void parseJSONChangeMob(String result) {
        String status = "";
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(localcont, "Wrong response", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        status = jo.getString("Status");
                        //Log.e("bal", jo.getString("Status")+" "+bal);
                    }//for

                    AlertDialog.Builder builder = new AlertDialog.Builder(BalanceTransfer.this);//.getParent()
                    builder.setTitle("Message");
                    builder.setIcon(R.mipmap.ic_launcher);
                    builder.setMessage("" + status);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //dialog.dismiss();
                            newMobileNumber.setText("");
                            oldMobileNumber.setText("");

                            newMobileNumber.requestFocus();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }

    //End asynchronous task for Change Mobile
    //Start asynchronous task for Change Password
    public class ChangePassTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "ChangePassword.asmx/ChangePassWord";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];

                n = in.read(b);

                if (n > 0) out.append(new String(b, 0, n));

            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            //Log.e("urlbal", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                List nameValuePairs = new ArrayList();
                // add an HTTP variable and value pair
                nameValuePairs.add(new BasicNameValuePair("LoginId", Common.lapuid));
                nameValuePairs.add(new BasicNameValuePair("oldpass", oldPassword.getText().toString().trim()));
                nameValuePairs.add(new BasicNameValuePair("newpass", newPassword.getText().toString().trim()));
                /*nameValuePairs.add(new BasicNameValuePair("RechargeID", Common.recharge_id));
                        nameValuePairs.add(new BasicNameValuePair("RegIdNo", Common.regid));*/

                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);


            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONChangePass(result);
        }
    }

    //Parse the JSON response
    void parseJSONChangePass(String result) {
        String status = "";
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(localcont, "Wrong response", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        status = jo.getString("Status");
                        //Log.e("bal", jo.getString("CURRENT_BALANCE")+" "+bal);
                    }//for

                    AlertDialog.Builder builder = new AlertDialog.Builder(BalanceTransfer.this);//.getParent()
                    builder.setTitle("Message");
                    builder.setIcon(R.mipmap.ic_launcher);
                    builder.setMessage("" + status);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //dialog.dismiss();
                            newPassword.setText("");
                            oldPassword.setText("");

                            oldPassword.requestFocus();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }

    //End asynchronous task for Change Password
    //Start asynchronous task for Add Notification
    public class AddNotificationTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "AddNotification.asmx/addNotication";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");

            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                List nameValuePairs = new ArrayList();
                // add an HTTP variable and value pair
                nameValuePairs.add(new BasicNameValuePair("NotificationText", addNotification.getText().toString().trim()));

                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONAddNotification(result);
            new NotificationTask().execute();
        }
    }

    //Parse the JSON response
    void parseJSONAddNotification(String result) {
        String status = "";
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(localcont, "Wrong response", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        status = jo.getString("Status");
                        //Log.e("bal", jo.getString("CURRENT_BALANCE")+" "+bal);
                    }//for

                    AlertDialog.Builder builder = new AlertDialog.Builder(BalanceTransfer.this);//.getParent()
                    builder.setTitle("Message");
                    builder.setIcon(R.mipmap.ic_launcher);
                    builder.setMessage("" + status);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            addNotification.setText("");
                            //dialog.dismiss();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }

    //End asynchronous task for Add Notification
    //Start asynchronous task for show Balance on dashboard
    public class LoadBalCheckTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "LoginUser.asmx/GetBalance";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }

            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            /*pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
			pDialog.setCancelable(false);
			pDialog.show();*/
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            Log.e("url", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONLoadBalCheck(result);
        }
    }

    //Parse the JSON response
    void parseJSONLoadBalCheck(String result) {
        String status = "";
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(BalanceTransfer.this, "Wrong response", Toast.LENGTH_LONG).show();
                } else {
                    //Received response from web service
                    /*"Status": "489.7500"*/
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        status = jo.getString("Status");
                        //Log.e("bal",status);
                    }//for

                    MainActivity.txtBalance.setText(status);
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
                e.printStackTrace();
            }
        }
    }

    //End asynchronous task for show Balance on dashboard
    //Start asynchronous task for Balance check
    public class BalCheckTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "LoginUser.asmx/GetBalance";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }

            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            Log.e("url", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

				/*List nameValuePairs = new ArrayList();
                        // add an HTTP variable and value pair
						nameValuePairs.add(new BasicNameValuePair("RechargeID", Common.recharge_id));
						nameValuePairs.add(new BasicNameValuePair("Regid", Common.regid));
						request.setEntity(new UrlEncodedFormEntity(nameValuePairs));*/

                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);

				/*HttpClient client = new DefaultHttpClient();
                        HttpGet request = new HttpGet();
						request.setURI(new URI(url));
						HttpResponse response = client.execute(request);
						request.addHeader("Regid",Common.regid);
						request.addHeader("RechargeID",Common.recharge_id);
						Log.e("Regid", Common.regid);
						Log.e("RechargeID", Common.recharge_id);
						HttpEntity entity = response.getEntity();
						result = getASCIIContentFromEntity(entity);

						Log.e("Result",result);*/

                //parseJSON(result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONBalCheck(result);
        }
    }

    //Parse the JSON response
    void parseJSONBalCheck(String result) {
        String status = "";
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(localcont, "Wrong response", Toast.LENGTH_LONG).show();
                } else {
                    //Received response from web service
                    /*"Status": "489.7500"*/
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        status = jo.getString("Status");
                        //Log.e("bal",status);

                    }//for

                    AlertDialog.Builder builder = new AlertDialog.Builder(BalanceTransfer.this);//.getParent()
                    builder.setTitle("Message");
                    builder.setIcon(R.mipmap.ic_launcher);
                    builder.setMessage("Your balance is " + status);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
                e.printStackTrace();
            }
        }
    }

    //End asynchronous task for Balance check
    //Start asynchronous task for Notification
    public class NotificationTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "AddNotification.asmx/ShowNotification";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];

                n = in.read(b);

                if (n > 0) out.append(new String(b, 0, n));

            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            //Log.e("urlbal", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

				/*request.addHeader("Regid",Common.regid);
                        request.addHeader("RechargeID",Common.recharge_id);*/

				/*List nameValuePairs = new ArrayList();
                        // add an HTTP variable and value pair
						nameValuePairs.add(new BasicNameValuePair("RechargeID", Common.recharge_id));
						nameValuePairs.add(new BasicNameValuePair("RegIdNo", Common.regid));
						request.setEntity(new UrlEncodedFormEntity(nameValuePairs));*/
                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);


            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONNotification(result);
        }
    }

    //Parse the JSON response
    void parseJSONNotification(String result) {
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(localcont, "Wrong response", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        txtNotification.setText(jo.getString("message"));
                    }
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }

    //End asynchronous task for Notification
    //Start asynchronous task for New SignUp/ Activation
    public class SignUpTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "UserActivation.asmx/NewUserActivation";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];

                n = in.read(b);

                if (n > 0) out.append(new String(b, 0, n));

            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(BalanceTransfer.this);
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                List nameValuePairs = new ArrayList();
                // add an HTTP variable and value pair
                nameValuePairs.add(new BasicNameValuePair("Name", edtname.getText().toString().trim()));
                nameValuePairs.add(new BasicNameValuePair("Pin", password.getText().toString().trim()));
                nameValuePairs.add(new BasicNameValuePair("MobNumber", mobNo.getText().toString().trim()));
                nameValuePairs.add(new BasicNameValuePair("Address", address.getText().toString().trim()));
                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                request.setURI(new URI(url));
                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();

            if (result.equals("")) //DISTRIBUTER//Admin
            {
                Toast.makeText(localcont, "Wrong response", Toast.LENGTH_LONG).show();
            }
            parseJSONSignUp(result);
        }
    }

    //Parse the JSON response
    void parseJSONSignUp(String result) {
        String status = null;
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(localcont, "Wrong User", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        status = jo.getString("Status");
                    }//for

                    AlertDialog.Builder builder = new AlertDialog.Builder(BalanceTransfer.this);
                    builder.setTitle("Message");
                    builder.setIcon(R.mipmap.ic_launcher);
                    builder.setMessage(String.format("Status : %s", status));
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //dialog.dismiss();
                            edtname.setText("");
                            mobNo.setText("");
                            address.setText("");
                            password.setText("");
                            edtname.requestFocus();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }
    //End of asynchronous task for New SignUp/ Activation
}
