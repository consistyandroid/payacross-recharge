package com.payacrossmobileapp.main;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class StartActivity extends Activity {

    Button btnSMS, btnGPRS, btnUpdate;
    com.payacrossmobileapp.database.sqlhandler sqlhandler;
    LinearLayout lliformation;
    Animation animVanish;
    TextView first, second, third;
    public static SharedPreferences loginPreference;
    public static SharedPreferences.Editor loginEditor;
    private Boolean saveLogin = true;

    String operatortype;
    String code, name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        sqlhandler = new com.payacrossmobileapp.database.sqlhandler(this);

        animVanish = AnimationUtils.loadAnimation(this, R.anim.anim_info);


        loginPreference = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        Common.checkInternetConenction(getApplicationContext());
        saveLogin = loginPreference.getBoolean("saveLogin", false);
       // logutStatus = loginPreference.getBoolean("logoutStatus", false);
        loginEditor = loginPreference.edit();





        /*lliformation = (LinearLayout) findViewById(R.id.lliformation);
        lliformation.startAnimation(animVanish);
        first = (TextView) findViewById(R.id.first);
        second = (TextView) findViewById(R.id.second);
        third = (TextView) findViewById(R.id.third);

        first.startAnimation(animVanish);

        Thread splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            second.startAnimation(animVanish);
                        }
                    });
                    Thread.sleep(1000);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            third.startAnimation(animVanish);
                        }

                    });
                } catch (Exception f) {
                }
            }

            ;
        };
        splashTread.start();

        first.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animVanish);
            }
        });

        second.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animVanish);
            }
        });

        third.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animVanish);
            }
        });*/

        btnSMS = (Button) findViewById(R.id.btn_sms);
        btnGPRS = (Button) findViewById(R.id.btn_gprs);
        btnUpdate = (Button) findViewById(R.id.btn_update);

        btnSMS.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animVanish);
                Intent homeintent = new Intent(StartActivity.this, MainActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("type", "SMS");
                homeintent.putExtras(bundle);
                startActivity(homeintent);
            }
        });

        btnGPRS.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                  /*  Intent intent = new Intent(StartActivity.this, LoginActivity.class);
                    startActivity(intent);*/
                    v.startAnimation(animVanish);
                    Intent intgprs = new Intent(v.getContext(), LoginActivity.class);
                    startActivity(intgprs);

            }
        });

        btnUpdate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animVanish);
                Intent intupdate = new Intent(Intent.ACTION_VIEW);
                intupdate.setData(Uri.parse(Common.playurl));
                startActivity(intupdate);
            }
        });

        Common.checkInternetConenction(getApplicationContext());
        TelephonyManager mngr = (TelephonyManager) StartActivity.this.getSystemService(StartActivity.TELEPHONY_SERVICE);
        Common.IMEI = mngr.getDeviceId();
    }

    @Override
    public void onBackPressed() {
        System.exit(0);
    }
}
