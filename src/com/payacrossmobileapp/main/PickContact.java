package com.payacrossmobileapp.main;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.Toast;

public class PickContact extends Activity
{
	Bundle bundle;
	String type;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Intent localIntent = new Intent("android.intent.action.PICK", ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
		PickContact.this.startActivityForResult(localIntent, 1);
		
		bundle=getIntent().getExtras();
		type=bundle.getString("type");
	}
	@Override
	public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent) {
		super.onActivityResult(paramInt1, paramInt2, paramIntent);
		switch (paramInt1)
		{
		}
		Cursor localCursor;
		do
		{
			while (paramInt2 != -1)
			{
				return;
			} 
			localCursor = managedQuery(paramIntent.getData(), null, null, null, null);
		} while (!localCursor.moveToFirst());
		String str1 = removeExtraCharacters(localCursor.getString(localCursor.getColumnIndexOrThrow("data1")));
		String str2 = str1.substring(-10 + str1.length());
		if(type.equalsIgnoreCase("mobile"))
		{
		com.payacrossmobileapp.recharge.MobileActivity.edtMobile.setText(str2);
		}
		else if(type.equalsIgnoreCase("data"))
		{
			com.payacrossmobileapp.recharge.DatacardActivity.edtMobile.setText(str2);
		}
		else if(type.equalsIgnoreCase("post"))
		{
			com.payacrossmobileapp.recharge.PostpaidActivity.edtMobile.setText(str2);
		}
		else
		{
			Toast.makeText(getApplicationContext(), "Something wrong!", Toast.LENGTH_LONG).show();
		}
		finish();
	}
	
	private String removeExtraCharacters(String paramString)
	{
		try
		{
			paramString = paramString.replace("(", "");
			paramString = paramString.replace(")", "");
			paramString = paramString.replace("-", "");
			paramString = paramString.replace(" ", "");
			String str = paramString.replace("+", "");
			return str;
		}
		catch (Exception localException)
		{
			Log.e("Cal Reciever_R", localException.toString());
		}
		return paramString;
	}
}
