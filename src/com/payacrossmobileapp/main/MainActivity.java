package com.payacrossmobileapp.main;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TabActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.telephony.SmsManager;
import android.text.TextUtils.TruncateAt;
import android.util.Base64;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.payacrossmobileapp.adapter.DownLineAdapterList;
import com.payacrossmobileapp.adapter.Group;
import com.payacrossmobileapp.adapter.MessageAdapterList;
import com.payacrossmobileapp.main.R.anim;
import com.payacrossmobileapp.recharge.DTHActivity;
import com.payacrossmobileapp.recharge.DatacardActivity;
import com.payacrossmobileapp.recharge.ElectricityBill;
import com.payacrossmobileapp.recharge.MobileActivity;
import com.payacrossmobileapp.recharge.PostpaidActivity;
import com.payacrossmobileapp.report.BalanceTransferReport;
import com.payacrossmobileapp.report.DayReportActivity;
import com.payacrossmobileapp.report.DownlineListActivity;
import com.payacrossmobileapp.report.LastTenActivity;
import com.payacrossmobileapp.report.MobileReportActivity;
import com.payacrossmobileapp.report.PurchaseReportActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@SuppressWarnings("deprecation")
public class MainActivity extends TabActivity {
    public static TextView txtBalance;
    public static SharedPreferences myPreference;
    public static SharedPreferences loginPreference;
    //
    public static SharedPreferences.Editor loginEditor;
    public static SharedPreferences.Editor myEditor;
    static String receiver = null;
    private static int RESULT_LOAD_IMAGE = 1;
    String[] status, strdate, operator, stramount, transid, detail, refundstat, smssent, statmentid, mobno;
    Context context = this;
    String balStatus = "", balAmt, msgstring;
    AutoCompleteTextView mem_id;
    NetworkState ns = new NetworkState();
    TextView emailId, txthelpPhnNoone, txtUserName;
    Typeface tf;
    RoundImage roundedImage;
    Dialog dialog;
    ImageButton balnce, refresh;
    TabHost tabHost;
    Intent intent;
    Button btnOk, btnChange, btnCancel, btnSend, btnAdd, btnSave;
    Animation animVanish;
    EditText newMobileNumber, oldPassword, newPassword, oldMobileNumber, txtDate;
    EditText receiverId, pinNo, amount, addNotification, serverNo, memberid;
    String type;
    ProgressDialog pDialog;
    Context localcontext = this;
    TextView txtNotification;
    String changeMobTitle = "Change Mobile Number", changePassTitle = "Change Password", version;
    int verCode;
    ListView listview;
    String[] id, name, mobile, balance;
    DownLineAdapterList adapter;
    CheckBox chkSave;
    LinearLayout llTitle;
    Spinner sp_receiverId;
    ArrayAdapter<String> retailerid;
    String retailer_id[], retid;
    String picturePath;
    Bitmap bitmap = null;
    SharedPreferences shre;
    Button Reset, Clear;
    EditText mobNo, address, password, edtname;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date;
    SparseArray<Group> groups = new SparseArray<Group>();
    ExpandableListView listView;
    Bundle bundle;
    LinearLayout title;
    private Boolean saveNum;
    private Menu mMenu;

    //send Message method
    public static void sendMessage(String msg, final Context contextthis) {
        try {
            final String msg_final = msg;
            final Context contextthis_final = contextthis;
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(contextthis);
            // set title
            alertDialogBuilder.setTitle("Message Confirm");
            alertDialogBuilder.setIcon(R.mipmap.ic_launcher);
            // set dialog message
            alertDialogBuilder
                    .setMessage(String.format("To: %s \nContent: %s", receiver, msg))
                    .setCancelable(false)
                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (receiver != null && receiver.length() > 0) {
                                SmsManager smsManager = SmsManager.getDefault();
                                smsManager.sendTextMessage(receiver, null, msg_final, null, null);
                                Toast.makeText(
                                        contextthis_final,
                                        "Request Send Successfully. Please Wait! ",
                                        Toast.LENGTH_LONG).show();
                            } else
                                Toast.makeText(
                                        contextthis_final,
                                        "Set Receiver Number ",
                                        Toast.LENGTH_LONG).

                                        show();
                        }
                    })
                    .setPositiveButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        } catch (Exception e) {
            Toast.makeText(contextthis, "" + e, Toast.LENGTH_LONG).show();
        }
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
        int targetWidth = 1000;
        int targetHeight = 1000;
        Bitmap targetBitmap = Bitmap.createBitmap(targetWidth,
                targetHeight, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(targetBitmap);
        Path path = new Path();
        path.addCircle(((float) targetWidth - 1) / 2,
                ((float) targetHeight - 1) / 2,
                (Math.min(((float) targetWidth),
                        ((float) targetHeight)) / 2),
                Path.Direction.CCW);

        canvas.clipPath(path);
        Bitmap sourceBitmap = bitmap;
        canvas.drawBitmap(sourceBitmap,
                new Rect(0, 0, sourceBitmap.getWidth(),
                        sourceBitmap.getHeight()),
                new Rect(0, 0, targetWidth, targetHeight), null);
        return targetBitmap;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (type.equalsIgnoreCase("GPRS")) {
            if (ns.isInternetAvailable(context)) {
                new LoadBalCheckTask().execute();
            } else {
                Toast.makeText(context, "Internet connection is not available", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


       /* try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }*/
        /*version = pInfo.versionName;
        verCode = pInfo.versionCode;*/

       /* Log.e("versionname", pInfo.versionName);
        Log.e("versioncode", String.valueOf(pInfo.versionCode));*/
        pDialog = new ProgressDialog(MainActivity.this);

        txtNotification = (TextView) findViewById(R.id.txtnotification);
        txtNotification.setSelected(true);
        txtNotification.setEllipsize(TruncateAt.MARQUEE);
        txtNotification.setSingleLine(true);

        llTitle = (LinearLayout) findViewById(R.id.llTitle);

        Bundle extras = getIntent().getExtras();
        type = extras.getString("type");

        llTitle = (LinearLayout) findViewById(R.id.title);
        if (type.equalsIgnoreCase("SMS")) {
            llTitle.setVisibility(View.GONE);
            txtNotification.setVisibility(View.GONE);
        } else {
            txtUserName = (TextView) findViewById(R.id.name);
            txtBalance = (TextView) findViewById(R.id.balance);

            txtUserName.setText(Common.name);

            if (ns.isInternetAvailable(context)) {
                new LoadBalCheckTask().execute();
            } else {
                Toast.makeText(context, "Internet connection is not available", Toast.LENGTH_SHORT).show();
            }
        }

        if (type.equalsIgnoreCase("GPRS")) {
            if (ns.isInternetAvailable(context)) {
                new NotificationTask().execute();
            } else {
                pDialog.dismiss();
                Toast.makeText(MainActivity.this, "Internet connection is not available", Toast.LENGTH_LONG).show();
            }
        }

        myPreference = getSharedPreferences("myPreference", MODE_PRIVATE);
        myEditor = myPreference.edit();
        receiver = myPreference.getString("serverno", "");

        loginPreference = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        loginEditor = loginPreference.edit();


        tabHost = getTabHost();
        TabHost.TabSpec spec;
        //Bundle bundle = new Bundle();
        bundle = new Bundle();

        intent = new Intent().setClass(this, MobileActivity.class);
        bundle.putString("type", type);
        bundle.putString("rechtype", "MOBILE");
        intent.putExtras(bundle);
        spec = tabHost.newTabSpec("Mobile")
                .setIndicator("MOB") //,getResources().getDrawable(R.drawable.mobile_tab)
                .setContent(intent);//res.getDrawable(R.drawable.tabimage1)
        tabHost.addTab(spec);

        intent = new Intent().setClass(this, DTHActivity.class);
        bundle.putString("type", type);
        bundle.putString("rechtype", "DTH");
        intent.putExtras(bundle);
        spec = tabHost.newTabSpec("DTH Recharge")
                .setIndicator("DTH")
                .setContent(intent);
        tabHost.addTab(spec);

        intent = new Intent().setClass(this, DatacardActivity.class);
        bundle.putString("type", type);
        bundle.putString("rechtype", "DC");
        intent.putExtras(bundle);
        spec = tabHost.newTabSpec("Data Recharge")
                .setIndicator("DATA")  //,getResources().getDrawable(R.drawable.datacard_tab)
                .setContent(intent);
        tabHost.addTab(spec);

        intent = new Intent().setClass(this, PostpaidActivity.class);
        bundle.putString("type", type);
        bundle.putString("rechtype", "POST");
        intent.putExtras(bundle);
        spec = tabHost.newTabSpec("Post")
                .setIndicator("POST")
                .setContent(intent);
        tabHost.addTab(spec);

        intent = new Intent().setClass(this, ElectricityBill.class);
        bundle.putString("type", type);
        bundle.putString("rechtype", "UTIL");
        intent.putExtras(bundle);
        spec = tabHost.newTabSpec("util")
                .setIndicator("UTIL")
                .setContent(intent);
        tabHost.addTab(spec);
        forceShowActionBarOverflowMenu();


    }

    //Menu List Layout
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mMenu = menu;
        Log.i("User", type + "::" + Common.usertype);
        try {
            if (type.equalsIgnoreCase("GPRS")) {
                if (Common.usertype.equalsIgnoreCase("Retailer")) {
                    getMenuInflater().inflate(R.menu.main, menu);
                    shre = PreferenceManager.getDefaultSharedPreferences(this);
                    String base64 = shre.getString("image_data", "");
                    if (base64.length() > 10) {
                        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);


                    }
                } else if (Common.usertype.equalsIgnoreCase("Owner")) {
                    getMenuInflater().inflate(R.menu.adminmenulist, menu);

                    shre = PreferenceManager.getDefaultSharedPreferences(this);
                    String base64 = shre.getString("image_data", "");

                    if (base64.length() > 10) {

                        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);


                    }
                } else if (Common.usertype.equalsIgnoreCase("Distributer") || Common.usertype.equalsIgnoreCase("Distributor")) {
                    getMenuInflater().inflate(R.menu.distributormenulist, menu);

                    shre = PreferenceManager.getDefaultSharedPreferences(this);
                    String base64 = shre.getString("image_data", "");

                    if (base64.length() > 10) {

                        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);


                    }
                } else {
                    Toast.makeText(localcontext, "Wrong response in User", Toast.LENGTH_LONG).show();
                }
            } else {
                getMenuInflater().inflate(R.menu.sms_menulist, menu);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        menu.findItem(R.id.bal_check);

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();

            FileInputStream in;
            BufferedInputStream buf;

            try {
                in = new FileInputStream(picturePath);

                buf = new BufferedInputStream(in);

                Bitmap realImage = BitmapFactory.decodeStream(in);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                realImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();

                //roundedImage = new RoundImage(realImage);

                String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

                shre = PreferenceManager.getDefaultSharedPreferences(this);
                Editor edit = shre.edit();
                edit.putString("image_data", encodedImage);
                edit.commit();

				/*MenuItem item ;
                item= mMenu.findItem(R.id.profileimg);
				item.setIcon(new BitmapDrawable(getRoundedCornerBitmap(realImage)));//new BitmapDrawable(realImage)
*/
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    //Menu List Items
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            //Menu Profile pic


            case R.id.last_report:
                Intent last = new Intent(MainActivity.this, LastTenActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("type", type);
                last.putExtras(bundle);
                startActivity(last);
                break;

            //Menu Profile pic
            case R.id.purchasereport:
                Intent purchase = new Intent(MainActivity.this, PurchaseReportActivity.class);
                Bundle purchasebundle = new Bundle();
                purchasebundle.putString("type", type);
                purchase.putExtras(purchasebundle);
                startActivity(purchase);
                finish();
                break;

            case R.id.browseplan:
                Intent browse = new Intent(MainActivity.this, BrowseplanActivity.class);
                Bundle browsebundle = new Bundle();
                browsebundle.putString("type", type);
                browse.putExtras(browsebundle);
                startActivity(browse);
                finish();
                break;



           /* case R.id.balanceview:
                Intent balview = new Intent(MainActivity.this, CommissionReport.class);
                Bundle balviewbundle = new Bundle();
                balviewbundle.putString("type", type);
                balview.putExtras(balviewbundle);
                startActivity(balview);
                finish();
                break;*/

            case R.id.baltransreport:
                Intent baltrans = new Intent(MainActivity.this, BalanceTransferReport.class);
                Bundle baltranreportbundle = new Bundle();
                baltranreportbundle.putString("type", type);
                baltrans.putExtras(baltranreportbundle);
                startActivity(baltrans);
                finish();
                break;

            //Menu Profile pic
        /*case R.id.profileimg:
			Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			startActivityForResult(i, RESULT_LOAD_IMAGE);
			break;
*/
            //Menu Change Mobile Number
            case R.id.change_mobileno:
                dialog = new Dialog(MainActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
                dialog.setContentView(R.layout.activity_changemobile);
                dialog.setTitle(changeMobTitle);
                dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
                animVanish = AnimationUtils.loadAnimation(MainActivity.this, anim.anim_vanish);
                newMobileNumber = (EditText) dialog.findViewById(R.id.edtnewmobileno);
                oldMobileNumber = (EditText) dialog.findViewById(R.id.edtoldmobno);

                btnChange = (Button) dialog.findViewById(R.id.btnChange);
                btnCancel = (Button) dialog.findViewById(R.id.btnCancel);

                dialog.show();

                btnChange.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setAnimation(animVanish);
                        if (oldMobileNumber.getText().toString().trim().equals("")) {
                            oldMobileNumber.setError("Please enter mobile number");
                            oldMobileNumber.requestFocus();
                        } else if (newMobileNumber.getText().toString().trim().equals("")) {
                            newMobileNumber.setError("Please enter mobile number");
                            newMobileNumber.requestFocus();
                        } else {
                            if (Common.internet_status) {
                                new ChangeMobTask().execute();
                            } else {
                                pDialog.dismiss();
                                Toast.makeText(MainActivity.this, "Internet connection is not available", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
                btnCancel.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setAnimation(animVanish);
                        dialog.hide();
                    }

                });
                break;

            //Menu Change Password
            case R.id.changepass:
                dialog = new Dialog(MainActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
                dialog.setContentView(R.layout.activity_changepassword);
                dialog.setTitle(changePassTitle);
                dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
                animVanish = AnimationUtils.loadAnimation(MainActivity.this, anim.anim_vanish);

                oldPassword = (EditText) dialog.findViewById(R.id.edtOldpasswod);
                newPassword = (EditText) dialog.findViewById(R.id.edtNewPassword);
                btnChange = (Button) dialog.findViewById(R.id.btnChange);
                btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
                dialog.show();

                btnChange.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setAnimation(animVanish);
                        if (oldPassword.getText().toString().trim().equals("")) {
                            oldPassword.setError("Please enter password");
                            oldPassword.requestFocus();
                        } else if (newPassword.getText().toString().trim().equals("")) {
                            newPassword.setError("Please enter password");
                            newPassword.requestFocus();
                        } else {
                            if (ns.isInternetAvailable(context)) {
                                new ChangePassTask().execute();
                            } else {
                                pDialog.dismiss();
                                Toast.makeText(MainActivity.this, "Internet connection is not available", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
                btnCancel.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setAnimation(animVanish);
                        dialog.dismiss();
                    }
                });
                break;

            //Menu Day Report
            case R.id.dayreport:

                Intent dayReport = new Intent(MainActivity.this, DayReportActivity.class);
                bundle = new Bundle();
                bundle.putString("type", type);
                dayReport.putExtras(bundle);
                startActivity(dayReport);
                break;
            //Menu search mobile
            case R.id.searchmobile:
                Intent search_mobile = new Intent(MainActivity.this, MobileReportActivity.class);
                bundle = new Bundle();
                bundle.putString("type", type);
                search_mobile.putExtras(bundle);
                startActivity(search_mobile);
                break;


            //Menu Add Notification
            case R.id.notification:

                dialog = new Dialog(MainActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
                dialog.setContentView(R.layout.activity_nofication);
                dialog.setTitle("Add Notification");
                dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
                animVanish = AnimationUtils.loadAnimation(MainActivity.this, anim.anim_vanish);

                addNotification = (EditText) dialog.findViewById(R.id.edtNotoication);

                btnAdd = (Button) dialog.findViewById(R.id.btnAdd);
                btnCancel = (Button) dialog.findViewById(R.id.btnCancel);

                dialog.show();

                btnAdd.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setAnimation(animVanish);
                        if (addNotification.getText().toString().trim().equals("")) {
                            addNotification.setError("Please add notification");
                            addNotification.requestFocus();
                        } else {
                            if (ns.isInternetAvailable(context)) {
                                new AddNotificationTask().execute();
                            } else {
                                pDialog.dismiss();
                                Toast.makeText(MainActivity.this, "Internet connection is not available", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
                btnCancel.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setAnimation(animVanish);
                        dialog.dismiss();
                    }
                });
                break;

            //Menu Setting
            case R.id.sms_setting:

                dialog = new Dialog(MainActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
                dialog.setContentView(R.layout.activity_setting);
                dialog.setTitle("Setting");
                dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
                animVanish = AnimationUtils.loadAnimation(MainActivity.this, anim.anim_vanish);

                serverNo = (EditText) dialog.findViewById(R.id.txt_reciverno);
                chkSave = (CheckBox) dialog.findViewById(R.id.chksave);

                btnSave = (Button) dialog.findViewById(R.id.btn_save);
                btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);

                dialog.show();

                String server = myPreference.getString("serverno", "");
                saveNum = myPreference.getBoolean("saveNum", false);
                if (saveNum == true) {
                    serverNo.setText(server);
                    chkSave.setChecked(true);
                }

                btnSave.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setAnimation(animVanish);
                        MainActivity.receiver = serverNo.getText().toString().trim();
                        if (receiver.length() != 0) {
                            if (chkSave.isChecked()) {
                                myEditor.putString("serverno", receiver);
                                myEditor.putBoolean("saveNum", true);
                                myEditor.commit();
                                Toast.makeText(getApplicationContext(), "Saved successfully", Toast.LENGTH_LONG).show();
                                dialog.dismiss();
                            } else {
                                myEditor.clear();
                                myEditor.commit();
                                dialog.dismiss();
                            }
                        } else {
                            //Toast.makeText(getApplicationContext(), "Set server no", Toast.LENGTH_LONG).show();
                            serverNo.setError("Set Server Number");
                            serverNo.requestFocus();
                        }
                    }
                });

                btnCancel.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setAnimation(animVanish);
                        dialog.dismiss();
                    }
                });
                break;

            //Menu Balance Check
            case R.id.bal_check:
                if (type.equalsIgnoreCase("SMS")) {
                    sendMessage("bal", MainActivity.this);
                } else {
                    try {
                        if (Common.internet_status) {
                            new BalCheckTask().execute();
                        } else {
                            pDialog.dismiss();
                            Toast.makeText(MainActivity.this, "Internet connection is not available", Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            //Menu Help
            case R.id.Help:

                dialog = new Dialog(MainActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
                dialog.setContentView(R.layout.activity_help);
                dialog.setTitle("Help");
                dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
                animVanish = AnimationUtils.loadAnimation(MainActivity.this, anim.anim_vanish);

                txthelpPhnNoone = (TextView) dialog.findViewById(R.id.txthelpphnno);

                emailId = (TextView) dialog.findViewById(R.id.txtemail);

                txthelpPhnNoone.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent mob = new Intent(android.content.Intent.ACTION_CALL, Uri.parse("tel:+91" + txthelpPhnNoone.getText().toString().trim()));
                        startActivity(mob);
                    }
                });

                emailId.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_EMAIL, new String[]{emailId.getText().toString()}); //new String[]{"recipient@example.com"}
                        i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
                        i.putExtra(Intent.EXTRA_TEXT, "body of email");
                        try {
                            startActivity(Intent.createChooser(i, "Send mail..."));
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(MainActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                btnOk = (Button) dialog.findViewById(R.id.btnok);
                dialog.show();
                btnOk.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        v.setAnimation(animVanish);
                        dialog.hide();
                    }
                });
                break;

            //Menu Balance Transfer
            case R.id.bal_transfer:
                Intent balatrans = new Intent(MainActivity.this, BalanceTransfer.class);
                bundle = new Bundle();
                bundle.putString("type", type);
                balatrans.putExtras(bundle);
                startActivity(balatrans);
                break;

            //Downline List
            case R.id.downline:

                if (type.equalsIgnoreCase("SMS")) {
                    //msgstring=String.format("%s %s %s", smsopcode,mobno,amount);
                    sendMessage("DL", MainActivity.this);
                } else {

                    Intent downList = new Intent(MainActivity.this, DownlineListActivity.class);
                    bundle = new Bundle();
                    bundle.putString("type", type);
                    downList.putExtras(bundle);
                    startActivity(downList);
                    //finish();
                }

                break;

            //Menu User Activation
            case R.id.user_activation:

                dialog = new Dialog(MainActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
                dialog.setContentView(R.layout.activity_signup);
                dialog.setTitle("User Activation");
                dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);

                Reset = (Button) dialog.findViewById(R.id.btnreset);
                Clear = (Button) dialog.findViewById(R.id.btnCancel);

                mobNo = (EditText) dialog.findViewById(R.id.edtmob);
                address = (EditText) dialog.findViewById(R.id.edtaddress);
                edtname = (EditText) dialog.findViewById(R.id.edtname);
                password = (EditText) dialog.findViewById(R.id.edtpass);

                dialog.show();

                Reset.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (ns.isInternetAvailable(context)) {
                            if (edtname.length() == 0) {
                                edtname.setError("Enter Name");
                                edtname.requestFocus();
                            } else if (address.length() == 0) {
                                address.setError("Enter Address");
                                address.requestFocus();
                            } else if (mobNo.length() == 0) {
                                mobNo.setError("Enter Mobile No");
                                mobNo.requestFocus();
                            } else if (password.length() == 0) {
                                password.setError("Enter Pin");
                                password.requestFocus();
                            } else {
                                if (type.equalsIgnoreCase("SMS")) {
                                    //R.Name.Address.Mob no.1234
                                    msgstring = edtname.getText().toString() + "." + address.getText().toString() + "." + mobNo.getText().toString() + "." +
                                            password.getText().toString();
                                    sendMessage("R." + msgstring, MainActivity.this);
                                } else {
                                    new SignUpTask().execute();
                                }
                            }
                        } else {
                            pDialog.dismiss();
                            Toast.makeText(MainActivity.this, "Internet connection is not available", Toast.LENGTH_LONG).show();
                        }
                    }
                });

                Clear.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.hide();
                    }
                });

                break;

            //Menu Logout
            case R.id.logout:
                loginEditor.putBoolean("logoutStatus", false);
                loginEditor.commit();
                Intent logout = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(logout);
                finish();
                break;
            default:
                return super.onOptionsItemSelected(item);

        }
        return true;
    }

    private void forceShowActionBarOverflowMenu() {
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //End asynchronous task for Set DownLine List in Balance Transfer

    //Parse the JSON response
    void parseJSONSetDownline(String result) {
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(context, "Downline Wrong response", Toast.LENGTH_LONG).show();
                } else {
                    retailer_id = new String[jsonArray.length()];

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        retailer_id[i] = jo.getString("MemberIdName");// + " - " + jo.getString("Member Name");
                    }
                    /*retailerid = new ArrayAdapter<String>(DistributorActivity.this, android.R.layout.simple_spinner_item, retailer_id);
                    retailerid.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					*//*retailerid.remove(retailer_id[0]);
								retailerid.notifyDataSetChanged();	*//*
                    sp_receiverId.setAdapter(retailerid);*/
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                            (this, android.R.layout.select_dialog_item, retailer_id);
                    //retailerid = new ArrayAdapter<String>(AdminActivity.this,android.R.layout.simple_list_item_1, retailer_id);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    /*retailerid.remove(retailer_id[0]);
                                retailerid.notifyDataSetChanged();	*/
                    mem_id = (AutoCompleteTextView) dialog.findViewById(R.id.edt_member_id);
                    mem_id.setThreshold(1);
                    mem_id.setAdapter(adapter);
                    Log.i("Result of bal trans", mem_id.toString());
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }

    //Parse the JSON response
    void parseJSONSignUp(String result) {
        String status = null;
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(localcontext, "Wrong User", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);

                        status = jo.getString("Status");

                    }//for

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Message");
                    builder.setIcon(R.mipmap.ic_launcher);
                    builder.setMessage(String.format("Status : %s", status));
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //dialog.dismiss();
                            edtname.setText("");
                            mobNo.setText("");
                            address.setText("");
                            password.setText("");
                            edtname.requestFocus();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }
    //End of asynchronous task for New SignUp/ Activation

    //Parse the JSON response
    void parseJSONDayReport(String result) {
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    // Toast.makeText(MainActivity.this, "Wrong response", Toast.LENGTH_LONG).show();
                } else {
                    status = new String[jsonArray.length()];
                    strdate = new String[jsonArray.length()];
                    operator = new String[jsonArray.length()];
                    stramount = new String[jsonArray.length()];
                    transid = new String[jsonArray.length()];
                    balance = new String[jsonArray.length()];
                    mobno = new String[jsonArray.length()];

                    try {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jo = jsonArray.getJSONObject(i);

                            status[i] = jo.getString("Status");
                            strdate[i] = jo.getString("Date Time");
                            operator[i] = jo.getString("Operator");
                            stramount[i] = jo.getString("Amount");
                            transid[i] = jo.getString("Sn");
                            balance[i] = jo.getString("Previous Balance");
                            mobno[i] = jo.getString("MobileNo");

                            MessageAdapterList adapter = new MessageAdapterList(context, status, strdate, operator, stramount, transid, balance, mobno);
                            listview.setAdapter(adapter);

                        }
                    } catch (Exception f) {
                        MessageAdapterList adapter = null;
                        listview.setAdapter(adapter);
                        JSONObject jo = jsonArray.getJSONObject(0);
                        Toast.makeText(getApplicationContext(), jo.getString("Status"), Toast.LENGTH_LONG).show();
                    }
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }

    //Parse the JSON response
    void parseJSONLoadBalCheck(String result) {

        try {
            if (result != null) {
                try {
                    JSONArray jsonArray = new JSONArray(result);
                    if (jsonArray.length() == 0) {
                        // Toast.makeText(MainActivity.this, "Wrong response", Toast.LENGTH_LONG).show();
                    } else {
                        //Received response from web service
					/*"Status": "489.7500"*/
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jo = jsonArray.getJSONObject(i);
                            balStatus = jo.getString("Status");
                            try {
                                double d = Double.parseDouble(balStatus);
                                balStatus = String.format("%.2f", d);// display only 2 decimal value
                            } catch (NumberFormatException e) {

                            }
                            txtBalance.setText(balStatus);

                            //Log.e("bal",status);
                        }//for
                        //   txtBalance.setText(String.format("%.02f", Float.parseFloat(balStatus)));
                        //txtBalance.setText(balStatus);
                    }
                } catch (JSONException e) {
                    Log.e("JSONError", e.getMessage());
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //End asynchronous task for Day Report

    //Parse the JSON response
    void parseJSONNotification(String result) {
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(localcontext, "Wrong response", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        txtNotification.setText(jo.getString("message"));
                    }
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }

    //Parse the JSON response
    void parseJSONBalCheck(String result) {
        String status = "";

        try {
            if (result != null) {
                try {
                    JSONArray jsonArray = new JSONArray(result);
                    if (jsonArray.length() == 0) {
                        // Toast.makeText(localcontext, "Wrong response", Toast.LENGTH_LONG).show();
                    } else {
                        //Received response from web service
					/*"Status": "489.7500"*/
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jo = jsonArray.getJSONObject(i);
                            status = jo.getString("Status");
                            status = String.format("%.02f", Float.parseFloat(balStatus));
                            //Log.e("bal",status);

                        }//for

                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);//.getParent()
                        builder.setTitle("Message");
                        builder.setIcon(R.mipmap.ic_launcher);
                        builder.setMessage("Your balance is " + status);
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                } catch (JSONException e) {
                    Log.e("JSONError", e.getMessage());
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            Log.e("JSONError", e.getMessage());
        }
    }
    //End asynchronous task for show Balance on dashboard

    //Parse the JSON response
    void parseJSONAddNotification(String result) {
        String status = "";
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(localcontext, "Wrong response", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        status = jo.getString("Status");
                        //Log.e("bal", jo.getString("CURRENT_BALANCE")+" "+bal);
                    }//for

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);//.getParent()
                    builder.setTitle("Message");
                    builder.setIcon(R.mipmap.ic_launcher);
                    builder.setMessage("" + status);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            addNotification.setText("");
                            //dialog.dismiss();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }

    //Parse the JSON response
    void parseJSONChangeMob(String result) {
        String status = "";
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(localcontext, "Wrong response", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        status = jo.getString("Status");
                        //Log.e("bal", jo.getString("Status")+" "+bal);
                    }//for

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);//.getParent()
                    builder.setTitle("Message");
                    builder.setIcon(R.mipmap.ic_launcher);
                    builder.setMessage("" + status);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //dialog.dismiss();
                            newMobileNumber.setText("");
                            oldMobileNumber.setText("");

                            newMobileNumber.requestFocus();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }
    //End asynchronous task for Notification

    //Parse the JSON response
    void parseJSONChangePass(String result) {
        String status = "";
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(localcontext, "Wrong response", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        status = jo.getString("Status");
                        //Log.e("bal", jo.getString("CURRENT_BALANCE")+" "+bal);
                    }//for

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);//.getParent()
                    builder.setTitle("Message");
                    builder.setIcon(R.mipmap.ic_launcher);
                    builder.setMessage("" + status);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //dialog.dismiss();
                            newPassword.setText("");
                            oldPassword.setText("");

                            oldPassword.requestFocus();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }

    //Parse the JSON response
    void parseJSONBalanceTrans(String result) {
        String status = "";

        try {
            if (result != null) {
                try {
                    JSONArray jsonArray = new JSONArray(result);
                    if (jsonArray.length() == 0) {
                        Toast.makeText(localcontext, "Wrong response", Toast.LENGTH_LONG).show();
                    } else {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jo = jsonArray.getJSONObject(i);
                            status = jo.getString("Status");
                            Log.e("baltrans", status);
                        }//for

                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);//.getParent()
                        builder.setTitle("Message");
                        builder.setIcon(R.mipmap.ic_launcher);
                        builder.setMessage("" + status);
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                double f = Double.parseDouble(balStatus);
                                balAmt = amount.getText().toString();
                                double a = Double.parseDouble(balAmt);
                                double amt = f - a;
                                String strAmt = Double.toString(amt);
                                txtBalance.setText(strAmt);
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(20000);
                                } catch (InterruptedException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                                if (ns.isInternetAvailable(context)) {
                                    new LoadBalCheckTask().execute();
                                } else {
                                    Toast.makeText(localcontext, "Internet connection is not available", Toast.LENGTH_LONG).show();
                                }
                            }
                        }).start();
                    }
                } catch (JSONException e) {
                    Log.e("JSONError", e.getMessage());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    //End asynchronous task for Balance check

    //Parse the JSON response
    void parseJSONDownlineList(String result) {
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(localcontext, "Wrong response", Toast.LENGTH_LONG).show();
                } else {
                    id = new String[jsonArray.length()];
                    name = new String[jsonArray.length()];
                    mobile = new String[jsonArray.length()];
                    balance = new String[jsonArray.length()];

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        id[i] = jo.getString("Member ID");
                        name[i] = jo.getString("Member Name");
                        mobile[i] = jo.getString("MobileNo");
                        balance[i] = jo.getString("Balance");
                    }
                    adapter = new DownLineAdapterList(getApplicationContext(), id, name, mobile, balance);
                    listview.setAdapter(adapter);
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(MainActivity.this,LoginActivity.class);
        startActivity(intent);
        finish();


    }
    //End asynchronous task for Add Notification

    //Start asynchronous task for Set DownLine List in Balance Transfer
    public class SetDownlineListTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "DownlineService.asmx/UserDownline";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");

            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            //Log.e("urlbal", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                List nameValuePairs = new ArrayList();
                nameValuePairs.add(new BasicNameValuePair("LapuId", Common.lapuid));
                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONSetDownline(result);
        }
    }

    //Start asynchronous task for New SignUp/ Activation
    public class SignUpTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "UserActivation.asmx/NewUserActivation";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];

                n = in.read(b);

                if (n > 0) out.append(new String(b, 0, n));

            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                List nameValuePairs = new ArrayList();
                // add an HTTP variable and value pair
                nameValuePairs.add(new BasicNameValuePair("Name", edtname.getText().toString().trim()));
                nameValuePairs.add(new BasicNameValuePair("Pin", password.getText().toString().trim()));
                nameValuePairs.add(new BasicNameValuePair("MobNumber", mobNo.getText().toString().trim()));
                nameValuePairs.add(new BasicNameValuePair("Address", address.getText().toString().trim()));
                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                request.setURI(new URI(url));
                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONSignUp(result);
        }
    }
    //End asynchronous task for Change Mobile

    //Start asynchronous task for Day Report
    public class DayeReportTask extends AsyncTask<Void, Void, String> {
        String date = String.format("%s", txtDate.getText().toString().trim());

        String url = Common.COMMON_URL + "DayReport.asmx/DayRechargeReport";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            //Log.e("urlbal", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                List nameValuePairs = new ArrayList();

                request.addHeader("RechargeID", Common.recharge_id);
                request.addHeader("RegIdNo", Common.regid);

                nameValuePairs.add(new BasicNameValuePair("Date", txtDate.getText().toString().trim()));
                nameValuePairs.add(new BasicNameValuePair("DeviceID", Common.IMEI));
                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONDayReport(result);
        }
    }

    //Start asynchronous task for show Balance on dashboard
    public class LoadBalCheckTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "LoginUser.asmx/GetBalance";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }

            return out.toString();
        }

        @Override
        protected void onPreExecute() {
			/*pDialog.setMessage("Please wait..");
			pDialog.setIndeterminate(true);
			pDialog.setCancelable(false);
			pDialog.show();*/
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            Log.e("url", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONLoadBalCheck(result);
        }
    }
    //End asynchronous task for Change Password

    //Start asynchronous task for Notification
    public class NotificationTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "AddNotification.asmx/ShowNotification";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];

                n = in.read(b);

                if (n > 0) out.append(new String(b, 0, n));

            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            //Log.e("urlbal", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));
                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);


            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONNotification(result);
        }
    }

    //Start asynchronous task for Balance check
    public class BalCheckTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "LoginUser.asmx/GetBalance";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }

            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            Log.e("url", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

				/*List nameValuePairs = new ArrayList();
				// add an HTTP variable and value pair
				nameValuePairs.add(new BasicNameValuePair("RechargeID", Common.recharge_id));
				nameValuePairs.add(new BasicNameValuePair("Regid", Common.regid));
				request.setEntity(new UrlEncodedFormEntity(nameValuePairs));*/

                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);

				/*HttpClient client = new DefaultHttpClient();
				HttpGet request = new HttpGet();
				request.setURI(new URI(url));
				HttpResponse response = client.execute(request);
				request.addHeader("Regid",Common.regid);
				request.addHeader("RechargeID",Common.recharge_id);
				Log.e("Regid", Common.regid);
				Log.e("RechargeID", Common.recharge_id);
				HttpEntity entity = response.getEntity();
				result = getASCIIContentFromEntity(entity);

				Log.e("Result",result);*/

                //parseJSON(result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONBalCheck(result);
        }
    }
    //End asynchronous task for Balance Transfer

    //Start asynchronous task for Add Notification
    public class AddNotificationTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "AddNotification.asmx/addNotication";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");

            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                List nameValuePairs = new ArrayList();
                // add an HTTP variable and value pair
                nameValuePairs.add(new BasicNameValuePair("NotificationText", addNotification.getText().toString().trim()));

                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONAddNotification(result);
            new NotificationTask().execute();
        }
    }

    //Start asynchronous task for Change Mobile
    public class ChangeMobTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "ChangeMobileNo.asmx/ChangeMobileno";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];

                n = in.read(b);

                if (n > 0) out.append(new String(b, 0, n));

            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            //Log.e("urlbal", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                List nameValuePairs = new ArrayList();
                // add an HTTP variable and value pair
                nameValuePairs.add(new BasicNameValuePair("Lapuid", Common.lapuid));
                nameValuePairs.add(new BasicNameValuePair("OldMobileNo", oldMobileNumber.getText().toString().trim()));
                nameValuePairs.add(new BasicNameValuePair("NewMobileNo", newMobileNumber.getText().toString().trim()));

                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);


            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONChangeMob(result);
        }
    }
    //End asynchronous task for DownLine List

    //Start asynchronous task for Change Password
    public class ChangePassTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "ChangePassword.asmx/ChangePassWord";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];

                n = in.read(b);

                if (n > 0) out.append(new String(b, 0, n));

            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            //Log.e("urlbal", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                List nameValuePairs = new ArrayList();
                // add an HTTP variable and value pair
                nameValuePairs.add(new BasicNameValuePair("LapuId", Common.lapuid));
                nameValuePairs.add(new BasicNameValuePair("oldpass", oldPassword.getText().toString().trim()));
                nameValuePairs.add(new BasicNameValuePair("newpass", newPassword.getText().toString().trim()));
				/*nameValuePairs.add(new BasicNameValuePair("RechargeID", Common.recharge_id));
				nameValuePairs.add(new BasicNameValuePair("RegIdNo", Common.regid));*/

                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);


            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONChangePass(result);
        }
    }

    //Start asynchronous task for Balance Transfer
    public class BalanceTransTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "BalaneTransfer.asmx/UserBalanceTransfer";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            //Log.e("urlbal", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                List nameValuePairs = new ArrayList();
                // add an HTTP variable and value pair
                nameValuePairs.add(new BasicNameValuePair("MemberId", retid));
                nameValuePairs.add(new BasicNameValuePair("Amount", amount.getText().toString().trim()));
//                nameValuePairs.add(new BasicNameValuePair("Pin", pinNo.getText().toString().trim()));

                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONBalanceTrans(result);
        }
    }

    //Start asynchronous task for DownLine List
    public class DownlineListTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "DownlineService.asmx/UserDownline";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            //Log.e("urlbal", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                List nameValuePairs = new ArrayList();
                nameValuePairs.add(new BasicNameValuePair("LapuId", Common.lapuid));
                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONDownlineList(result);
        }
    }
}
