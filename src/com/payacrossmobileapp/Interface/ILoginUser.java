package com.payacrossmobileapp.Interface;

import com.payacrossmobileapp.Json.LoginDetailsPojo;

import java.util.ArrayList;



public interface ILoginUser {
    void getLoginDetails(ArrayList<LoginDetailsPojo> logindetaillist);
    void getLoginErrorMessage(String result);
}
