package com.payacrossmobileapp.async;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.payacrossmobileapp.Interface.ILoginUser;

import com.payacrossmobileapp.Json.LoginDetailsPojo;

import com.payacrossmobileapp.main.Common;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Somnath-Laptop on 05-Jan-2017.
 */

public class LoginUserAsync extends AsyncTask<String, Void, String> {
    public ILoginUser delegate = null;


    String result = null;
    String key;
    String line;

    @Override
    protected String doInBackground(String... params) {
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        String userName = params[0];
        String password = params[1];
        try {

            URL url = new URL(Common.COMMON_URL + "LoginUser.asmx/ValidateUser?IP=" + Common.IMEI);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setRequestProperty("userName", userName);
            urlConnection.setRequestProperty("password", password);
            urlConnection.connect();
            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));
            while ((line = reader.readLine()) != null) {
                // buffer.append(line + "\n");
                return line;
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                return null;
            }
            result = buffer.toString();
            return result;
        } catch (IOException e) {
            Log.e("LoginAsync", "Error ", e);
            return null;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }

        }
    }

    @Override
    protected void onPostExecute(String rechargeresult) {

        parseJSON(rechargeresult);

    }

    //Parse the JSON response
    void parseJSON(String result) {
        if (result != null) {
            try {
                if (result.contains("Please enter valid User name or Password")) {
                    delegate.getLoginErrorMessage(result);

                } else {
                    Gson gson = new Gson();
                    ArrayList<LoginDetailsPojo> logindetaillist = gson.fromJson(result, new TypeToken<ArrayList<LoginDetailsPojo>>() {
                    }.getType());

                    try {
                        delegate.getLoginDetails(logindetaillist);
                    } catch (Exception e) {
                        delegate.getLoginErrorMessage(result);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                delegate.getLoginErrorMessage(result);
            }

        }
    }
}
