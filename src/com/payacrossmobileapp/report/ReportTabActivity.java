package com.payacrossmobileapp.report;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;

import com.payacrossmobileapp.main.R;

public class ReportTabActivity extends TabActivity
{
	TabHost tabHost;
	String type,operatortype;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.rechargetab);
		
		tabHost=getTabHost();
		TabHost.TabSpec spec;
		
		Bundle extras = getIntent().getExtras();
		type=extras.getString("type");
		
		Bundle bundle = new Bundle();
		Intent intentMobile = new Intent().setClass(this, LastTenActivity.class);
		bundle.putString("type",type);
		bundle.putString("rechtype","M");
		intentMobile.putExtras(bundle);
		spec = tabHost.newTabSpec("Mobile")
				.setIndicator("",getResources().getDrawable(R.drawable.mobile_tab))
				.setContent(intentMobile);//res.getDrawable(R.drawable.tabimage1)
		tabHost.addTab(spec);

		Intent intentDth = new Intent().setClass(this, LastTenActivity.class);
		bundle.putString("type",type);
		bundle.putString("rechtype","D");
		intentDth.putExtras(bundle);
		spec = tabHost.newTabSpec("DTH")
				.setIndicator("",getResources().getDrawable(R.drawable.dth_tab))
				.setContent(intentDth);
		tabHost.addTab(spec);

		Intent intentData = new Intent().setClass(this, LastTenActivity.class);
		bundle.putString("type",type);
		bundle.putString("rechtype","C");
		intentData.putExtras(bundle);
		spec = tabHost.newTabSpec("Data")
				.setIndicator("",getResources().getDrawable(R.drawable.datacard_tab))
				.setContent(intentData);
		tabHost.addTab(spec);

		Intent intentBill = new Intent().setClass(this, LastTenActivity.class);
		bundle.putString("type",type);
		bundle.putString("rechtype","P");
		intentBill.putExtras(bundle);
		spec = tabHost.newTabSpec("Post")
				.setIndicator("",getResources().getDrawable(R.drawable.postpaid_tab))
				.setContent(intentBill);
		tabHost.addTab(spec);

		tabHost.setCurrentTab(0);
		
	}

}
