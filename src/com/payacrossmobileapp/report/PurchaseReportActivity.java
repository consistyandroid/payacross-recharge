package com.payacrossmobileapp.report;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.payacrossmobileapp.adapter.PurchaseAdapterList;
import com.payacrossmobileapp.main.BalanceTransfer;
import com.payacrossmobileapp.main.Common;
import com.payacrossmobileapp.main.Log;
import com.payacrossmobileapp.main.LoginActivity;
import com.payacrossmobileapp.main.MainActivity;
import com.payacrossmobileapp.main.R;
import com.payacrossmobileapp.main.R.anim;
import com.payacrossmobileapp.main.browsePlans;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class PurchaseReportActivity extends Activity {
    Button btnChange, btnCancel;
    Animation animVanish;
    ProgressDialog pDialog;

    String[] array_type = {"Select Type", "Debit", "Credit", "Refund"};


    String[] status, strdate, operator, amount, transid, balance, detail, refundstat, smssent, statmentid, mobno, hours, credit, debit, remarks, prebalance;
    ListView listview;
    Context context = this;

    private static int RESULT_LOAD_IMAGE = 1;
    String picturePath;
    Bitmap bitmap = null;
    SharedPreferences shre;
    private Menu mMenu;
    String type, type1, purchasecode, daycode;
    Context localcontext = this;
    Dialog dialog;
    EditText newMobileNumber, oldPassword, newPassword, oldMobileNumber, txtBalance, txtNotification;
    EditText receiverId, pinNo, edtamount, addNotification, serverNo;
    Button btnOk, btnSend, btnAdd, btnSave,Reset, Clear;
    private Boolean saveNum;
    CheckBox chkSave;
    public static SharedPreferences myPreference;
    public static SharedPreferences.Editor myEditor;
    TextView emailId, txthelpPhnNoone, txtUserName;
    Spinner sp_receiverId, sp_Type, sp_days;
    String retailer_id[], retid;
    EditText mobNo, address, password, edtname;
    ArrayAdapter<String> retailerid;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_report);

        animVanish = AnimationUtils.loadAnimation(PurchaseReportActivity.this, anim.anim_vanish);
        pDialog = new ProgressDialog(PurchaseReportActivity.this);

        Bundle extras = getIntent().getExtras();
        type = extras.getString("type");
        type1 = extras.getString("type");
        listview = (ListView) findViewById(R.id.list_lastfive);

        sp_Type = (Spinner) findViewById(R.id.sp_type);
        sp_days = (Spinner) findViewById(R.id.sp_days);

        btnChange = (Button) findViewById(R.id.btnShow);

        ArrayAdapter<String> type = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, array_type);
        type.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_Type.setAdapter(type);


        sp_Type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View vi2ew, int position, long id) {
                purchasecode = array_type[position];
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        ArrayAdapter<String> type1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Common.day_spinnername);
        type1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_days.setAdapter(type1);


        sp_days.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View vi2ew, int position, long id) {
                daycode = Common.day_spinnercode[position];
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        btnChange.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setAnimation(animVanish);
                if (validation()) {
                    if (Common.internet_status) {
                        new PurchaseTask().execute();
                    } else {
                        Toast.makeText(PurchaseReportActivity.this, "Internet connection is not available", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

    }

    private boolean validation() {
        if (sp_Type.getSelectedItemPosition() < 1) {
            Toast.makeText(getApplicationContext(), "Please select type", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (sp_days.getSelectedItemPosition() < 1) {
            Toast.makeText(getApplicationContext(), "Please select day", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    //Menu List Layout
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mMenu = menu;

        try {
            if (type.equalsIgnoreCase("GPRS")) {
                if (Common.usertype.equalsIgnoreCase("Retailer")) {
                    getMenuInflater().inflate(R.menu.main, menu);
                    shre = PreferenceManager.getDefaultSharedPreferences(this);
                    String base64 = shre.getString("image_data", "");
                    if (base64.length() > 10) {
                        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

						/*MenuItem item ;
                        item= menu.findItem(R.id.profileimg);
						item.setIcon(new BitmapDrawable(getRoundedCornerBitmap(decodedByte)));//new BitmapDrawable(decodedByte)
						mMenu = menu;*/
                    }
                } else if (Common.usertype.equalsIgnoreCase("Owner")) {
                    getMenuInflater().inflate(R.menu.adminmenulist, menu);

                    shre = PreferenceManager.getDefaultSharedPreferences(this);
                    String base64 = shre.getString("image_data", "");

                    if (base64.length() > 10) {

                        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

					/*	MenuItem item ;
                        item= menu.findItem(R.id.profileimg);
						item.setIcon(new BitmapDrawable(getRoundedCornerBitmap(decodedByte)));
						mMenu = menu;
*/
                        /*tf = Typeface.createFromAsset(DayReportActivity.this.getAssets(), Common.fonts);
                                item = menu.findItem(R.id.bal_transfer);
								TextView itemuser = (TextView) item.getActionView();

							           tf= Typeface.createFromAsset(this.getAssets(), "Allura-Regular.otf");
							           itemuser.setTypeface(tf);
							           itemuser.setBackgroundColor(Color.TRANSPARENT);*/
                    }
                } else if (Common.usertype.equalsIgnoreCase("Distributer")) {
                    getMenuInflater().inflate(R.menu.distributormenulist, menu);

                    shre = PreferenceManager.getDefaultSharedPreferences(this);
                    String base64 = shre.getString("image_data", "");

                    if (base64.length() > 10) {

                        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

						/*MenuItem item ;
						item= menu.findItem(R.id.profileimg);
						item.setIcon(new BitmapDrawable(getRoundedCornerBitmap(decodedByte)));
						mMenu = menu;*/
                    }
                } else {
                    Toast.makeText(localcontext, "Wrong responce", Toast.LENGTH_LONG).show();
                }
            } else {
                getMenuInflater().inflate(R.menu.sms_menulist, menu);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        menu.findItem(R.id.bal_check);

        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();

            FileInputStream in;
            BufferedInputStream buf;

            try {
                in = new FileInputStream(picturePath);

                buf = new BufferedInputStream(in);

                Bitmap realImage = BitmapFactory.decodeStream(in);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                realImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();

                //roundedImage = new RoundImage(realImage);

                String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

                shre = PreferenceManager.getDefaultSharedPreferences(this);
                Editor edit = shre.edit();
                edit.putString("image_data", encodedImage);
                edit.commit();

				/*MenuItem item ;
				item= mMenu.findItem(R.id.profileimg);
				item.setIcon(new BitmapDrawable(getRoundedCornerBitmap(realImage)));*///new BitmapDrawable(realImage)
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }


    //Menu List Items
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            //Menu Profile pic
            case R.id.last_report:
                Intent last = new Intent(PurchaseReportActivity.this, LastTenActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("type", type);
                last.putExtras(bundle);
                startActivity(last);
                finish();
                break;


         /*   case R.id.balanceview:
                Intent balview = new Intent(PurchaseReportActivity.this,CommissionReport.class);
                Bundle balviewbundle = new Bundle();
                balviewbundle.putString("type", type);
                balview.putExtras(balviewbundle);
                startActivity(balview);
                finish();
                break;*/
            //Menu Profile pic
	/*	case R.id.profileimg:
			Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			startActivityForResult(i, RESULT_LOAD_IMAGE);
			break;
*/
            case R.id.browseplan:
                Intent browse = new Intent(PurchaseReportActivity.this, browsePlans.class);
                Bundle browsebundle = new Bundle();
                browsebundle.putString("type", type);
                browse.putExtras(browsebundle);
                startActivity(browse);
                finish();
                break;

            //Menu Change Mobile Number
            case R.id.change_mobileno:
                dialog = new Dialog(PurchaseReportActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
                dialog.setContentView(R.layout.activity_changemobile);
                dialog.setTitle("Change Mobile");
                dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
                animVanish = AnimationUtils.loadAnimation(PurchaseReportActivity.this, anim.anim_vanish);
                newMobileNumber = (EditText) dialog.findViewById(R.id.edtnewmobileno);
                oldMobileNumber = (EditText) dialog.findViewById(R.id.edtoldmobno);

                btnChange = (Button) dialog.findViewById(R.id.btnChange);
                btnCancel = (Button) dialog.findViewById(R.id.btnCancel);

                dialog.show();

                btnChange.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setAnimation(animVanish);
                        if (oldMobileNumber.getText().toString().trim().equals("")) {
                            oldMobileNumber.setError("Please enter mobile number");
                            oldMobileNumber.requestFocus();
                        } else if (newMobileNumber.getText().toString().trim().equals("")) {
                            newMobileNumber.setError("Please enter mobile number");
                            newMobileNumber.requestFocus();
                        } else {
                            if (Common.internet_status) {
                                new ChangeMobTask().execute();
                            } else {
                                pDialog.dismiss();
                                Toast.makeText(PurchaseReportActivity.this, "Internet connection is not available", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
                btnCancel.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setAnimation(animVanish);
                        dialog.hide();
                    }

                });
                break;

            //Menu Change Password
            case R.id.changepass:
                dialog = new Dialog(PurchaseReportActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
                dialog.setContentView(R.layout.activity_changepassword);
                dialog.setTitle("Change Password");
                dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
                animVanish = AnimationUtils.loadAnimation(PurchaseReportActivity.this, anim.anim_vanish);

                oldPassword = (EditText) dialog.findViewById(R.id.edtOldpasswod);
                newPassword = (EditText) dialog.findViewById(R.id.edtNewPassword);
                btnChange = (Button) dialog.findViewById(R.id.btnChange);
                btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
                dialog.show();

                btnChange.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setAnimation(animVanish);
                        if (oldPassword.getText().toString().trim().equals("")) {
                            oldPassword.setError("Please enter password");
                            oldPassword.requestFocus();
                        } else if (newPassword.getText().toString().trim().equals("")) {
                            newPassword.setError("Please enter password");
                            newPassword.requestFocus();
                        } else {
                            if (Common.internet_status) {
                                new ChangePassTask().execute();
                            } else {
                                pDialog.dismiss();
                                Toast.makeText(PurchaseReportActivity.this, "Internet connection is not available", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
                btnCancel.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setAnimation(animVanish);
                        dialog.dismiss();
                    }
                });
                break;

            //Menu Day Report
            case R.id.dayreport:

                Intent dayReport = new Intent(PurchaseReportActivity.this, DayReportActivity.class);
                bundle = new Bundle();
                bundle.putString("type", type);
                dayReport.putExtras(bundle);
                startActivity(dayReport);
                break;
            case R.id.searchmobile:
                Intent search_mobile = new Intent(PurchaseReportActivity.this, MobileReportActivity.class);
                bundle = new Bundle();
                bundle.putString("type", type);
                search_mobile.putExtras(bundle);
                startActivity(search_mobile);
                break;

            //Menu Add Notification
            case R.id.notification:

                dialog = new Dialog(PurchaseReportActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
                dialog.setContentView(R.layout.activity_nofication);
                dialog.setTitle("Add Notification");
                dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
                animVanish = AnimationUtils.loadAnimation(PurchaseReportActivity.this, anim.anim_vanish);

                addNotification = (EditText) dialog.findViewById(R.id.edtNotoication);

                btnAdd = (Button) dialog.findViewById(R.id.btnAdd);
                btnCancel = (Button) dialog.findViewById(R.id.btnCancel);

                dialog.show();

                btnAdd.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setAnimation(animVanish);
                        if (addNotification.getText().toString().trim().equals("")) {
                            addNotification.setError("Please add notification");
                            addNotification.requestFocus();
                        } else {
                            if (Common.internet_status) {
                                new AddNotificationTask().execute();
                            } else {
                                pDialog.dismiss();
                                Toast.makeText(PurchaseReportActivity.this, "Internet connection is not available", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
                btnCancel.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setAnimation(animVanish);
                        dialog.dismiss();
                    }
                });
                break;

            //Menu Balance Check
            case R.id.bal_check:
                if (type.equalsIgnoreCase("SMS")) {
                    //msgstring=String.format("%s %s %s", smsopcode,mobno,amount);
                    MainActivity.sendMessage("BAL", PurchaseReportActivity.this);
                } else {
                    try {
                        if (Common.internet_status) {
                            new BalCheckTask().execute();
                        } else {
                            pDialog.dismiss();
                            Toast.makeText(PurchaseReportActivity.this, "Internet connection is not available", Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            //Menu Help
            case R.id.Help:

                dialog = new Dialog(PurchaseReportActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
                dialog.setContentView(R.layout.activity_help);
                dialog.setTitle("Help");
                dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
                animVanish = AnimationUtils.loadAnimation(PurchaseReportActivity.this, anim.anim_vanish);

                txthelpPhnNoone = (TextView) dialog.findViewById(R.id.txthelpphnno);

                emailId = (TextView) dialog.findViewById(R.id.txtemail);

                txthelpPhnNoone.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent mob = new Intent(android.content.Intent.ACTION_CALL, Uri.parse("tel:+91" + txthelpPhnNoone.getText().toString().trim()));
                        startActivity(mob);
                    }
                });

                emailId.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_EMAIL, new String [] {emailId.getText().toString()}); //new String[]{"recipient@example.com"}
                        i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
                        i.putExtra(Intent.EXTRA_TEXT, "body of email");
                        try {
                            startActivity(Intent.createChooser(i, "Send mail..."));
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(PurchaseReportActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                btnOk = (Button) dialog.findViewById(R.id.btnok);
                dialog.show();
                btnOk.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        v.setAnimation(animVanish);
                        dialog.hide();
                    }
                });
                break;

            //Menu Balance Transfer
            case R.id.bal_transfer:
                Intent balatrans = new Intent(PurchaseReportActivity.this, BalanceTransfer.class);
                bundle = new Bundle();
                bundle.putString("type", type);
                balatrans.putExtras(bundle);
                startActivity(balatrans);
                break;

            case R.id.downline:

                if (type.equalsIgnoreCase("SMS")) {
                    //msgstring=String.format("%s %s %s", smsopcode,mobno,amount);
                    MainActivity.sendMessage("DL", PurchaseReportActivity.this);
                } else {
                    Intent downList = new Intent(PurchaseReportActivity.this, DownlineListActivity.class);
                    bundle = new Bundle();
                    bundle.putString("type", type);
                    downList.putExtras(bundle);
                    startActivity(downList);
                    finish();
                }

                break;

           /* case R.id.balanceview:
                Intent balview = new Intent(PurchaseReportActivity.this,BalanceView.class);
                Bundle balviewbundle = new Bundle();
                balviewbundle.putString("type",type);
                balview.putExtras(balviewbundle);
                startActivity(balview);
                finish();
                break;*/

            case R.id.baltransreport:
                Intent baltrans = new Intent(PurchaseReportActivity.this,BalanceTransferReport.class);
                Bundle baltranreportbundle = new Bundle();
                baltranreportbundle.putString("type",type);
                baltrans.putExtras(baltranreportbundle);
                startActivity(baltrans);
                finish();
                break;
            //Menu User Activation
            case R.id.user_activation:

                dialog = new Dialog(PurchaseReportActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
                dialog.setContentView(R.layout.activity_signup);
                dialog.setTitle("User Activation");
                dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);

                Reset = (Button) dialog.findViewById(R.id.btnreset);
                Clear = (Button) dialog.findViewById(R.id.btnCancel);

                mobNo = (EditText) dialog.findViewById(R.id.edtmob);
                address = (EditText) dialog.findViewById(R.id.edtaddress);
                edtname = (EditText) dialog.findViewById(R.id.edtname);
                password = (EditText) dialog.findViewById(R.id.edtpass);

                dialog.show();

                Reset.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
					/*if(name.getText().toString().trim().equals(""))
							{
								name.setError("Please enter name");
								name.requestFocus();
							}
							else if(address.getText().toString().trim().equals(""))
							{
								address.setError("Please enter address");
								address.requestFocus();
							}
							else if(mobNo.getText().toString().trim().equals(""))
							{
								mobNo.setError("Please enter mobile no");
								mobNo.requestFocus();
							}
							else if(password.getText().toString().trim().equals(""))
							{
								password.setError("Please enter pin");
								password.requestFocus();
							}					
							else*/
                        if (Common.internet_status) {
                            new SignUpTask().execute();
                        } else {
                            pDialog.dismiss();
                            Toast.makeText(PurchaseReportActivity.this, "Internet connection is not available", Toast.LENGTH_LONG).show();
                        }
                    }
                });

                Clear.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.hide();
                    }
                });

                break;

            //Menu Logout
            case R.id.logout:
                Intent logout = new Intent(PurchaseReportActivity.this, LoginActivity.class);
                startActivity(logout);
                finish();
                break;
            default:
                return super.onOptionsItemSelected(item);

        }
        return true;
    }

    private void forceShowActionBarOverflowMenu() {
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Start asynchronous task for Set DownLine List in Balance Transfer
    public class SetDownlineListTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "DownlineService.asmx/UserDownline";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            //Log.e("urlbal", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                List nameValuePairs = new ArrayList();
                nameValuePairs.add(new BasicNameValuePair("LapuId", Common.lapuid));
                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONSetDownline(result);
        }
    }

    //Parse the JSON response
    void parseJSONSetDownline(String result) {
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(localcontext, "Wrong responce", Toast.LENGTH_LONG).show();
                } else {
                    retailer_id = new String[jsonArray.length()];

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        retailer_id[i] = jo.getString("Member ID") + " - " + jo.getString("Member Name");
                    }
                    retailerid = new ArrayAdapter<String>(PurchaseReportActivity.this, android.R.layout.simple_spinner_item, retailer_id);
                    retailerid.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					/*retailerid.remove(retailer_id[0]);
									retailerid.notifyDataSetChanged();	*/
                    sp_receiverId.setAdapter(retailerid);
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }
    //End asynchronous task for Set DownLine List in Balance Transfer


    //Start asynchronous task for New SignUp/ Activation
    public class SignUpTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "UserActivation.asmx/NewUserActivation";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];

                n = in.read(b);

                if (n > 0) out.append(new String(b, 0, n));

            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(PurchaseReportActivity.this);
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                List nameValuePairs = new ArrayList();
                // add an HTTP variable and value pair
                nameValuePairs.add(new BasicNameValuePair("Name", edtname.getText().toString().trim()));
                nameValuePairs.add(new BasicNameValuePair("Pin", password.getText().toString().trim()));
                nameValuePairs.add(new BasicNameValuePair("MobNumber", mobNo.getText().toString().trim()));
                nameValuePairs.add(new BasicNameValuePair("Address", address.getText().toString().trim()));
                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                request.setURI(new URI(url));
                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();

            if (result.equals("")) //DISTRIBUTER//Admin
            {
                Toast.makeText(localcontext, "Wrong responce", Toast.LENGTH_LONG).show();
            }
            parseJSONSignUp(result);
        }
    }

    //Parse the JSON response
    void parseJSONSignUp(String result) {
        String status = null;
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(localcontext, "Wrong User", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        status = jo.getString("Status");
                    }//for

                    AlertDialog.Builder builder = new AlertDialog.Builder(PurchaseReportActivity.this);
                    builder.setTitle("Message");
                    builder.setIcon(R.mipmap.ic_launcher);
                    builder.setMessage(String.format("Status : %s", status));
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //dialog.dismiss();
                            edtname.setText("");
                            mobNo.setText("");
                            address.setText("");
                            password.setText("");
                            edtname.requestFocus();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }
    //End of asynchronous task for New SignUp/ Activation

    //Start asynchronous task for Day Report
    public class PurchaseTask extends AsyncTask<Void, Void, String> {
        //  String url = Common.COMMON_URL + "GprsFundTransferReport.asmx/fundTransferReport";
        String url = Common.COMMON_URL + "GprsFundTransferReport.asmx/fundTransferTopupReport";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            //Log.e("urlbal", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                List nameValuePairs = new ArrayList();

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                nameValuePairs.add(new BasicNameValuePair("Type", purchasecode));
                nameValuePairs.add(new BasicNameValuePair("DayCode", daycode));

                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONDayReport(result);
        }
    }

    //Parse the JSON response
    void parseJSONDayReport(String result) {
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(PurchaseReportActivity.this, "Wrong responce", Toast.LENGTH_LONG).show();
                } else {
                    strdate = new String[jsonArray.length()];
                    remarks = new String[jsonArray.length()];
                    credit = new String[jsonArray.length()];
                    balance = new String[jsonArray.length()];
                    debit = new String[jsonArray.length()];
                    hours = new String[jsonArray.length()];
                    prebalance = new String[jsonArray.length()];

                    try {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jo = jsonArray.getJSONObject(i);

                            strdate[i] = jo.getString("Trans_Date");
                            remarks[i] = jo.getString("Remarks");
                            credit[i] = jo.getString("Credit");
                            balance[i]=jo.getString("Balance");

                            debit[i] = jo.getString("Debit");
                            hours[i] = jo.getString("Hrs");
                            prebalance[i] = jo.getString("PreBalance");

                            Log.i("Balance---", jo.getString("Balance"));


                        }
                        PurchaseAdapterList adapter = new PurchaseAdapterList(context, strdate, remarks, credit, balance, debit, hours, prebalance);
                        listview.setAdapter(adapter);

                    } catch (Exception f) {
                        PurchaseAdapterList adapter = null;
                        listview.setAdapter(adapter);
                        JSONObject jo = jsonArray.getJSONObject(0);
                        Toast.makeText(getApplicationContext(), jo.getString("Status"), Toast.LENGTH_LONG).show();
                    }
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }
    //End asynchronous task for Day Report


    //Start asynchronous task for show Balance on dashboard
    public class LoadBalCheckTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "LoginUser.asmx/GetBalance";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }

            return out.toString();
        }

        @Override
        protected void onPreExecute() {
			/*pDialog.setMessage("Please wait..");
			pDialog.setIndeterminate(true);
			pDialog.setCancelable(false);
			pDialog.show();*/
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            Log.e("url", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONLoadBalCheck(result);
        }
    }

    //Parse the JSON response
    void parseJSONLoadBalCheck(String result) {
        String status = "";
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(PurchaseReportActivity.this, "Wrong responce", Toast.LENGTH_LONG).show();
                } else {
                    //Received response from web service
					/*"Status": "489.7500"*/
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        status = jo.getString("Status");
                        //Log.e("bal",status);
                    }//for

                    MainActivity.txtBalance.setText(status);
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
                e.printStackTrace();
            }
        }
    }
    //End asynchronous task for show Balance on dashboard


    //Start asynchronous task for Notification
    public class NotificationTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "AddNotification.asmx/ShowNotification";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];

                n = in.read(b);

                if (n > 0) out.append(new String(b, 0, n));

            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            //Log.e("urlbal", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

				/*request.addHeader("Regid",Common.regid);
						request.addHeader("RechargeID",Common.recharge_id);*/

				/*List nameValuePairs = new ArrayList();
						// add an HTTP variable and value pair
						nameValuePairs.add(new BasicNameValuePair("RechargeID", Common.recharge_id));
						nameValuePairs.add(new BasicNameValuePair("RegIdNo", Common.regid));
						request.setEntity(new UrlEncodedFormEntity(nameValuePairs));*/
                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);


            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONNotification(result);
        }
    }

    //Parse the JSON response
    void parseJSONNotification(String result) {
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(localcontext, "Wrong responce", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        txtNotification.setText(jo.getString("message"));
                    }
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }
    //End asynchronous task for Notification


    //Start asynchronous task for Balance check
    public class BalCheckTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "LoginUser.asmx/GetBalance";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }

            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            Log.e("url", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

				/*List nameValuePairs = new ArrayList();
						// add an HTTP variable and value pair
						nameValuePairs.add(new BasicNameValuePair("RechargeID", Common.recharge_id));
						nameValuePairs.add(new BasicNameValuePair("Regid", Common.regid));
						request.setEntity(new UrlEncodedFormEntity(nameValuePairs));*/

                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);

				/*HttpClient client = new DefaultHttpClient();
						HttpGet request = new HttpGet();
						request.setURI(new URI(url));
						HttpResponse response = client.execute(request);
						request.addHeader("Regid",Common.regid);
						request.addHeader("RechargeID",Common.recharge_id);
						Log.e("Regid", Common.regid);
						Log.e("RechargeID", Common.recharge_id);
						HttpEntity entity = response.getEntity();
						result = getASCIIContentFromEntity(entity);

						Log.e("Result",result);*/

                //parseJSON(result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONBalCheck(result);
        }
    }

    //Parse the JSON response
    void parseJSONBalCheck(String result) {
        String status = "";
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(localcontext, "Wrong responce", Toast.LENGTH_LONG).show();
                } else {
                    //Received response from web service
					/*"Status": "489.7500"*/
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        status = jo.getString("Status");
                        //Log.e("bal",status);

                    }//for

                    AlertDialog.Builder builder = new AlertDialog.Builder(PurchaseReportActivity.this);//.getParent()
                    builder.setTitle("Message");
                    builder.setIcon(R.mipmap.ic_launcher);
                    builder.setMessage("Your balance is " + status);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
                e.printStackTrace();
            }
        }
    }
    //End asynchronous task for Balance check


    //Start asynchronous task for Add Notification
    public class AddNotificationTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "AddNotification.asmx/addNotication";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");

            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                List nameValuePairs = new ArrayList();
                // add an HTTP variable and value pair
                nameValuePairs.add(new BasicNameValuePair("NotificationText", addNotification.getText().toString().trim()));

                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONAddNotification(result);
            new NotificationTask().execute();
        }
    }

    //Parse the JSON response
    void parseJSONAddNotification(String result) {
        String status = "";
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(localcontext, "Wrong responce", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        status = jo.getString("Status");
                        //Log.e("bal", jo.getString("CURRENT_BALANCE")+" "+bal);
                    }//for

                    AlertDialog.Builder builder = new AlertDialog.Builder(PurchaseReportActivity.this);//.getParent()
                    builder.setTitle("Message");
                    builder.setIcon(R.mipmap.ic_launcher);
                    builder.setMessage("" + status);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            addNotification.setText("");
                            //dialog.dismiss();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }
    //End asynchronous task for Add Notification


    //Start asynchronous task for Change Mobile
    public class ChangeMobTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "ChangeMobileNo.asmx/ChangeMobileno";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];

                n = in.read(b);

                if (n > 0) out.append(new String(b, 0, n));

            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            //Log.e("urlbal", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                List nameValuePairs = new ArrayList();
                // add an HTTP variable and value pair
                nameValuePairs.add(new BasicNameValuePair("Lapuid", Common.lapuid));
                nameValuePairs.add(new BasicNameValuePair("OldMobileNo", oldMobileNumber.getText().toString().trim()));
                nameValuePairs.add(new BasicNameValuePair("NewMobileNo", newMobileNumber.getText().toString().trim()));

                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);


            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONChangeMob(result);
        }
    }

    //Parse the JSON response
    void parseJSONChangeMob(String result) {
        String status = "";
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(localcontext, "Wrong responce", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        status = jo.getString("Status");
                        //Log.e("bal", jo.getString("Status")+" "+bal);
                    }//for

                    AlertDialog.Builder builder = new AlertDialog.Builder(PurchaseReportActivity.this);//.getParent()
                    builder.setTitle("Message");
                    builder.setIcon(R.mipmap.ic_launcher);
                    builder.setMessage("" + status);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //dialog.dismiss();
                            newMobileNumber.setText("");
                            oldMobileNumber.setText("");

                            newMobileNumber.requestFocus();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }
    //End asynchronous task for Change Mobile

    //Start asynchronous task for Change Password
    public class ChangePassTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "ChangePassword.asmx/ChangePassWord";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];

                n = in.read(b);

                if (n > 0) out.append(new String(b, 0, n));

            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            //Log.e("urlbal", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                List nameValuePairs = new ArrayList();
                // add an HTTP variable and value pair
                nameValuePairs.add(new BasicNameValuePair("LoginId", Common.lapuid));
                nameValuePairs.add(new BasicNameValuePair("oldpass", oldPassword.getText().toString().trim()));
                nameValuePairs.add(new BasicNameValuePair("newpass", newPassword.getText().toString().trim()));
				/*nameValuePairs.add(new BasicNameValuePair("RechargeID", Common.recharge_id));
						nameValuePairs.add(new BasicNameValuePair("RegIdNo", Common.regid));*/

                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);


            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONChangePass(result);
        }
    }

    //Parse the JSON response
    void parseJSONChangePass(String result) {
        String status = "";
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(localcontext, "Wrong responce", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        status = jo.getString("Status");
                        //Log.e("bal", jo.getString("CURRENT_BALANCE")+" "+bal);
                    }//for

                    AlertDialog.Builder builder = new AlertDialog.Builder(PurchaseReportActivity.this);//.getParent()
                    builder.setTitle("Message");
                    builder.setIcon(R.mipmap.ic_launcher);
                    builder.setMessage("" + status);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //dialog.dismiss();
                            newPassword.setText("");
                            oldPassword.setText("");

                            oldPassword.requestFocus();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }
    //End asynchronous task for Change Password


    //Start asynchronous task for Balance Transfer
    public class BalanceTransTask extends AsyncTask<Void, Void, String> {
        String url = Common.COMMON_URL + "BalaneTransfer.asmx/UserBalanceTransfer";

        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[4096];
                n = in.read(b);
                if (n > 0) out.append(new String(b, 0, n));
            }
            return out.toString();
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            url = url.replaceAll(" ", "%20");
            //Log.e("urlbal", url);
            try {

                HttpClient client = new DefaultHttpClient();
                HttpPost request = new HttpPost();
                request.setURI(new URI(url));

                request.addHeader("Regid", Common.regid);
                request.addHeader("RechargeID", Common.recharge_id);

                List nameValuePairs = new ArrayList();
                // add an HTTP variable and value pair
                nameValuePairs.add(new BasicNameValuePair("MemberId", retid));
                nameValuePairs.add(new BasicNameValuePair("Amount", edtamount.getText().toString().trim()));
                nameValuePairs.add(new BasicNameValuePair("Pin", pinNo.getText().toString().trim()));

                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = client.execute(request);

                HttpEntity entity = response.getEntity();
                result = getASCIIContentFromEntity(entity);

                Log.e("Result", result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            parseJSONBalanceTrans(result);
        }
    }

    //Parse the JSON response
    void parseJSONBalanceTrans(String result) {
        String status = "";
        if (result != null) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() == 0) {
                    Toast.makeText(localcontext, "Wrong responce", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        status = jo.getString("Status");
                        //Log.e("baltrans", status);
                    }//for

                    AlertDialog.Builder builder = new AlertDialog.Builder(PurchaseReportActivity.this);//.getParent()
                    builder.setTitle("Message");
                    builder.setIcon(R.mipmap.ic_launcher);
                    builder.setMessage("" + status);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
							/*pinNo.setText("");
									amount.setText("");*/
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(20000);
                            } catch (InterruptedException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            new LoadBalCheckTask().execute();
                        }
                    }).start();
                }
            } catch (JSONException e) {
                Log.e("JSONError", e.getMessage());
            }
        }
    }
    //End asynchronous task for Balance Transfer

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
        int targetWidth = 1000;
        int targetHeight = 1000;
        Bitmap targetBitmap = Bitmap.createBitmap(targetWidth,
                targetHeight, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(targetBitmap);
        Path path = new Path();
        path.addCircle(((float) targetWidth - 1) / 2,
                ((float) targetHeight - 1) / 2,
                (Math.min(((float) targetWidth),
                        ((float) targetHeight)) / 2),
                Path.Direction.CCW);

        canvas.clipPath(path);
        Bitmap sourceBitmap = bitmap;
        canvas.drawBitmap(sourceBitmap,
                new Rect(0, 0, sourceBitmap.getWidth(),
                        sourceBitmap.getHeight()),
                new Rect(0, 0, targetWidth, targetHeight), null);
        return targetBitmap;
    }
    @Override
    public void onBackPressed() {
        Intent homeintent = new Intent(PurchaseReportActivity.this, MainActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("type", "GPRS");
        homeintent.putExtras(bundle);
        startActivity(homeintent);
        finish();
    }
}
